
$(function () {
//    $('input').on("keypress", function (e) {
//        /* ENTER PRESSED*/
//        if (e.keyCode == 13) {
//            /* FOCUS ELEMENT */
//            var inputs = $(this).parents("form").eq(0).find(":input");
//            var idx = inputs.index(this);
//
//            if (idx == inputs.length - 1) {
//                inputs[0].select()
//            } else {
//                inputs[idx + 1].focus(); //  handles submit buttons
//                inputs[idx + 1].select();
//            }
//            return false;
//        }
//    });
    $('input').bind('keypress', function (eInner) {
        if (eInner.keyCode == 13) //if its a enter key
        {
            var tabindex = $(this).attr('tabindex');
            tabindex++; //increment tabindex
            $('[tabindex=' + tabindex + ']').focus();

//            $('#Msg').text($(this).attr('id') + " tabindex: " + tabindex + " next element: " + $('*').attr('tabindex').id);


            // to cancel out Onenter page postback in asp.net
            return false;
        }
    });

    $('select').bind('keydown', function (eInner) {
        alert(eInner.keyCode);
        if (eInner.keyCode == 13) //if its a enter key
        {
            var tabindex = $(this).attr('tabindex');
            tabindex++; //increment tabindex
            $('[tabindex=' + tabindex + ']').focus();

//            $('#Msg').text($(this).attr('id') + " tabindex: " + tabindex + " next element: " + $('*').attr('tabindex').id);


            // to cancel out Onenter page postback in asp.net
            return false;
        }
    });

});


function checkAlp(e) {
    /* Valid Value : A to Z, a to z, tab, capslock, backspace, delete */
    var keyValue;
    var evt;
    evt = e || window.event
    keyValue = evt.keyCode || evt.which;
    if ((keyValue == 8) || //backspace
            (keyValue == 9) || //tab
            (keyValue == 20) || //caps lock
            (keyValue == 32) || //space
            (keyValue >= 65 && keyValue <= 90) || //a-z
            (keyValue >= 97 && keyValue <= 122))    //A-Z
    {
        return true;
    } else {
        return false;
    }
}

function checkNumber(e) {
    /* Valid Value : tab, capslock, backspace, delete, 0 to 9 */
    var keyValue;
    var evt;
    evt = e || window.event
    keyValue = evt.keyCode || evt.which;
    if ((keyValue == 8) || //backspace
            (keyValue == 9) || //tab
            (keyValue == 20) || //caps lock
            (keyValue == 32) || //space
            (keyValue >= 48 && keyValue <= 57)) {   //0-9 
        return true;
    } else {
        return false;
    }
}

function checkAlpNumeric(e) {
    /* Valid Value : A to Z, a to z, space, tab, capslock, backspace, delete, 0 to 9 */
    var keyValue;
    var evt;
    evt = e || window.event
    keyValue = evt.keyCode || evt.which;
    if ((keyValue == 8) || //backspace
            (keyValue == 9) || //tab
            (keyValue == 20) || //caps lock
            (keyValue == 32) || //space
            (keyValue >= 65 && keyValue <= 90) || //a-z
            (keyValue >= 48 && keyValue <= 57) || //0-9 
            (keyValue >= 97 && keyValue <= 122))    //A-Z
    {
        return true;
    } else {
        return false;
    }
}

function checkNumberStripe(e) {
    /* Valid Value : tab, capslock, backspace, delete, 0 to 9 , . , - */
    var keyValue;
    var evt;
    evt = e || window.event
    keyValue = evt.keyCode || evt.which;
    if ((keyValue == 8) || //backspace
            (keyValue == 9) || //tab
            (keyValue == 20) || //caps lock
            (keyValue == 32) || //space
            (keyValue == 45) || //-
            (keyValue == 46) || //.
            (keyValue >= 48 && keyValue <= 57)) {   //0-9 
        return true;
    } else {
        return false;
    }
}
function checkNumberAndx(e) {
    /* Valid Value : tab, capslock, backspace, delete, 0 to 9 , ., x  */
    var keyValue;
    var evt;
    evt = e || window.event
    keyValue = evt.keyCode || evt.which;
    if ((keyValue == 8) || //backspace
            (keyValue == 9) || //tab
            (keyValue == 20) || //caps lock
            (keyValue == 32) || //space
            (keyValue == 46) || //.
            (keyValue >= 48 && keyValue <= 57) || //0-9
            (keyValue == 120)) {   //x
        return true;
    } else {
        return false;
    }
}

function checkFloat(e, v) {
    /* Valid Value : tab, capslock, backspace, delete, 0 to 9 and . */
    var keyValue;
    var evt;
    var l = 0;
    var check = true;

    evt = e || window.event
    keyValue = evt.keyCode || evt.which;

    for (l = 0; l < v.length; l++) {
        if (v[l] == ".") {
            check = false;
        }
    }
    if ((keyValue == 8) || //backspace
            (keyValue == 9) || //tab
            (keyValue == 20) || //caps lock
            (keyValue == 46 && check == true) || //.
            (keyValue >= 48 && keyValue <= 57)) {   //0-9 
        return true;
    } else {
        return false;
    }
}

function checkFloatFormat(v) {
    var x = "";
    var l = v.length - 1;

    if (v.length >= 0 && v[0] == ".") {
        x = "0" + v;
        return x;
    } else if (v.length >= 0 && v[l] == ".") {
        x = v + "0";
        return x;
    } else {
        return v;
    }
}

function checkEmail(value) {
    if (value == "") {
        return value;
    } else {
        var pattern = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (pattern.test(value)) {
            return value;
        } else {
            return "Wrong Format";
        }
    }
}

function trim(s)
{
    var l = 0;
    var r = s.length - 1;
    while (l < s.length && s[l] == ' ')
    {
        l++;
    }
    while (r > l && s[r] == ' ')
    {
        r -= 1;
    }
    return s.substring(l, r + 1);
}

function formatTimeManual(value) {

    var hour = value.substr(0, 2);
    var minute = value.substr(3, 2);
    var second = value.substr(6, 2);

    if ((hour <= 24) && (minute <= 60) && (second <= 60))
    {
        return obj.value;
    } else {
        return "";
    }

}

function formatDouble(obj, e) {
    var value;
    var key;
    var evt;

    evt = e || window.event
    key = evt.keyCode || evt.which;
    value = obj.value;

    if ((key == 8) || //backspace
            (key == 9) || //tab
            (key == 20) || //caps lock
            (key == 32) || //space
            (key >= 48 && key <= 57)) {   //0-9 

        if (value.length == 3 && key != 8 && obj.value != '0.0') {
            obj.value = value + ".";
        }

        return true;
    } else {
        return false;
    }
}
function removeComma(str) {
    var tempstr = "";
    var digit = str;
    for (var x = 0; x < digit.length; x++)
    {
        if (digit.charAt(x) != ",")
            tempstr += digit.charAt(x);
    }
    return tempstr;
}

function addComma(input, e) {
    var keyValue;
    var evt;
    evt = e || window.event
    keyValue = evt.keyCode || evt.which;

    if (keyValue == 9)
        return;

    var digit = "", fraction = "";
    var str = input.value;

//    if (input.value == "" || input.value == "0" || input.value == "00" || input.value == "000" || input.value == ".0" || input.value == ".00" || input.value == "0.0" || input.value == "0.") {
//        input.value = "";
//        return;
//    }

    if (str.indexOf(".") == 0)
        return;
    if (str.indexOf(".") > 0) {
        digit = str.substring(0, str.indexOf("."));
        fraction = str.substring(str.indexOf("."), str.length);
    } else
        digit = str;

    digit = removeComma(digit);

    var start = 0, end = 0;
    var result = "";
    start = digit.length - 3;
    end = digit.length;
    while (start > 0) {
        if (start > 0 && digit.substring(start - 1, start) != ",")
            result = "," + digit.substring(start, end) + result;
        else
            result = digit.substring(start, end) + result;
        end = start;
        start -= 3;
    }
    input.value = digit.substring(0, end) + result + fraction;
}

function handleDrop(event, ui) {
    var droppedAccess = ui.draggable;
    droppedAccess.fadeOut('fast');

}

function handleDrop2(event, ui) {
    var droppedAccess = ui.draggable;
    droppedAccess.fadeOut('fast');
}

function calculateTotal(obj1, obj2, obj3) {
            var val1 = obj1.value;
            var val2 = obj2.value;
            var val3 = val1 - val2;
            obj3.value = val3;
    }

function detect_device() {
    if (navigator.userAgent.match(/Android/i)) {
        alert("Hai Android ! ^^");
        window.location.href = "loginMobile.xhtml";
    } else if (navigator.userAgent.match(/webOS/i)) {
        alert("Hai Web OS ! ^^");
        window.location.href = "loginMobile.xhtml";
    } else if (navigator.userAgent.match(/iPhone/i)) {
        alert("Hai iPhone ! ^^");
        window.location.href = "loginMobile.xhtml";
    } else if (navigator.userAgent.match(/iPad/i)) {
        alert("Hai iPad ! ^^");
        window.location.href = "loginMobile.xhtml";
    } else if (navigator.userAgent.match(/iPod/i)) {
        alert("Hai iPod ! ^^");
        window.location.href = "loginMobile.xhtml";
    } else if (navigator.userAgent.match(/BlackBerry/)) {
        alert("Hai BlackBerry ! ^^");
        window.location.href = "loginMobile.xhtml";
    } else if (navigator.userAgent.match(/Windows Phone/i)) {
        alert("Hai Windows Phone ! ^^");
        window.location.href = "loginMobile.xhtml";
    } else if (navigator.userAgent.match(/ZuneWP7/i)) {
        alert("Hai Zune WP7 ! ^^");
        window.location.href = "loginMobile.xhtml";
    }
//    if (navigator.userAgent.match(/Android/i) ||
//            navigator.userAgent.match(/webOS/i) ||
//            navigator.userAgent.match(/iPhone/i) ||
//            navigator.userAgent.match(/iPad/i) ||
//            navigator.userAgent.match(/iPod/i) ||
//            navigator.userAgent.match(/BlackBerry/) ||
//            navigator.userAgent.match(/Windows Phone/i) ||
//            navigator.userAgent.match(/ZuneWP7/i)
//            ) {
//        window.location.href = "eric/chartMobile.xhtml";
//    }
    function calculateTotal(obj1, obj2, obj3) {
            var val1 = obj1.value;
            var val2 = obj2.value;
            var val3 = val1 - val2;
            obj3.values(val3);
    }
}