/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital.controllers;

import com.hospital.crud.CrudService;
import com.hospital.models.TblAccountSpecial;
import com.hospital.models.TblCoa;
import com.hospital.models.TblUser;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author arisoft
 */
@ManagedBean(name = "masterAccountSpecialController")
@ViewScoped
public class MasterAccountSpecialController extends BaseController{

    @EJB
    private CrudService crudService;
    private LazyDataModel<TblAccountSpecial> lazyTblAccountSpecial = null;
    private TblAccountSpecial tblAccountSpecial = new TblAccountSpecial();
    private String accountNo;

    /**
     * Creates a new instance of MasterSpecialAccountController
     */
    
    public MasterAccountSpecialController() {
    }
    
    public void refreshDataTable() {
        loadTable();
    }
    
    @PostConstruct
    public void init() {
        loadTable();
    }
    
    private void loadTable() {
        lazyTblAccountSpecial = new LazyDataModel<TblAccountSpecial>() {
            @Override
            public List<TblAccountSpecial> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> map) {
                String thisQuery = "SELECT t FROM TblAccountSpecial t WHERE 1 = 1";
                Set<Map.Entry<String, Object>> rawParameters = map.entrySet();

                Boolean isSearch = false;
                for (Map.Entry<String, Object> entry : rawParameters) {
                    isSearch = true;
                    thisQuery = thisQuery + " AND LOWER(t." + entry.getKey() + ") LIKE '%" + entry.getValue().toString().toLowerCase() + "%'";
                }

                if (sortField != null) {
                    String sortMethod = "DESC";
                    if (sortOrder.toString().equalsIgnoreCase("ASCENDING")) {
                        sortMethod = "ASC";
                    }
                    thisQuery = thisQuery + " ORDER BY t." + sortField + " " + sortMethod;
                }
                List<TblAccountSpecial> list = crudService.findWithCreateQuery(thisQuery, first, pageSize);
                if (isSearch) {
                    setRowCount(list.size());
                } else {
                    int rowCount = crudService.countWithNamedQuery("TblAccountSpecial.countAll");
                    setRowCount(rowCount);
                }
                setPageSize(pageSize);
                return list;
            }

            @Override
            public TblAccountSpecial getRowData(String rowKey) {
                HashMap<String, Object> param = new HashMap<>();
                param.put("accountSpecialId", rowKey);
                List<TblAccountSpecial> list = crudService.findWithNamedQuery("TblAccountSpecial.findByAccountSpecialId", param);
                if (list.isEmpty()) {
                    return null;
                } else {
                    return list.get(0);
                }
            }

            @Override
            public Object getRowKey(TblAccountSpecial obj) {
                return obj.getAccountSpecialId();
            }
        };
    }
    
    public void doUpdate() {
        if (tblAccountSpecial == null) {
            createMessage("WARN", "Pilih data terlebih dahulu.");
        } else {
            accountNo = tblAccountSpecial.getAccountSpecialCoaId().getCoaAccountNo() + " - " + tblAccountSpecial.getAccountSpecialCoaId().getCoaAccountNama();
            executeInstance("PF('widDlgUpdate').show()");
            updateInstance("formDlgUpdate:pgridUpdate");
        }
    }
    
    public void update() {
        String account = accountNo.substring(0, accountNo.indexOf(" "));
        HashMap<String, Object> param = new HashMap<>();
        param.put("coaAccountNo", account);
        List<TblCoa> list = crudService.findWithNamedQuery("TblCoa.findByCoaAccountNo", param);
        tblAccountSpecial.setAccountSpecialCoaId(list.get(0));
        crudService.update(tblAccountSpecial);
        tblAccountSpecial = new TblAccountSpecial();
        createMessage("INFO", "Account special diubah.");
        executeInstance("PF('widDlgUpdate').hide()");
        updateInstance("thisForm:thisData");
    }
    
    public List<String> findNoAccount(String query) {
        return crudService.findByNativeQuery("SELECT CONCAT(coa_account_no,' : ', coa_account_nama) FROM tbl_coa WHERE coa_account_no LIKE '%" + query.toLowerCase() + "%' OR upper(coa_account_nama) LIKE '" + query.toUpperCase()+ "%'");
    }

    public LazyDataModel<TblAccountSpecial> getLazyTblAccountSpecial() {
        return lazyTblAccountSpecial;
    }

    public void setLazyTblAccountSpecial(LazyDataModel<TblAccountSpecial> lazyTblAccountSpecial) {
        this.lazyTblAccountSpecial = lazyTblAccountSpecial;
    }

    public TblAccountSpecial getTblAccountSpecial() {
        return tblAccountSpecial;
    }

    public void setTblAccountSpecial(TblAccountSpecial tblAccountSpecial) {
        this.tblAccountSpecial = tblAccountSpecial;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }
    
    
    

}
