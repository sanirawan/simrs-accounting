/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital.controllers;

import com.hospital.crud.CrudService;
import com.hospital.models.TblCoa;
import com.hospital.models.TrxLedger;
import com.hospital.models.TrxLedgerGroup;
import com.hospital.utils.HospitalStringUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author eh-regex
 */
@ManagedBean(name = "jurnalUmum")
@ViewScoped
public class JurnalUmum extends BaseController implements Serializable {

    @EJB
    private CrudService crudService;
    private LazyDataModel<TrxLedgerGroup> lazyTrxLedgerGroupJurnalUmum = null;
    private LazyDataModel<TrxLedgerGroup> lazyTrxLedgerGroupJurnalUmumBelumPosting = null;
    private List<TrxLedgerGroup> selectedJurnalUmumBelumPosting = new ArrayList<>();
    private TrxLedgerGroup trxLedgerGroupDeleted = null;
    private TrxLedgerGroup trxLedgerGroupEdited = null;
    private List<TrxLedger> listDetailCurrent = new ArrayList<>();
    private List<TrxLedger> listDetailForDeleted = new ArrayList<>();

    private String noJurnal = "";
    private String keteranganJurnal = "";
    private Date tanggalJurnal = new Date();

    private List<TrxLedger> listDetail = new ArrayList<>();
    private Double totalDebit;
    private Double totalKredit;
    private Double totalSelish;
    private Double totalKreditOtomatisFill;
    private String totalDebitFormatter = "0";
    private String totalKreditFormatter = "0";
    private String totalSelisihFormatter = "0";
    private String totalKreditOtomatisFillFormatter = "0";

    private List<TrxLedger> listDetailForTable1;
    private List<TrxLedger> listDetailForTable2;

    /**
     * =============== PENCARIAN DATA
     */
    private Boolean toggleButtonFilter = true;

    private Boolean checkBoxFilterNoJurnal = false;
    private Boolean checkBoxFilterTanggal = false;
    private Boolean checkBoxFilterCoa = false;

    private Boolean toggleFilterNoJurnal = true;
    private Boolean toggleFilterTanggal = true;
    private Boolean toggleFilterCoa = true;

    private String searchNoJurnal = "";
    private Date searchTanggal;
    private String searchCoa = "";

    /**
     * Creates a new instance of KasirController
     */
    public JurnalUmum() {

    }

    @PostConstruct
    public void init() {
        loadTable();
        loadTableBelumPosting();
    }

    public void prepareInsertPembayaran() {
        trxLedgerGroupDeleted = null;

        noJurnal = "";
        keteranganJurnal = "";
        tanggalJurnal = new Date();
        listDetail = new ArrayList<>();

        totalDebit = 0.0;
        totalKredit = 0.0;
        totalSelish = 0.0;
        totalKreditOtomatisFill = 0.0;
        totalDebitFormatter = "0";
        totalKreditFormatter = "0";
        totalSelisihFormatter = "0";
        totalKreditOtomatisFillFormatter = "0";

        executeInstance("PF('widDlgInsert').show()");
        updateInstance("formDlgInsert:dlgInsert");
    }

    public List<String> findCoaNo(String query) {
        return crudService.findByNativeQuery("SELECT coa_account_no FROM tbl_coa WHERE UPPER(coa_account_no) LIKE '" + query.toUpperCase() + "%'");
    }

    public void noAccOnChange(TrxLedger detail) {
        HashMap<String, Object> param = new HashMap<>();
        param.put("coaAccountNo", detail.getDetailCoaNo());
        List<TblCoa> listCoa = crudService.findWithNamedQuery("TblCoa.findByCoaAccountNo", param);
        if (!listCoa.isEmpty()) {
            detail.setDetailCoaNama(listCoa.get(0).getCoaAccountNama());
        }
    }

    public List<String> findCoaNama(String query) {
        return crudService.findByNativeQuery("SELECT coa_account_nama FROM tbl_coa WHERE UPPER(coa_account_nama) LIKE '%" + query.toUpperCase() + "%'");
    }

    public void namaAccOnChange(TrxLedger detail) {
        HashMap<String, Object> param = new HashMap<>();
        param.put("coaAccountNama", detail.getDetailCoaNama());
        List<TblCoa> listCoa = crudService.findWithNamedQuery("TblCoa.findByCoaAccountNama", param);
        if (!listCoa.isEmpty()) {
            detail.setDetailCoaNo(listCoa.get(0).getCoaAccountNo());
        }
    }

    private void loadTable() {
        lazyTrxLedgerGroupJurnalUmum = new LazyDataModel<TrxLedgerGroup>() {
            @Override
            public List<TrxLedgerGroup> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> map) {
                String thisQuery = "SELECT t FROM TrxLedgerGroup t WHERE 1 = 1";
                Set<Map.Entry<String, Object>> rawParameters = map.entrySet();

                Boolean isSearch = false;
                for (Map.Entry<String, Object> entry : rawParameters) {
                    isSearch = true;
                    thisQuery = thisQuery + " AND LOWER(t." + entry.getKey() + ") LIKE '%" + entry.getValue().toString().toLowerCase() + "%'";
                }

                if (sortField != null) {
                    String sortMethod = "DESC";
                    if (sortOrder.toString().equalsIgnoreCase("ASCENDING")) {
                        sortMethod = "ASC";
                    }
                    thisQuery = thisQuery + " ORDER BY t." + sortField + " " + sortMethod;
                } else {
                    thisQuery = thisQuery + " ORDER BY t.ledgerGroupStatusJurnalUmum DESC, t.ledgerGroupTgl DESC, t.insertDate DESC";
                }

                List<TrxLedgerGroup> list = crudService.findWithCreateQuery(thisQuery, first, pageSize);
                if (isSearch) {
                    setRowCount(list.size());
                } else {
                    int rowCount = crudService.countWithNamedQuery("TrxLedgerGroup.countAllJurnalUmum");
                    setRowCount(rowCount);
                }
                setPageSize(pageSize);
                return list;
            }

            @Override
            public TrxLedgerGroup getRowData(String rowKey) {
                HashMap<String, Object> param = new HashMap<>();
                param.put("ledgerGroupId", rowKey);
                List<TrxLedgerGroup> list = crudService.findWithNamedQuery("TrxLedgerGroup.findByLedgerGroupId", param);
                if (list.isEmpty()) {
                    return null;
                } else {
                    return list.get(0);
                }
            }

            @Override
            public Object getRowKey(TrxLedgerGroup obj) {
                return obj.getLedgerGroupId();
            }
        };
    }

    private void loadTableBelumPosting() {
        lazyTrxLedgerGroupJurnalUmumBelumPosting = new LazyDataModel<TrxLedgerGroup>() {
            @Override
            public List<TrxLedgerGroup> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> map) {
                String thisQuery = "SELECT t FROM TrxLedgerGroup t WHERE t.ledgerGroupStatusJurnalUmum = '2'";
                Set<Map.Entry<String, Object>> rawParameters = map.entrySet();

                Boolean isSearch = false;
                for (Map.Entry<String, Object> entry : rawParameters) {
                    isSearch = true;
                    thisQuery = thisQuery + " AND LOWER(t." + entry.getKey() + ") LIKE '%" + entry.getValue().toString().toLowerCase() + "%'";
                }

                if (sortField != null) {
                    String sortMethod = "DESC";
                    if (sortOrder.toString().equalsIgnoreCase("ASCENDING")) {
                        sortMethod = "ASC";
                    }
                    thisQuery = thisQuery + " ORDER BY t." + sortField + " " + sortMethod;
                } else {
                    thisQuery = thisQuery + " ORDER BY t.ledgerGroupTgl DESC, t.insertDate DESC";
                }

                List<TrxLedgerGroup> list = crudService.findWithCreateQuery(thisQuery, first, pageSize);
                if (isSearch) {
                    setRowCount(list.size());
                } else {
                    int rowCount = crudService.countWithNamedQuery("TrxLedgerGroup.countAllJurnalUmumBelumPosting");
                    setRowCount(rowCount);
                }
                setPageSize(pageSize);
                return list;
            }

            @Override
            public TrxLedgerGroup getRowData(String rowKey) {
                HashMap<String, Object> param = new HashMap<>();
                param.put("ledgerGroupId", rowKey);
                List<TrxLedgerGroup> list = crudService.findWithNamedQuery("TrxLedgerGroup.findByLedgerGroupId", param);
                if (list.isEmpty()) {
                    return null;
                } else {
                    return list.get(0);
                }
            }

            @Override
            public Object getRowKey(TrxLedgerGroup obj) {
                return obj.getLedgerGroupId();
            }
        };
    }

    public void onRowToggle1(ToggleEvent event) {
        TrxLedgerGroup trx = (TrxLedgerGroup) event.getData();
        listDetailForTable1 = crudService.findByNativeQuery("select * from trx_ledger where ledger_ledger_group_id = '" + trx.getLedgerGroupId() + "' ORDER BY ledger_seq", TrxLedger.class);
    }

    public void onRowToggle2(ToggleEvent event) {
        TrxLedgerGroup trx = (TrxLedgerGroup) event.getData();
        listDetailForTable2 = crudService.findByNativeQuery("select * from trx_ledger where ledger_ledger_group_id = '" + trx.getLedgerGroupId() + "' ORDER BY ledger_seq", TrxLedger.class);
    }

    public void listenerCheckboxFilter() {
        if (checkBoxFilterNoJurnal) {
            toggleFilterNoJurnal = false;
        } else {
            toggleFilterNoJurnal = true;
        }
        if (checkBoxFilterTanggal) {
            toggleFilterTanggal = false;
        } else {
            toggleFilterTanggal = true;
        }
        if (checkBoxFilterCoa) {
            toggleFilterCoa = false;
        } else {
            toggleFilterCoa = true;
        }

        if (checkBoxFilterNoJurnal || checkBoxFilterTanggal || checkBoxFilterCoa) {
            toggleButtonFilter = false;
        } else {
            toggleButtonFilter = true;
        }
    }

    public List<String> findCoa(String query) {
        return crudService.findByNativeQuery("SELECT CONCAT(coa_account_no, ' - ' ,coa_account_nama) FROM tbl_coa WHERE UPPER(CONCAT(coa_account_no, coa_account_nama)) LIKE '%" + query.toUpperCase() + "%'");
    }

    public List<String> findNoJurnal(String query) {
        return crudService.findByNativeQuery("SELECT ledger_group_no_jurnal_umum FROM trx_ledger_group WHERE ledger_group_status_jurnal_umum > 0 AND UPPER(ledger_group_no_jurnal_umum) LIKE '%" + query.toUpperCase() + "%'");
    }

    public void doFilter() {
        lazyTrxLedgerGroupJurnalUmum = new LazyDataModel<TrxLedgerGroup>() {
            @Override
            public List<TrxLedgerGroup> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> map) {
                String thisQuery = "SELECT t FROM TrxLedgerGroup t INNER JOIN t.trxLedger td WHERE 1 = 1";
                String thisQueryCount = "SELECT COUNT(t) FROM TrxLedgerGroup t INNER JOIN t.trxLedger td WHERE 1 = 1";
                Set<Map.Entry<String, Object>> rawParameters = map.entrySet();

                if (checkBoxFilterNoJurnal) {
                    thisQuery = thisQuery + " AND t.ledgerGroupNoJurnalUmum = '" + searchNoJurnal + "'";
                    thisQueryCount = thisQueryCount + " AND t.ledgerGroupNoJurnalUmum = '" + searchNoJurnal + "'";
                }
                if (checkBoxFilterTanggal) {
                    thisQuery = thisQuery + " AND (t.ledgerGroupTgl BETWEEN '" + dateQueryFormat("from", searchTanggal) + "' AND '" + dateQueryFormat("to", searchTanggal) + "')";
                    thisQueryCount = thisQueryCount + " AND (t.ledgerGroupTgl BETWEEN '" + dateQueryFormat("from", searchTanggal) + "' AND '" + dateQueryFormat("to", searchTanggal) + "')";
                }
                if (checkBoxFilterCoa) {
                    thisQuery = thisQuery + " AND td.ledgerCoaId.coaAccountNo = '" + searchCoa.substring(0, searchCoa.indexOf(" ")) + "'";
                    thisQueryCount = thisQueryCount + " AND td.ledgerCoaId.coaAccountNo = '" + searchCoa.substring(0, searchCoa.indexOf(" ")) + "'";
                }

                thisQuery = thisQuery + " GROUP BY t.ledgerGroupId ORDER BY t.ledgerGroupStatusJurnalUmum DESC, t.ledgerGroupTgl DESC, t.insertDate DESC";
                List<TrxLedgerGroup> list = crudService.findWithCreateQuery(thisQuery, first, pageSize);
                setRowCount(crudService.countWithCreateQuery(thisQueryCount));
                setPageSize(pageSize);
                return list;
            }
        };

        lazyTrxLedgerGroupJurnalUmumBelumPosting = new LazyDataModel<TrxLedgerGroup>() {
            @Override
            public List<TrxLedgerGroup> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> map) {
                String thisQuery = "SELECT t FROM TrxLedgerGroup t INNER JOIN t.trxLedger td WHERE t.ledgerGroupStatusJurnalUmum = '2'";
                String thisQueryCount = "SELECT COUNT(t) FROM TrxLedgerGroup t INNER JOIN t.trxLedger td WHERE t.ledgerGroupStatusJurnalUmum = '2'";
                Set<Map.Entry<String, Object>> rawParameters = map.entrySet();

                if (checkBoxFilterNoJurnal) {
                    thisQuery = thisQuery + " AND t.ledgerGroupNoJurnalUmum = '" + searchNoJurnal + "'";
                    thisQueryCount = thisQueryCount + " AND t.ledgerGroupNoJurnalUmum = '" + searchNoJurnal + "'";
                }
                if (checkBoxFilterTanggal) {
                    thisQuery = thisQuery + " AND (t.ledgerGroupTgl BETWEEN '" + dateQueryFormat("from", searchTanggal) + "' AND '" + dateQueryFormat("to", searchTanggal) + "')";
                    thisQueryCount = thisQueryCount + " AND (t.ledgerGroupTgl BETWEEN '" + dateQueryFormat("from", searchTanggal) + "' AND '" + dateQueryFormat("to", searchTanggal) + "')";
                }
                if (checkBoxFilterCoa) {
                    thisQuery = thisQuery + " AND td.ledgerCoaId.coaAccountNo = '" + searchCoa.substring(0, searchCoa.indexOf(" ")) + "'";
                    thisQueryCount = thisQueryCount + " AND td.ledgerCoaId.coaAccountNo = '" + searchCoa.substring(0, searchCoa.indexOf(" ")) + "'";
                }

                thisQuery = thisQuery + " GROUP BY t.ledgerGroupId ORDER BY t.ledgerGroupTgl DESC, t.insertDate DESC";
                List<TrxLedgerGroup> list = crudService.findWithCreateQuery(thisQuery, first, pageSize);
                setRowCount(crudService.countWithCreateQuery(thisQueryCount));
                setPageSize(pageSize);
                return list;
            }
        };
    }

    public void insertNewRowDetail() {
        totalDebit = 0.0;
        totalKredit = 0.0;
        for (int i = 0; i < listDetail.size(); i++) {
            totalDebit = totalDebit + Double.valueOf(removeComma(listDetail.get(i).getDetailDebitFormatter()));
            totalKredit = totalKredit + Double.valueOf(removeComma(listDetail.get(i).getDetailKreditFormatter()));
        }
        totalKreditOtomatisFill = totalDebit - totalKredit;
        totalKreditOtomatisFillFormatter = formatterDecimal.format(totalKreditOtomatisFill);

        TrxLedger detail = new TrxLedger();
        detail.setLedgerId(HospitalStringUtils.generateUUID());
        detail.setLedgerKeterangan(keteranganJurnal);
        detail.setDetailDebitFormatter("0");
        detail.setDetailKreditFormatter(totalKreditOtomatisFillFormatter);
        listDetail.add(detail);

        debitOnChange();
        kreditOnChange();
    }

    public void debitOnChange() {
        totalDebit = 0.0;
        for (int i = 0; i < listDetail.size(); i++) {
            totalDebit = totalDebit + Double.valueOf(removeComma(listDetail.get(i).getDetailDebitFormatter()));
        }
        totalDebitFormatter = formatterDecimal.format(totalDebit);

        totalSelish = totalDebit - Double.valueOf(removeComma(totalKreditFormatter));
        totalSelisihFormatter = formatterDecimal.format(totalSelish);
    }

    public void kreditOnChange() {
        totalKredit = 0.0;
        for (int i = 0; i < listDetail.size(); i++) {
            totalKredit = totalKredit + Double.valueOf(removeComma(listDetail.get(i).getDetailKreditFormatter()));
        }
        totalKreditFormatter = formatterDecimal.format(totalKredit);

        totalSelish = Double.valueOf(removeComma(totalDebitFormatter)) - totalKredit;
        totalSelisihFormatter = formatterDecimal.format(totalSelish);
    }

    public void deleteDetailInsert(TrxLedger detail) {
        for (int i = 0; i < listDetail.size(); i++) {
            if (listDetail.get(i).getLedgerId().equals(detail.getLedgerId())) {
                listDetail.remove(i);
            }
        }
        debitOnChange();
        kreditOnChange();

        updateInstance("formDlgInsert:dtTableJurnal");
        updateInstance("formDlgInsert:gridFooter");
    }

    public void deleteDetailUpdate(TrxLedger detail) {
        for (int i = 0; i < listDetail.size(); i++) {
            if (listDetail.get(i).getLedgerId().equals(detail.getLedgerId())) {
                listDetail.remove(i);
            }
        }

        // Add to list delete
        for (int i = 0; i < listDetailCurrent.size(); i++) {
            if (listDetailCurrent.get(i).getLedgerId().equalsIgnoreCase(detail.getLedgerId())) {
                listDetailForDeleted.add(listDetailCurrent.get(i));
            }
        }

        debitOnChange();
        kreditOnChange();

        updateInstance("formDlgUpdate:dtTableJurnal");
        updateInstance("formDlgUpdate:gridFooter");
    }

    public void preparePosting() {
        if (selectedJurnalUmumBelumPosting.isEmpty()) {
            createMessage("WARN", "Pilih jurnal terlebih dahulu.");
        } else {
            executeInstance("PF('widDlgPosting').show()");
            updateInstance("formPosting:dlgPosting");
        }
    }

    public void posting() {
        try {
            for (int i = 0; i < selectedJurnalUmumBelumPosting.size(); i++) {
                selectedJurnalUmumBelumPosting.get(i).setLedgerGroupStatusJurnalUmum(0);
                selectedJurnalUmumBelumPosting.get(i).setLedgerGroupTglPostingJurnalUmum(new Date());
                selectedJurnalUmumBelumPosting.get(i).setUpdateDate(new Date());
                selectedJurnalUmumBelumPosting.get(i).setUpdateBy(getUsername());
            }
            crudService.updateMultiple(selectedJurnalUmumBelumPosting);

            executeInstance("PF('widDlgPosting').hide()");
            updateInstance("thisForm:thisData");
            createMessage("INFO", "Data berhasil diposting !");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<TrxLedger> setHeadToDetail(List<TrxLedger> list, TrxLedgerGroup head) {
        for (int i = 0; i < list.size(); i++) {
            list.get(i).setLedgerGroupid(head);
        }
        return list;
    }

    public void saveJurnalUmum() {
        HashMap<String, Object> param = new HashMap<>();
        if (listDetail.isEmpty()) {
            createMessage("ERROR", "Silahkan mengisi detail jurnal !");
        } else {
            Boolean statusError = false;
            for (int i = 0; i < listDetail.size(); i++) {
                if (statusError == false) {
                    listDetail.get(i).setLedgerSeq(i + 1);
                    listDetail.get(i).setLedgerBagian("JU");
                    listDetail.get(i).setLedgerTgl(tanggalJurnal);
                    listDetail.get(i).setInsertDate(new Date());
                    listDetail.get(i).setInsertBy(getUsername());
                    listDetail.get(i).setLedgerDebit(Double.valueOf(removeComma(listDetail.get(i).getDetailDebitFormatter())));
                    listDetail.get(i).setLedgerKredit(Double.valueOf(removeComma(listDetail.get(i).getDetailKreditFormatter())));

                    try {
                        param.put("coaAccountNo", listDetail.get(i).getDetailCoaNo());
                        List<TblCoa> listCoa = crudService.findWithNamedQuery("TblCoa.findByCoaAccountNo", param);
                        if (listCoa.isEmpty()) {
                            createMessage("ERROR", "Silahkan isi Acc dengan benar !");
                            statusError = true;
                        } else {
                            listDetail.get(i).setLedgerCoaId(listCoa.get(0));
                            listDetail.get(i).setLedgerDesc(listCoa.get(0).getCoaAccountNama());
                        }
                    } catch (Exception e) {
                        createMessage("ERROR", "Silahkan isi Acc dengan benar !");
                        statusError = true;
                    }
                }
            }

            if (statusError == false) {
                if (!totalDebitFormatter.equals(totalKreditFormatter)) {
                    createMessage("ERROR", "Total debit & total kredit harus sama/balance !");
                } else {
                    TrxLedgerGroup newTrxLedgerGroup = new TrxLedgerGroup();
                    newTrxLedgerGroup.setLedgerGroupId(HospitalStringUtils.generateUUID());
                    newTrxLedgerGroup.setLedgerGroupTgl(tanggalJurnal);
                    newTrxLedgerGroup.setLedgerGroupNoJurnalUmum(noJurnal);
                    newTrxLedgerGroup.setLedgerGroupKeteranganJurnalUmum(keteranganJurnal);
                    newTrxLedgerGroup.setLedgerGroupStatusJurnalUmum(2);
                    listDetail = setHeadToDetail(listDetail, newTrxLedgerGroup);
                    newTrxLedgerGroup.setInsertDate(new Date());
                    newTrxLedgerGroup.setInsertBy(getUsername());

                    newTrxLedgerGroup.setTrxLedger(listDetail);
                    crudService.update(newTrxLedgerGroup);

                    createMessage("INFO", "Jurnal umum berhasil disimpan !");
                    executeInstance("PF('widDlgInsert').hide()");
                    updateInstance("thisForm:thisData");
                }
            }
        }
    }

    public void prepareDeleteJurnalUmum(TrxLedgerGroup head) {
        if (head.getLedgerGroupStatusJurnalUmum() == 0) {
            createMessage("ERROR", "Tidak dapat menghapus jurnal yang sudah diposting !");
        } else {
            trxLedgerGroupDeleted = head;
            executeInstance("PF('widDlgDelete').show()");
            updateInstance("formDelete:dlgDelete");
        }
    }

    public void deleteJurnalUmum() {
        crudService.delete(trxLedgerGroupDeleted);
        createMessage("INFO", "Hapus data berhasil !");
        executeInstance("PF('widDlgDelete').hide()");
        updateInstance("thisForm:thisData");
    }

    public void prepareEditJurnalUmum(TrxLedgerGroup head) {
        if (head.getLedgerGroupStatusJurnalUmum() == 0) {
            createMessage("ERROR", "Tidak dapat mengubah data jurnal yang sudah diposting !");
        } else {
            noJurnal = head.getLedgerGroupNoJurnalUmum();
            keteranganJurnal = head.getLedgerGroupKeteranganJurnalUmum();
            tanggalJurnal = head.getLedgerGroupTgl();
            listDetail = new ArrayList<>();
            listDetailCurrent = new ArrayList<>();
            listDetailForDeleted = new ArrayList<>();
            trxLedgerGroupEdited = head;

            for (TrxLedger trxLedger : head.getTrxLedger()) {
                trxLedger.setDetailCoaNo(trxLedger.getLedgerCoaId().getCoaAccountNo());
                trxLedger.setDetailCoaNama(trxLedger.getLedgerCoaId().getCoaAccountNama());
                trxLedger.setDetailDebitFormatter(formatterDecimal.format(trxLedger.getLedgerDebit()));
                trxLedger.setDetailKreditFormatter(formatterDecimal.format(trxLedger.getLedgerKredit()));
                listDetail.add(trxLedger);
                listDetailCurrent.add(trxLedger);
            }
            Collections.sort(listDetail, (TrxLedger o1, TrxLedger o2) -> o1.getLedgerSeq().compareTo(o2.getLedgerSeq()));
            debitOnChange();
            kreditOnChange();

            executeInstance("PF('widDlgUpdate').show()");
            updateInstance("formDlgUpdate:dlgUpdate");
        }
    }

    public void editJurnalUmum() {
        HashMap<String, Object> param = new HashMap<>();
        if (listDetail.isEmpty()) {
            createMessage("ERROR", "Silahkan mengisi detail jurnal !");
        } else {
            Boolean statusError = false;
            for (int i = 0; i < listDetail.size(); i++) {
                if (statusError == false) {
                    listDetail.get(i).setLedgerSeq(i + 1);
                    listDetail.get(i).setLedgerBagian("JU");
                    listDetail.get(i).setLedgerTgl(tanggalJurnal);
                    listDetail.get(i).setInsertDate(new Date());
                    listDetail.get(i).setInsertBy(getUsername());
                    listDetail.get(i).setLedgerDebit(Double.valueOf(removeComma(listDetail.get(i).getDetailDebitFormatter())));
                    listDetail.get(i).setLedgerKredit(Double.valueOf(removeComma(listDetail.get(i).getDetailKreditFormatter())));

                    try {
                        param.put("coaAccountNo", listDetail.get(i).getDetailCoaNo());
                        List<TblCoa> listCoa = crudService.findWithNamedQuery("TblCoa.findByCoaAccountNo", param);
                        if (listCoa.isEmpty()) {
                            createMessage("ERROR", "Silahkan isi Acc dengan benar !");
                            statusError = true;
                        } else {
                            listDetail.get(i).setLedgerCoaId(listCoa.get(0));
                            listDetail.get(i).setLedgerDesc(listCoa.get(0).getCoaAccountNama());
                        }
                    } catch (Exception e) {
                        createMessage("ERROR", "Silahkan isi Acc dengan benar !");
                        statusError = true;
                    }
                }
            }

            if (statusError == false) {
                if (!totalDebitFormatter.equals(totalKreditFormatter)) {
                    createMessage("ERROR", "Total debit & total kredit harus sama/balance !");
                } else {
                    param = new HashMap<>();
                    param.put("ledgerGroupId", trxLedgerGroupEdited.getLedgerGroupId());
                    List<TrxLedgerGroup> thisTrxLedgerGroup = crudService.findWithNamedQuery("TrxLedgerGroup.findByLedgerGroupId", param);
                    if (thisTrxLedgerGroup.isEmpty()) {
                        createMessage("ERROR", "Please try again !");
                        executeInstance("PF('widDlgUpdate').hide()");
                        updateInstance("thisForm:thisData");
                    } else {
                        TrxLedgerGroup editTrxLedgerGroup = thisTrxLedgerGroup.get(0);
                        editTrxLedgerGroup.setLedgerGroupTgl(tanggalJurnal);
                        editTrxLedgerGroup.setLedgerGroupNoJurnalUmum(noJurnal);
                        editTrxLedgerGroup.setLedgerGroupKeteranganJurnalUmum(keteranganJurnal);

                        listDetail = setHeadToDetail(listDetail, editTrxLedgerGroup);
                        editTrxLedgerGroup.setUpdateDate(new Date());
                        editTrxLedgerGroup.setUpdateBy(getUsername());

                        editTrxLedgerGroup.setTrxLedger(listDetail);
                        crudService.update(editTrxLedgerGroup);

                        // Delete detail
                        for (int i = 0; i < listDetailForDeleted.size(); i++) {
                            crudService.delete(listDetailForDeleted.get(i));
                        }

                        createMessage("INFO", "Jurnal umum berhasil diubah !");
                        executeInstance("PF('widDlgUpdate').hide()");
                        updateInstance("thisForm:thisData");
                    }
                }
            }
        }
    }

    public LazyDataModel<TrxLedgerGroup> getLazyTrxLedgerGroupJurnalUmum() {
        return lazyTrxLedgerGroupJurnalUmum;
    }

    public void setLazyTrxLedgerGroupJurnalUmum(LazyDataModel<TrxLedgerGroup> lazyTrxLedgerGroupJurnalUmum) {
        this.lazyTrxLedgerGroupJurnalUmum = lazyTrxLedgerGroupJurnalUmum;
    }

    public LazyDataModel<TrxLedgerGroup> getLazyTrxLedgerGroupJurnalUmumBelumPosting() {
        return lazyTrxLedgerGroupJurnalUmumBelumPosting;
    }

    public void setLazyTrxLedgerGroupJurnalUmumBelumPosting(LazyDataModel<TrxLedgerGroup> lazyTrxLedgerGroupJurnalUmumBelumPosting) {
        this.lazyTrxLedgerGroupJurnalUmumBelumPosting = lazyTrxLedgerGroupJurnalUmumBelumPosting;
    }

    public List<TrxLedgerGroup> getSelectedJurnalUmumBelumPosting() {
        return selectedJurnalUmumBelumPosting;
    }

    public void setSelectedJurnalUmumBelumPosting(List<TrxLedgerGroup> selectedJurnalUmumBelumPosting) {
        this.selectedJurnalUmumBelumPosting = selectedJurnalUmumBelumPosting;
    }

    public Date getTanggalJurnal() {
        return tanggalJurnal;
    }

    public void setTanggalJurnal(Date tanggalJurnal) {
        this.tanggalJurnal = tanggalJurnal;
    }

    public String getNoJurnal() {
        return noJurnal;
    }

    public void setNoJurnal(String noJurnal) {
        this.noJurnal = noJurnal;
    }

    public String getKeteranganJurnal() {
        return keteranganJurnal;
    }

    public void setKeteranganJurnal(String keteranganJurnal) {
        this.keteranganJurnal = keteranganJurnal;
    }

    public List<TrxLedger> getListDetail() {
        return listDetail;
    }

    public void setListDetail(List<TrxLedger> listDetail) {
        this.listDetail = listDetail;
    }

    public Double getTotalDebit() {
        return totalDebit;
    }

    public void setTotalDebit(Double totalDebit) {
        this.totalDebit = totalDebit;
    }

    public Double getTotalKredit() {
        return totalKredit;
    }

    public void setTotalKredit(Double totalKredit) {
        this.totalKredit = totalKredit;
    }

    public String getTotalDebitFormatter() {
        return totalDebitFormatter;
    }

    public void setTotalDebitFormatter(String totalDebitFormatter) {
        this.totalDebitFormatter = totalDebitFormatter;
    }

    public String getTotalKreditFormatter() {
        return totalKreditFormatter;
    }

    public void setTotalKreditFormatter(String totalKreditFormatter) {
        this.totalKreditFormatter = totalKreditFormatter;
    }

    public List<TrxLedger> getListDetailForTable1() {
        return listDetailForTable1;
    }

    public void setListDetailForTable1(List<TrxLedger> listDetailForTable1) {
        this.listDetailForTable1 = listDetailForTable1;
    }

    public List<TrxLedger> getListDetailForTable2() {
        return listDetailForTable2;
    }

    public void setListDetailForTable2(List<TrxLedger> listDetailForTable2) {
        this.listDetailForTable2 = listDetailForTable2;
    }

    public TrxLedgerGroup getTrxLedgerGroupDeleted() {
        return trxLedgerGroupDeleted;
    }

    public void setTrxLedgerGroupDeleted(TrxLedgerGroup trxLedgerGroupDeleted) {
        this.trxLedgerGroupDeleted = trxLedgerGroupDeleted;
    }

    public TrxLedgerGroup getTrxLedgerGroupEdited() {
        return trxLedgerGroupEdited;
    }

    public void setTrxLedgerGroupEdited(TrxLedgerGroup trxLedgerGroupEdited) {
        this.trxLedgerGroupEdited = trxLedgerGroupEdited;
    }

    public List<TrxLedger> getListDetailCurrent() {
        return listDetailCurrent;
    }

    public void setListDetailCurrent(List<TrxLedger> listDetailCurrent) {
        this.listDetailCurrent = listDetailCurrent;
    }

    public List<TrxLedger> getListDetailForDeleted() {
        return listDetailForDeleted;
    }

    public void setListDetailForDeleted(List<TrxLedger> listDetailForDeleted) {
        this.listDetailForDeleted = listDetailForDeleted;
    }

    public String getTotalSelisihFormatter() {
        return totalSelisihFormatter;
    }

    public void setTotalSelisihFormatter(String totalSelisihFormatter) {
        this.totalSelisihFormatter = totalSelisihFormatter;
    }

    public Double getTotalSelish() {
        return totalSelish;
    }

    public void setTotalSelish(Double totalSelish) {
        this.totalSelish = totalSelish;
    }

    public Boolean getToggleButtonFilter() {
        return toggleButtonFilter;
    }

    public void setToggleButtonFilter(Boolean toggleButtonFilter) {
        this.toggleButtonFilter = toggleButtonFilter;
    }

    public Boolean getCheckBoxFilterNoJurnal() {
        return checkBoxFilterNoJurnal;
    }

    public void setCheckBoxFilterNoJurnal(Boolean checkBoxFilterNoJurnal) {
        this.checkBoxFilterNoJurnal = checkBoxFilterNoJurnal;
    }

    public Boolean getCheckBoxFilterTanggal() {
        return checkBoxFilterTanggal;
    }

    public void setCheckBoxFilterTanggal(Boolean checkBoxFilterTanggal) {
        this.checkBoxFilterTanggal = checkBoxFilterTanggal;
    }

    public Boolean getCheckBoxFilterCoa() {
        return checkBoxFilterCoa;
    }

    public void setCheckBoxFilterCoa(Boolean checkBoxFilterCoa) {
        this.checkBoxFilterCoa = checkBoxFilterCoa;
    }

    public Boolean getToggleFilterNoJurnal() {
        return toggleFilterNoJurnal;
    }

    public void setToggleFilterNoJurnal(Boolean toggleFilterNoJurnal) {
        this.toggleFilterNoJurnal = toggleFilterNoJurnal;
    }

    public Boolean getToggleFilterTanggal() {
        return toggleFilterTanggal;
    }

    public void setToggleFilterTanggal(Boolean toggleFilterTanggal) {
        this.toggleFilterTanggal = toggleFilterTanggal;
    }

    public Boolean getToggleFilterCoa() {
        return toggleFilterCoa;
    }

    public void setToggleFilterCoa(Boolean toggleFilterCoa) {
        this.toggleFilterCoa = toggleFilterCoa;
    }

    public String getSearchNoJurnal() {
        return searchNoJurnal;
    }

    public void setSearchNoJurnal(String searchNoJurnal) {
        this.searchNoJurnal = searchNoJurnal;
    }

    public Date getSearchTanggal() {
        return searchTanggal;
    }

    public void setSearchTanggal(Date searchTanggal) {
        this.searchTanggal = searchTanggal;
    }

    public String getSearchCoa() {
        return searchCoa;
    }

    public void setSearchCoa(String searchCoa) {
        this.searchCoa = searchCoa;
    }

}
