/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital.controllers;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.MappedSuperclass;
import javax.servlet.http.HttpSession;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Erick
 */
@MappedSuperclass
public class BaseController implements Serializable {

    public ResourceBundle parameter = ResourceBundle.getBundle("com.parameter");
    private HttpSession session;
    private String username;

    public String pattern1 = "dd MMMM yyyy";
    public SimpleDateFormat format1 = new SimpleDateFormat(pattern1);
    public String pattern2 = "yyyy-MM-dd";
    public SimpleDateFormat format2 = new SimpleDateFormat(pattern2);
    public String pattern3 = "dd/ MMM/ yyyy";
    public SimpleDateFormat format3 = new SimpleDateFormat(pattern3);
    public String pattern4 = "yyyyMMddhhmmss";
    public SimpleDateFormat format4 = new SimpleDateFormat(pattern4);
    public String pattern5 = "yyyy-MM-dd HH:mm:ss";
    public SimpleDateFormat format5 = new SimpleDateFormat(pattern5);
    public String pattern6 = "dd-MM-yyyy HH:mm";
    public SimpleDateFormat format6 = new SimpleDateFormat(pattern6);
    public String pattern7 = "HHmm";
    public SimpleDateFormat format7 = new SimpleDateFormat(pattern7);
    public String pattern8 = "HH:mm:ss";
    public SimpleDateFormat format8 = new SimpleDateFormat(pattern8);
    public String pattern9 = "yyyy";
    public SimpleDateFormat format9 = new SimpleDateFormat(pattern9);
    public String pattern10 = "MMM-yyyy";
    public SimpleDateFormat format10 = new SimpleDateFormat(pattern10);

    public DecimalFormat formatterDecimal = new DecimalFormat("###,###");

    public BaseController() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        session = (HttpSession) facesContext.getExternalContext().getSession(true);
        username = (String) session.getAttribute("usrName");
    }

    public void executeInstance(String ins) {
        RequestContext.getCurrentInstance().execute(ins);
    }

    public void updateInstance(String ins) {
        RequestContext.getCurrentInstance().update(ins);
    }

    public void createMessage(String sevInfo, String msg) {
        if (sevInfo.equalsIgnoreCase("INFO")) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, msg, msg));
        } else if (sevInfo.equalsIgnoreCase("WARN")) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, msg, msg));
        } else if (sevInfo.equalsIgnoreCase("ERROR")) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, msg, msg));
        } else if (sevInfo.equalsIgnoreCase("FATAL")) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, msg, msg));
        }
    }

    public String dateQueryFormat(String category, Date selectedTanggal) {
        Calendar thisCalendar = Calendar.getInstance();
        thisCalendar.setTime(selectedTanggal);

        if (category.equalsIgnoreCase("from")) {
            thisCalendar.set(Calendar.HOUR_OF_DAY, 0);
            thisCalendar.set(Calendar.MINUTE, 0);
            thisCalendar.set(Calendar.SECOND, 0);
            thisCalendar.set(Calendar.MILLISECOND, 0);
        } else {
            thisCalendar.set(Calendar.HOUR_OF_DAY, 23);
            thisCalendar.set(Calendar.MINUTE, 59);
            thisCalendar.set(Calendar.SECOND, 59);
            thisCalendar.set(Calendar.MILLISECOND, 999);
        }
        return format5.format(thisCalendar.getTime());
    }

    public String fixedLengthString(String str, int leng) {
        if (str.length() > leng) {
            return str.substring(0, leng);
        } else {
            for (int i = str.length(); i <= leng; i++) {
                str += " ";
            }
            return str;
        }
    }

    public String checkNull(String str) {
        if ((str == null) || (str.equalsIgnoreCase("null"))) {
            return "";
        } else {
            return str;
        }
    }

    public Double checkNull(Double str) {
        if ((str == null) || (str.toString().equalsIgnoreCase("null"))) {
            return 0.0;
        } else {
            return str;
        }
    }

    public String removeComma(String str) {
        return str.replace(",", "");
    }

    public HttpSession getSession() {
        return session;
    }

    public void setSession(HttpSession session) {
        this.session = session;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public ResourceBundle getParameter() {
        return parameter;
    }

    public void setParameter(ResourceBundle parameter) {
        this.parameter = parameter;
    }

    public String getPattern1() {
        return pattern1;
    }

    public void setPattern1(String pattern1) {
        this.pattern1 = pattern1;
    }

    public SimpleDateFormat getFormat1() {
        return format1;
    }

    public void setFormat1(SimpleDateFormat format1) {
        this.format1 = format1;
    }

    public String getPattern2() {
        return pattern2;
    }

    public void setPattern2(String pattern2) {
        this.pattern2 = pattern2;
    }

    public SimpleDateFormat getFormat2() {
        return format2;
    }

    public void setFormat2(SimpleDateFormat format2) {
        this.format2 = format2;
    }

    public String getPattern3() {
        return pattern3;
    }

    public void setPattern3(String pattern3) {
        this.pattern3 = pattern3;
    }

    public SimpleDateFormat getFormat3() {
        return format3;
    }

    public void setFormat3(SimpleDateFormat format3) {
        this.format3 = format3;
    }

    public String getPattern4() {
        return pattern4;
    }

    public void setPattern4(String pattern4) {
        this.pattern4 = pattern4;
    }

    public SimpleDateFormat getFormat4() {
        return format4;
    }

    public void setFormat4(SimpleDateFormat format4) {
        this.format4 = format4;
    }

    public String getPattern5() {
        return pattern5;
    }

    public void setPattern5(String pattern5) {
        this.pattern5 = pattern5;
    }

    public SimpleDateFormat getFormat5() {
        return format5;
    }

    public void setFormat5(SimpleDateFormat format5) {
        this.format5 = format5;
    }

    public String getPattern6() {
        return pattern6;
    }

    public void setPattern6(String pattern6) {
        this.pattern6 = pattern6;
    }

    public SimpleDateFormat getFormat6() {
        return format6;
    }

    public void setFormat6(SimpleDateFormat format6) {
        this.format6 = format6;
    }

    public DecimalFormat getFormatterDecimal() {
        return formatterDecimal;
    }

    public void setFormatterDecimal(DecimalFormat formatterDecimal) {
        this.formatterDecimal = formatterDecimal;
    }

    public String getPattern7() {
        return pattern7;
    }

    public void setPattern7(String pattern7) {
        this.pattern7 = pattern7;
    }

    public SimpleDateFormat getFormat7() {
        return format7;
    }

    public void setFormat7(SimpleDateFormat format7) {
        this.format7 = format7;
    }

    public String getPattern8() {
        return pattern8;
    }

    public void setPattern8(String pattern8) {
        this.pattern8 = pattern8;
    }

    public SimpleDateFormat getFormat8() {
        return format8;
    }

    public void setFormat8(SimpleDateFormat format8) {
        this.format8 = format8;
    }

    public String getPattern9() {
        return pattern9;
    }

    public void setPattern9(String pattern9) {
        this.pattern9 = pattern9;
    }

    public SimpleDateFormat getFormat9() {
        return format9;
    }

    public void setFormat9(SimpleDateFormat format9) {
        this.format9 = format9;
    }

    public String getPattern10() {
        return pattern10;
    }

    public void setPattern10(String pattern10) {
        this.pattern10 = pattern10;
    }

    public SimpleDateFormat getFormat10() {
        return format10;
    }

    public void setFormat10(SimpleDateFormat format10) {
        this.format10 = format10;
    }

}
