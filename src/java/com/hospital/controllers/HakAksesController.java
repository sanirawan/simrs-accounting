/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital.controllers;

import com.hospital.crud.CrudService;
import com.hospital.models.TblHakAkses;
import com.hospital.models.TblMenu;
import com.hospital.models.TblRole;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DualListModel;

/**
 *
 * @author work
 */
@ManagedBean(name = "hakAksesController")
@ApplicationScoped
public class HakAksesController extends BaseController implements Serializable {

    @EJB
    private CrudService crudService;
    private List<TblMenu> listMenu = new ArrayList<>();
    private List<TblMenu> listMenuAkses = new ArrayList<>();
    private DualListModel<TblMenu> menu;
    private List<TblRole> listRole = new ArrayList<>();
    private Long selectedRole;

    /**
     * Creates a new instance of HakAksesController
     */
    public HakAksesController() {
    }

    @PostConstruct
    public void init() {
        loadTable();
    }

    private void loadTable() {
        listRole = crudService.findWithNamedQuery("TblRole.findAll");
        listMenu = crudService.findByNativeQuery("SELECT * from tbl_menu", TblMenu.class);
        menu = new DualListModel<>(listMenu, new ArrayList<TblMenu>());
    }

    public void findMenuByRole(SelectEvent event) {
        listMenu = crudService.findByNativeQuery("select * from tbl_menu where menu_id not in (select hak_akes_menu_id from tbl_hak_akses where hak_akses_role_id = " + selectedRole + ")", TblMenu.class);
        listMenuAkses = crudService.findByNativeQuery("select * from tbl_menu where menu_id in (select hak_akes_menu_id from tbl_hak_akses where hak_akses_role_id = " + selectedRole + ")", TblMenu.class);
        menu = new DualListModel<>(listMenu, listMenuAkses);
    }

    public void saveHakAkses() {
        HashMap<String, Object> param = new HashMap<>();
        param.put("roleId", selectedRole);
        List<TblRole> list = crudService.findWithNamedQuery("TblRole.findByRoleId", param);
        crudService.updateWithNativeQuery("DELETE FROM tbl_hak_akses WHERE hak_akses_role_id = " + selectedRole);
        for (int i = 0; i < menu.getTarget().size(); i++) {
            TblMenu target = menu.getTarget().get(i);
//            List<TblMenu> menus = crudService.findByNativeQuery("SELECT * FROM tbl_menu where menu_id = " + target, TblMenu.class);
            TblHakAkses hakAkses = new TblHakAkses();
            hakAkses.setHakAkesMenuId(target);
            hakAkses.setHakAksesRoleId(list.get(0));
            crudService.update(hakAkses);
        }
        createMessage("INFO", "Hak akses berhasil disimpan.");
    }
    
    public boolean verifyHakAkses(String hakId) {

        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        HttpSession session = req.getSession(true);

        String roleId = (String) session.getAttribute("roleId");

        Object group;
        String groupId;
        String a = " ";

        List<TblHakAkses> listAkses = crudService.findByNativeQuery("Select hak_akses_id from tbl_hak_akses where hak_akses_role_id = " + roleId + " AND hak_akes_menu_id IN(" + hakId + ")");

        if (listAkses.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

//    public boolean verifyHakAkses(String hakId) {
//
//        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
//        HttpSession session = req.getSession(true);
//
//        String uname = (String) session.getAttribute("usrName");
//
//        Object group;
//        String groupId;
//        String a = " ";
//
//        listAkses = crudService.findByNativeQuery("Select DISTINCT accs_id as ID from usr_accessright_tj where username = '" + uname + "' AND accessright_tj_id = " + hakId);
//
//        if (listAkses.isEmpty()) {
//            return false;
//        } else {
//            return true;
//        }
//    }
    public DualListModel<TblMenu> getMenu() {
        return menu;
    }

    public void setMenu(DualListModel<TblMenu> menu) {
        this.menu = menu;
    }

    public List<TblRole> getListRole() {
        return listRole;
    }

    public void setListRole(List<TblRole> listRole) {
        this.listRole = listRole;
    }

    public Long getSelectedRole() {
        return selectedRole;
    }

    public void setSelectedRole(Long selectedRole) {
        this.selectedRole = selectedRole;
    }
}
