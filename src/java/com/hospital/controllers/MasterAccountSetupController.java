/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital.controllers;

import com.hospital.crud.CrudService;
import com.hospital.models.TblAccountSetup;
import com.hospital.models.TblAccountSpecial;
import com.hospital.models.TblCoa;
import com.hospital.models.TblUser;
import com.hospital.utils.HospitalStringUtils;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author arisoft
 */
@ManagedBean(name = "masterAccountSetupController")
@ViewScoped
public class MasterAccountSetupController extends BaseController {

    @EJB
    private CrudService crudService;
    private LazyDataModel<TblAccountSetup> lazyTblAccountSetup = null;
    private TblAccountSetup tblAccountSetup = new TblAccountSetup();
    private String accountNo;

    /**
     * Creates a new instance of MasterSpecialAccountController
     */
    public MasterAccountSetupController() {
    }

    public void refreshDataTable() {
        loadTable();
    }

    @PostConstruct
    public void init() {
        loadTable();
    }

    private void loadTable() {
        lazyTblAccountSetup = new LazyDataModel<TblAccountSetup>() {
            @Override
            public List<TblAccountSetup> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> map) {
                String thisQuery = "SELECT t FROM TblAccountSetup t WHERE 1 = 1";
                Set<Map.Entry<String, Object>> rawParameters = map.entrySet();

                Boolean isSearch = false;
                for (Map.Entry<String, Object> entry : rawParameters) {
                    isSearch = true;
                    thisQuery = thisQuery + " AND LOWER(t." + entry.getKey() + ") LIKE '%" + entry.getValue().toString().toLowerCase() + "%'";
                }

                if (sortField != null) {
                    String sortMethod = "DESC";
                    if (sortOrder.toString().equalsIgnoreCase("ASCENDING")) {
                        sortMethod = "ASC";
                    }
                    thisQuery = thisQuery + " ORDER BY t." + sortField + " " + sortMethod;
                }
                List<TblAccountSetup> list = crudService.findWithCreateQuery(thisQuery, first, pageSize);
                if (isSearch) {
                    setRowCount(list.size());
                } else {
                    int rowCount = crudService.countWithNamedQuery("TblAccountSpecial.countAll");
                    setRowCount(rowCount);
                }
                setPageSize(pageSize);
                return list;
            }

            @Override
            public TblAccountSetup getRowData(String rowKey) {
                HashMap<String, Object> param = new HashMap<>();
                param.put("accountSetupId", rowKey);
                List<TblAccountSetup> list = crudService.findWithNamedQuery("TblAccountSetup.findByAccountSetupId", param);
                if (list.isEmpty()) {
                    return null;
                } else {
                    return list.get(0);
                }
            }

            @Override
            public Object getRowKey(TblAccountSetup obj) {
                return obj.getAccountSetupId();
            }
        };
    }

    public void doInsert() {
        tblAccountSetup = new TblAccountSetup();
        accountNo = "";
        executeInstance("PF('widDlgInsert').show()");
        updateInstance("formDlgInsert:pgridInsert");

    }

    public void doUpdate() {
        if (tblAccountSetup == null) {
            createMessage("WARN", "Pilih data terlebih dahulu.");
        } else {
            accountNo = tblAccountSetup.getAccountSetupCoaId().getCoaAccountNo() + " - " + tblAccountSetup.getAccountSetupCoaId().getCoaAccountNama();
            executeInstance("PF('widDlgUpdate').show()");
            updateInstance("formDlgUpdate:pgridUpdate");
        }
    }

    public void insert() {
        tblAccountSetup.setAccountSetupId(HospitalStringUtils.generateUUID());
        String account = accountNo.substring(0, accountNo.indexOf(" "));
        HashMap<String, Object> param = new HashMap<>();
        param.put("coaAccountNo", account);
        List<TblCoa> list = crudService.findWithNamedQuery("TblCoa.findByCoaAccountNo", param);
        tblAccountSetup.setAccountSetupCoaId(list.get(0));
        crudService.update(tblAccountSetup);
        tblAccountSetup = new TblAccountSetup();
        createMessage("INFO", "Account setup ditambah.");
        executeInstance("PF('widDlgInsert').hide()");
        updateInstance("thisForm:thisData");
    }
    
    public void update() {
        String account = accountNo.substring(0, accountNo.indexOf(" "));
        HashMap<String, Object> param = new HashMap<>();
        param.put("coaAccountNo", account);
        List<TblCoa> list = crudService.findWithNamedQuery("TblCoa.findByCoaAccountNo", param);
        tblAccountSetup.setAccountSetupCoaId(list.get(0));
        crudService.update(tblAccountSetup);
        tblAccountSetup = new TblAccountSetup();
        createMessage("INFO", "Account setup diubah.");
        executeInstance("PF('widDlgUpdate').hide()");
        updateInstance("thisForm:thisData");
    }
    

    public List<String> findNoAccount(String query) {
        return crudService.findByNativeQuery("SELECT CONCAT(coa_account_no,' : ', coa_account_nama) FROM tbl_coa WHERE coa_account_no LIKE '" + query.toLowerCase() + "%' OR upper(coa_account_nama) LIKE '" + query.toUpperCase() + "%'");
    }
    
    public void doDelete() {
        if (tblAccountSetup == null) {
            createMessage("WARN", "Pilih data terlebih dahulu.");
        } else {
            updateInstance(":formCfmDelete:cfmDelete");
            executeInstance("PF('widCfmDelete').show()");
        }
    }

    public void delete() {
        crudService.delete(tblAccountSetup);
        createMessage("INFO", "Hapus data berhasil.");
        executeInstance("PF('widCfmDelete').hide()");
        updateInstance("thisForm:thisData");
    }

    public LazyDataModel<TblAccountSetup> getLazyTblAccountSetup() {
        return lazyTblAccountSetup;
    }

    public void setLazyTblAccountSetup(LazyDataModel<TblAccountSetup> lazyTblAccountSetup) {
        this.lazyTblAccountSetup = lazyTblAccountSetup;
    }

    public TblAccountSetup getTblAccountSetup() {
        return tblAccountSetup;
    }

    public void setTblAccountSetup(TblAccountSetup tblAccountSetup) {
        this.tblAccountSetup = tblAccountSetup;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

}
