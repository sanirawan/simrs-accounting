/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital.controllers;

import com.hospital.crud.CrudService;
import com.hospital.models.BalanceSheet;
import com.hospital.models.TblAccountSpecial;
import com.hospital.models.BukuKas;
import com.hospital.utils.HospitalStringUtils;
import static com.hospital.utils.HospitalStringUtils.generateParameterTanggalReport;
import com.hospital.utils.ReportGenerator;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author arisoft
 */
@ManagedBean(name = "bukuKasController")
@ViewScoped
public class BukuKasController extends BaseController implements Serializable {

    @EJB
    private CrudService crudService;
    private List<BukuKas> listBukuKas = null;
    private List<BukuKas> listBukuKasBank = null;
    private List<TblAccountSpecial> listAccountKas = new ArrayList<>();
    private List<TblAccountSpecial> listAccountBank = new ArrayList<>();
    private String saldoAwalKas;
    private String saldoAwalBank;
    private String saldoAkhirKas;
    private String saldoAkhirBank;

    /**
     * =============== PENCARIAN DATA
     */
    private Boolean toggleButtonFilter = true;

    private Boolean checkBoxFilterTanggal = false;
    private Boolean checkBoxFilterUnit = false;

    private Boolean toggleFilterTanggal = true;
    private Boolean toggleFilterUnit = true;

    private Date selectedFilterTanggal;
    private String selectedFilterUnit = "";
    private String searchNoRef = "";

    /**
     * Creates a new instance of LedgerViewController
     */
    public BukuKasController() {
    }

    @PostConstruct
    public void init() {
        loadAccountSpecial();
        loadTable();
    }

    private void loadAccountSpecial() {
        HashMap<String, Object> param = new HashMap<>();
        param.put("accountSpecialFunction", "Account Kas");
        listAccountKas = crudService.findWithNamedQuery("TblAccountSpecial.findByAccountSpecialFunction", param);
        param = new HashMap<>();
        param.put("accountSpecialFunction", "Account Bank");
        listAccountBank = crudService.findWithNamedQuery("TblAccountSpecial.findByAccountSpecialFunction", param);
    }

//    private void loadTable() {
//        lazyBukuKas = new LazyDataModel<BukuKas>() {
//            @Override
//            public List<BukuKas> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> map) {
//                String tahunBulan = new SimpleDateFormat("yyyy-MM").format(new Date());
//                
//                List<BukuKas> list = crudService.findByNativeQuery("select * from trx_ledger tl inner join tbl_coa tc on (tl.ledger_coa_id = tc.coa_id) where ledger_no_ref in (select tl.ledger_no_ref from trx_ledger tl inner join tbl_coa tc on (tl.ledger_coa_id = tc.coa_id) where ledger_tgl::text LIKE '" + tahunBulan + "%' and tc.coa_account_no LIKE '" + listAccountKas.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "%') and tc.coa_account_no NOT LIKE '" + listAccountKas.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "%' order by TO_CHAR(ledger_tgl, 'YYYYMMDD') DESC, ledger_no_ref, ledger_seq asc", BukuKas.class, first, first);
//                int rowCount = crudService.nativeQueryReturnInteger("select COUNT(*) from trx_ledger tl inner join tbl_coa tc on (tl.ledger_coa_id = tc.coa_id) where ledger_no_ref in (select tl.ledger_no_ref from trx_ledger tl inner join tbl_coa tc on (tl.ledger_coa_id = tc.coa_id) where ledger_tgl::text LIKE '" + tahunBulan + "%' and tc.coa_account_no LIKE '" + listAccountKas.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "%') and tc.coa_account_no NOT LIKE '" + listAccountKas.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "%'");
//                setRowCount(rowCount);
//                setPageSize(pageSize);
//                return list;
//            }
//
//            @Override
//            public BukuKas getRowData(String rowKey) {
//                HashMap<String, Object> param = new HashMap<>();
//                param.put("ledgerId", rowKey);
//                List<BukuKas> list = crudService.findWithNamedQuery("BukuKas.findByLedgerId", param);
//                if (list.isEmpty()) {
//                    return null;
//                } else {
//                    return list.get(0);
//                }
//            }
//
//            @Override
//            public Object getRowKey(BukuKas obj) {
//                return obj.getLedgerId();
//            }
//        };
//        lazyBukuKasBank = new LazyDataModel<BukuKas>() {
//            @Override
//            public List<BukuKas> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> map) {
//                List<BukuKas> list = crudService.findByNativeQuery("select * from trx_ledger tl inner join tbl_coa tc on (tl.ledger_coa_id = tc.coa_id) where ledger_no_ref in (select tl.ledger_no_ref from trx_ledger tl inner join tbl_coa tc on (tl.ledger_coa_id = tc.coa_id) where tc.coa_account_no LIKE '" + listAccountBank.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "%') and tc.coa_account_no NOT LIKE '" + listAccountBank.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "%' order by TO_CHAR(ledger_tgl, 'YYYYMMDD') DESC, ledger_no_ref, ledger_seq asc", BukuKas.class, first, first);
//                int rowCount = crudService.nativeQueryReturnInteger("select COUNT(*) from trx_ledger tl inner join tbl_coa tc on (tl.ledger_coa_id = tc.coa_id) where ledger_no_ref in (select tl.ledger_no_ref from trx_ledger tl inner join tbl_coa tc on (tl.ledger_coa_id = tc.coa_id) where tc.coa_account_no LIKE '" + listAccountBank.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "%') and tc.coa_account_no NOT LIKE '" + listAccountBank.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "%'");
//                setRowCount(rowCount);
//                setPageSize(pageSize);
//                return list;
//            }
//
//            @Override
//            public BukuKas getRowData(String rowKey) {
//                HashMap<String, Object> param = new HashMap<>();
//                param.put("ledgerId", rowKey);
//                List<BukuKas> list = crudService.findWithNamedQuery("BukuKas.findByLedgerId", param);
//                if (list.isEmpty()) {
//                    return null;
//                } else {
//                    return list.get(0);
//                }
//            }
//
//            @Override
//            public Object getRowKey(BukuKas obj) {
//                return obj.getLedgerId();
//            }
//        };
//    }
    public List<String> findNoRef(String query) {
        return crudService.findByNativeQuery("SELECT ledger_no_ref FROM trx_ledger WHERE UPPER(ledger_no_ref) LIKE '%" + query.toUpperCase() + "%' GROUP BY ledger_no_ref");
    }

//    public void searchByNoRef() {
//        lazyBukuKas = new LazyDataModel<BukuKas>() {
//            @Override
//            public List<BukuKas> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> map) {
//                String thisQuery = "SELECT t FROM BukuKas t WHERE t.ledgerNoRef = '" + searchNoRef + "' ORDER BY t.ledgerTgl DESC, t.ledgerSeq ASC";
//                String countQuery = "SELECT COUNT(t) FROM BukuKas t WHERE t.ledgerNoRef = '" + searchNoRef + "'";
//                Set<Map.Entry<String, Object>> rawParameters = map.entrySet();
//                List<BukuKas> list = crudService.findWithCreateQuery(thisQuery, first, pageSize);
//                setRowCount(crudService.countWithCreateQuery(countQuery));
//                setPageSize(pageSize);
//                return list;
//            }
//        };
//    }
    public void listenerCheckboxFilter() {
        if (checkBoxFilterTanggal) {
            toggleFilterTanggal = false;
        } else {
            toggleFilterTanggal = true;
        }
        if (checkBoxFilterUnit) {
            toggleFilterUnit = false;
        } else {
            toggleFilterUnit = true;
        }

        if (checkBoxFilterTanggal || checkBoxFilterUnit) {
            toggleButtonFilter = false;
        } else {
            toggleButtonFilter = true;
        }
    }

    public void loadTable() {
        String tahunBulan = new SimpleDateFormat("yyyy-MM").format(new Date());
        String tahun = new SimpleDateFormat("yyyy").format(new Date());

        String thisQuery = "";
        String thisQueryCount = "select COUNT(*) from trx_ledger tl inner join tbl_coa tc on (tl.ledger_coa_id = tc.coa_id) where ledger_no_ref in (select tl.ledger_no_ref from trx_ledger tl inner join tbl_coa tc on (tl.ledger_coa_id = tc.coa_id) where tc.coa_account_no LIKE '" + listAccountKas.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "%') and tc.coa_account_no NOT LIKE '" + listAccountKas.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "%'";

        if (checkBoxFilterTanggal) {
            tahunBulan = new SimpleDateFormat("yyyy-MM").format(selectedFilterTanggal);
            tahun = new SimpleDateFormat("yyyy").format(new Date());
            
            List<BalanceSheet> listSaldoAwal = crudService.findByNativeQuery("SELECT uuid_in(md5(random()::text || now()::text)::cstring) as balance_sheet_id, countingsaldoawal('" + listAccountKas.get(0).getAccountSpecialCoaId().getCoaId() + "', '" + tahun + "-01-01', '" + tahunBulan + "-01') as balance_sheet_saldo_awal", BalanceSheet.class);
            thisQuery = "select tl.*, countingsaldoakhir(" + listSaldoAwal.get(0).getBalanceSheetSaldoAwal() + ", '" + listAccountKas.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "', '" + tahunBulan + "-01', tl.ledger_tgl) as saldo_akhir from trx_ledger tl inner join tbl_coa tc on (tl.ledger_coa_id = tc.coa_id) inner join trx_ledger_group tlg on (tl.ledger_ledger_group_id = tlg.ledger_group_id) where tlg.ledger_group_status_jurnal_umum = 0 AND ledger_no_ref in (select tl.ledger_no_ref from trx_ledger tl inner join tbl_coa tc on (tl.ledger_coa_id = tc.coa_id) where tc.coa_account_no LIKE '" + listAccountKas.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "%') and tc.coa_account_no NOT LIKE '" + listAccountKas.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "%' ";
            thisQuery += " and ledger_tgl::text LIKE '" + tahunBulan + "%'";
            thisQueryCount += " and ledger_tgl::text LIKE '" + tahunBulan + "%'";
            saldoAwalKas = HospitalStringUtils.addCommaFromDouble(listSaldoAwal.get(0).getBalanceSheetSaldoAwal());
        } else {
            List<BalanceSheet> listSaldoAwal = crudService.findByNativeQuery("SELECT uuid_in(md5(random()::text || now()::text)::cstring) as balance_sheet_id, countingsaldoawal('" + listAccountKas.get(0).getAccountSpecialCoaId().getCoaId() + "', '" + tahun + "-01-01', '" + tahunBulan + "-01') as balance_sheet_saldo_awal", BalanceSheet.class);
            thisQuery = "select tl.*, countingsaldoakhir(" + listSaldoAwal.get(0).getBalanceSheetSaldoAwal() + ", '" + listAccountKas.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "', '" + tahunBulan + "-01', tl.ledger_tgl) as saldo_akhir from trx_ledger tl inner join tbl_coa tc on (tl.ledger_coa_id = tc.coa_id) inner join trx_ledger_group tlg on (tl.ledger_ledger_group_id = tlg.ledger_group_id) where tlg.ledger_group_status_jurnal_umum = 0 AND ledger_no_ref in (select tl.ledger_no_ref from trx_ledger tl inner join tbl_coa tc on (tl.ledger_coa_id = tc.coa_id) where tc.coa_account_no LIKE '" + listAccountKas.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "%') and tc.coa_account_no NOT LIKE '" + listAccountKas.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "%' ";
            thisQuery += " and ledger_tgl::text LIKE '" + tahunBulan + "%'";
            thisQueryCount += " and ledger_tgl::text LIKE '" + tahunBulan + "%'";
            saldoAwalKas = HospitalStringUtils.addCommaFromDouble(listSaldoAwal.get(0).getBalanceSheetSaldoAwal());
        }

        thisQuery += " order by ledger_tgl, ledger_no_ref, ledger_seq asc";
        listBukuKas = crudService.findByNativeQuery(thisQuery, BukuKas.class);
        if(!listBukuKas.isEmpty()){
            saldoAkhirKas = HospitalStringUtils.addCommaFromDouble(listBukuKas.get(listBukuKas.size() - 1).getSaldoAkhir());
        }else{
            saldoAkhirKas = saldoAwalKas;
        }

        thisQuery = "";
        if (checkBoxFilterTanggal) {
            tahunBulan = new SimpleDateFormat("yyyy-MM").format(selectedFilterTanggal);
            tahun = new SimpleDateFormat("yyyy").format(new Date());
            
            List<BalanceSheet> listSaldoAwal = crudService.findByNativeQuery("SELECT uuid_in(md5(random()::text || now()::text)::cstring) as balance_sheet_id, countingsaldoawal('" + listAccountBank.get(0).getAccountSpecialCoaId().getCoaId() + "', '" + tahun + "-01-01', '" + tahunBulan + "-01') as balance_sheet_saldo_awal", BalanceSheet.class);
            thisQuery = "select tl.*, countingsaldoakhir(" + listSaldoAwal.get(0).getBalanceSheetSaldoAwal() + ", '" + listAccountBank.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "', '" + tahunBulan + "-01', tl.ledger_tgl) as saldo_akhir from trx_ledger tl inner join tbl_coa tc on (tl.ledger_coa_id = tc.coa_id) where ledger_no_ref in (select tl.ledger_no_ref from trx_ledger tl inner join tbl_coa tc on (tl.ledger_coa_id = tc.coa_id) where tc.coa_account_no LIKE '" + listAccountBank.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "%') and tc.coa_account_no NOT LIKE '" + listAccountBank.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "%' ";
            thisQuery += " and ledger_tgl::text LIKE '" + tahunBulan + "%'";
            thisQueryCount += " and ledger_tgl::text LIKE '" + tahunBulan + "%'";
            saldoAwalBank = HospitalStringUtils.addCommaFromDouble(listSaldoAwal.get(0).getBalanceSheetSaldoAwal());
        } else {
            List<BalanceSheet> listSaldoAwal = crudService.findByNativeQuery("SELECT uuid_in(md5(random()::text || now()::text)::cstring) as balance_sheet_id, countingsaldoawal('" + listAccountBank.get(0).getAccountSpecialCoaId().getCoaId() + "', '" + tahun + "-01-01', '" + tahunBulan + "-01') as balance_sheet_saldo_awal", BalanceSheet.class);
            thisQuery = "select tl.*, countingsaldoakhir(" + listSaldoAwal.get(0).getBalanceSheetSaldoAwal() + ", '" + listAccountBank.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "', '" + tahunBulan + "-01', tl.ledger_tgl) as saldo_akhir from trx_ledger tl inner join tbl_coa tc on (tl.ledger_coa_id = tc.coa_id) where ledger_no_ref in (select tl.ledger_no_ref from trx_ledger tl inner join tbl_coa tc on (tl.ledger_coa_id = tc.coa_id) where tc.coa_account_no LIKE '" + listAccountBank.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "%') and tc.coa_account_no NOT LIKE '" + listAccountBank.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "%' ";
            thisQuery += " and ledger_tgl::text LIKE '" + tahunBulan + "%'";
            thisQueryCount += " and ledger_tgl::text LIKE '" + tahunBulan + "%'";
            saldoAwalBank = HospitalStringUtils.addCommaFromDouble(listSaldoAwal.get(0).getBalanceSheetSaldoAwal());
        }
        thisQuery += " order by ledger_tgl, ledger_no_ref, ledger_seq asc";
        listBukuKasBank = crudService.findByNativeQuery(thisQuery, BukuKas.class);
        if(!listBukuKasBank.isEmpty()){
            saldoAkhirBank = HospitalStringUtils.addCommaFromDouble(listBukuKasBank.get(listBukuKasBank.size() - 1).getSaldoAkhir());
        }else{
            saldoAkhirBank = saldoAwalKas;
        }
//        listBukuKasBank = crudService.findByNativeQuery("select * from trx_ledger tl inner join tbl_coa tc on (tl.ledger_coa_id = tc.coa_id) where ledger_no_ref in (select tl.ledger_no_ref from trx_ledger tl inner join tbl_coa tc on (tl.ledger_coa_id = tc.coa_id) where tc.coa_account_no LIKE '" + listAccountBank.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "%') and tc.coa_account_no NOT LIKE '" + listAccountBank.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "%' order by TO_CHAR(ledger_tgl, 'YYYYMMDD') DESC, ledger_no_ref, ledger_seq asc", BukuKas.class);

    }
    
    public void doPrintAccountKas(){
        SimpleDateFormat tahunBulan = new SimpleDateFormat("yyyy-MM");
        HashMap<String, Object> paramSearch = new HashMap<>();
        HashMap reportParams = new HashMap();
        reportParams.put("paramDateNow", generateParameterTanggalReport(new Date()));
        reportParams.put("paramRSNama", parameter.getString("rsNama"));
        reportParams.put("paramRSAlamat", parameter.getString("rsAlamat"));
        if (checkBoxFilterTanggal) {
            reportParams.put("paramTanggal", format10.format(selectedFilterTanggal));
        }else{
            reportParams.put("paramTanggal", "-");
        }
        
        try {
            ReportGenerator reportGenerator = new ReportGenerator();
            reportGenerator.showReportPDF(reportParams, parameter.getString("pathLocationReport") + "Kas/BukuKas/LaporanBukuKas.jasper", listBukuKas);
        } catch (Exception ex) {
            createMessage("FATAL", "Gagal Mencetak Laporan !");
        }
    }

    public void doPrintAccountBank(){
        HashMap<String, Object> paramSearch = new HashMap<>();
        HashMap reportParams = new HashMap();
        reportParams.put("paramDateNow", generateParameterTanggalReport(new Date()));
        reportParams.put("paramRSNama", parameter.getString("rsNama"));
        reportParams.put("paramRSAlamat", parameter.getString("rsAlamat"));
        if (checkBoxFilterTanggal) {
            reportParams.put("paramTanggal", format10.format(selectedFilterTanggal));
        }else{
            reportParams.put("paramTanggal", "-");
        }
        
        try {
            ReportGenerator reportGenerator = new ReportGenerator();
            reportGenerator.showReportPDF(reportParams, parameter.getString("pathLocationReport") + "Kas/BukuKas/LaporanBukuKas.jasper", listBukuKasBank);
        } catch (Exception ex) {
            createMessage("FATAL", "Gagal Mencetak Laporan !");
        }
    }

    public List<BukuKas> getListBukuKas() {
        return listBukuKas;
    }

    public void setListBukuKas(List<BukuKas> listBukuKas) {
        this.listBukuKas = listBukuKas;
    }

    public List<BukuKas> getListBukuKasBank() {
        return listBukuKasBank;
    }

    public void setListBukuKasBank(List<BukuKas> listBukuKasBank) {
        this.listBukuKasBank = listBukuKasBank;
    }

    public List<TblAccountSpecial> getListAccountBank() {
        return listAccountBank;
    }

    public void setListAccountBank(List<TblAccountSpecial> listAccountBank) {
        this.listAccountBank = listAccountBank;
    }

    public Boolean getToggleButtonFilter() {
        return toggleButtonFilter;
    }

    public void setToggleButtonFilter(Boolean toggleButtonFilter) {
        this.toggleButtonFilter = toggleButtonFilter;
    }

    public Boolean getCheckBoxFilterTanggal() {
        return checkBoxFilterTanggal;
    }

    public void setCheckBoxFilterTanggal(Boolean checkBoxFilterTanggal) {
        this.checkBoxFilterTanggal = checkBoxFilterTanggal;
    }

    public Boolean getCheckBoxFilterUnit() {
        return checkBoxFilterUnit;
    }

    public void setCheckBoxFilterUnit(Boolean checkBoxFilterUnit) {
        this.checkBoxFilterUnit = checkBoxFilterUnit;
    }

    public Boolean getToggleFilterTanggal() {
        return toggleFilterTanggal;
    }

    public void setToggleFilterTanggal(Boolean toggleFilterTanggal) {
        this.toggleFilterTanggal = toggleFilterTanggal;
    }

    public Boolean getToggleFilterUnit() {
        return toggleFilterUnit;
    }

    public void setToggleFilterUnit(Boolean toggleFilterUnit) {
        this.toggleFilterUnit = toggleFilterUnit;
    }

    public Date getSelectedFilterTanggal() {
        return selectedFilterTanggal;
    }

    public void setSelectedFilterTanggal(Date selectedFilterTanggal) {
        this.selectedFilterTanggal = selectedFilterTanggal;
    }

    public String getSelectedFilterUnit() {
        return selectedFilterUnit;
    }

    public void setSelectedFilterUnit(String selectedFilterUnit) {
        this.selectedFilterUnit = selectedFilterUnit;
    }

    public String getSearchNoRef() {
        return searchNoRef;
    }

    public void setSearchNoRef(String searchNoRef) {
        this.searchNoRef = searchNoRef;
    }

    public List<TblAccountSpecial> getListAccountKas() {
        return listAccountKas;
    }

    public void setListAccountKas(List<TblAccountSpecial> listAccountKas) {
        this.listAccountKas = listAccountKas;
    }

    public String getSaldoAwalKas() {
        return saldoAwalKas;
    }

    public void setSaldoAwalKas(String saldoAwalKas) {
        this.saldoAwalKas = saldoAwalKas;
    }

    public String getSaldoAwalBank() {
        return saldoAwalBank;
    }

    public void setSaldoAwalBank(String saldoAwalBank) {
        this.saldoAwalBank = saldoAwalBank;
    }

    public String getSaldoAkhirKas() {
        return saldoAkhirKas;
    }

    public void setSaldoAkhirKas(String saldoAkhirKas) {
        this.saldoAkhirKas = saldoAkhirKas;
    }

    public String getSaldoAkhirBank() {
        return saldoAkhirBank;
    }

    public void setSaldoAkhirBank(String saldoAkhirBank) {
        this.saldoAkhirBank = saldoAkhirBank;
    }

}
