/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital.controllers.reports;

import com.hospital.controllers.BaseController;
import com.hospital.crud.CrudService;
import com.hospital.models.TblCoa;
import com.hospital.models.TblUnit;
import static com.hospital.utils.HospitalStringUtils.generateParameterTanggalReport;
import com.hospital.utils.ReportGenerator;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author regijg
 */
@ManagedBean(name = "reportHarianKasController")
@ViewScoped
public class ReportHarianKasController extends BaseController implements Serializable{

    @EJB
    private CrudService crudService;
    //private TblCoa tblCoa = new TblCoa();
    private String selectAccountKasOrBank = "";
    private List<TblCoa> listAccount = new ArrayList<>();
    private String selectAccount;
    private Date selectedTanggalAwal;
    private Date selectedTanggalAkhir;
    
    /**
     * Creates a new instance of ReportHarianKasController
     */
    public ReportHarianKasController() {
    }
    
    @PostConstruct
    public void init() {
        // Set tanggal awal 1 minggu sebelumnya
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_MONTH, -7);
        selectedTanggalAwal = cal.getTime();
        selectedTanggalAkhir = new Date();
        
        if (listAccount.size() > 0) {
            selectAccount = listAccount.get(0).getCoaAccountNo();
        }
    }
    
    public void thisOnChange() {
    }
    
    public void loadAccountKasOrBank() {
        listAccount = crudService.findByNativeQuery("SELECT * FROM tbl_coa WHERE coa_account_no LIKE (SELECT coa_account_no FROM tbl_coa WHERE coa_account_no = '" + selectAccountKasOrBank + "') || '%' ORDER BY 1", TblCoa.class);
    }
    
    public void doPrintKasMasuk() throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, Exception {
        HashMap<String, Object> paramSearch = new HashMap<>();
        HashMap reportParams = new HashMap();
        reportParams.put("paramDateNow", generateParameterTanggalReport(new Date()));
        reportParams.put("paramRSNama", parameter.getString("rsNama"));
        reportParams.put("paramRSAlamat", parameter.getString("rsAlamat"));
        reportParams.put("paramTanggal", format3.format(selectedTanggalAwal) + " - " + format3.format(selectedTanggalAkhir));

        String query = "SELECT tcih.cash_in_head_no_ref as noBukti,"
                + " to_char(tcih.cash_in_head_tgl, 'dd-MM-yyyy') as tanggal,"
                + " tcih.cash_in_head_keterangan as keterangan,"
                + " tc.coa_account_no as accNo,"
                + " tcih.cash_in_head_jumlah as jumlah"
                + " FROM trx_cash_in_head tcih"
                + " LEFT OUTER JOIN tbl_coa tc on tcih.cash_in_head_coa_id = tc.coa_id"
                + " WHERE tcih.cash_in_head_ledger_group_id != '' and tcih.cash_in_head_status = 0"
                + " AND tcih.cash_in_head_is_out = 'FALSE'"
                + " AND (tcih.cash_in_head_tgl BETWEEN '" + dateQueryFormat("from", selectedTanggalAwal) + "' AND '" + dateQueryFormat("to", selectedTanggalAkhir) + "') "
                + " AND tc.coa_account_no = '" + selectAccount + "'"
                + " ORDER BY tcih.cash_in_head_no_ref";
        
        List<Object> resultsObject = crudService.findByNativeQuery(query);
        List<GenerateReport> resultsToPrint = new ArrayList<>();

        for (Object eachObject : resultsObject) {
            Object[] o = (Object[]) eachObject; //types below are just guessed:

            String noBukti = (String) o[0];
            String tanggal = (String) o[1];
            String keterangan = (String) o[2];
            String accNo = (String) o[3];
            BigDecimal jumlah = (BigDecimal) o[4];
            
            GenerateReport eachGenerateReport = new GenerateReport();
            eachGenerateReport.setNoBukti(noBukti);
            eachGenerateReport.setTanggal(tanggal);
            eachGenerateReport.setKeterangan(keterangan);
            eachGenerateReport.setAccNo(accNo);
            eachGenerateReport.setJumlah(jumlah);
            resultsToPrint.add(eachGenerateReport);
        }
        try {
            ReportGenerator reportGenerator = new ReportGenerator();
            reportGenerator.showReportPDF(reportParams, parameter.getString("pathLocationReport") + "Kas/harian/LaporanHarianBukuKasMasuk.jasper", resultsToPrint);
        } catch (Exception ex) {
            createMessage("FATAL", "Gagal Mencetak !");
        }
    }
    
    public void doPrintKasKeluar() throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, Exception {
        HashMap<String, Object> paramSearch = new HashMap<>();
        HashMap reportParams = new HashMap();
        reportParams.put("paramDateNow", generateParameterTanggalReport(new Date()));
        reportParams.put("paramRSNama", parameter.getString("rsNama"));
        reportParams.put("paramRSAlamat", parameter.getString("rsAlamat"));
        reportParams.put("paramTanggal", format3.format(selectedTanggalAwal) + " - " + format3.format(selectedTanggalAkhir));

        String query = "SELECT tcih.cash_in_head_no_ref as noBukti,"
                + " to_char(tcih.cash_in_head_tgl, 'dd-MM-yyyy') as tanggal,"
                + " tcih.cash_in_head_keterangan as keterangan,"
                + " tc.coa_account_no as accNo,"
                + " tcih.cash_in_head_jumlah as jumlah"
                + " FROM trx_cash_in_head tcih"
                + " LEFT OUTER JOIN tbl_coa tc on tcih.cash_in_head_coa_id = tc.coa_id"
                + " WHERE tcih.cash_in_head_ledger_group_id != '' and tcih.cash_in_head_status = 0"
                + " AND tcih.cash_in_head_is_out != 'FALSE'"
                + " AND (tcih.cash_in_head_tgl BETWEEN '" + dateQueryFormat("from", selectedTanggalAwal) + "' AND '" + dateQueryFormat("to", selectedTanggalAkhir) + "') "
                + " AND tc.coa_account_no = '" + selectAccount + "'"
                + " ORDER BY tcih.cash_in_head_no_ref";
        
        List<Object> resultsObject = crudService.findByNativeQuery(query);
        List<GenerateReport> resultsToPrint = new ArrayList<>();

        for (Object eachObject : resultsObject) {
            Object[] o = (Object[]) eachObject; //types below are just guessed:

            String noBukti = (String) o[0];
            String tanggal = (String) o[1];
            String keterangan = (String) o[2];
            String accNo = (String) o[3];
            BigDecimal jumlah = (BigDecimal) o[4];
            
            GenerateReport eachGenerateReport = new GenerateReport();
            eachGenerateReport.setNoBukti(noBukti);
            eachGenerateReport.setTanggal(tanggal);
            eachGenerateReport.setKeterangan(keterangan);
            eachGenerateReport.setAccNo(accNo);
            eachGenerateReport.setJumlah(jumlah);
            resultsToPrint.add(eachGenerateReport);
        }
        try {
            ReportGenerator reportGenerator = new ReportGenerator();
            reportGenerator.showReportPDF(reportParams, parameter.getString("pathLocationReport") + "Kas/harian/LaporanHarianBukuKasKeluar.jasper", resultsToPrint);
        } catch (Exception ex) {
            createMessage("FATAL", "Gagal Mencetak !");
        }
    }

    public String getSelectAccountKasOrBank() {
        return selectAccountKasOrBank;
    }

    public void setSelectAccountKasOrBank(String selectAccountKasOrBank) {
        this.selectAccountKasOrBank = selectAccountKasOrBank;
    }

    public List<TblCoa> getListAccount() {
        return listAccount;
    }

    public void setListAccount(List<TblCoa> listAccount) {
        this.listAccount = listAccount;
    }

    public String getSelectAccount() {
        return selectAccount;
    }

    public void setSelectAccount(String selectAccount) {
        this.selectAccount = selectAccount;
    }

    public Date getSelectedTanggalAwal() {
        return selectedTanggalAwal;
    }

    public void setSelectedTanggalAwal(Date selectedTanggalAwal) {
        this.selectedTanggalAwal = selectedTanggalAwal;
    }

    public Date getSelectedTanggalAkhir() {
        return selectedTanggalAkhir;
    }

    public void setSelectedTanggalAkhir(Date selectedTanggalAkhir) {
        this.selectedTanggalAkhir = selectedTanggalAkhir;
    }
    
    public class GenerateReport implements Serializable {
        
        public String noBukti = "";
        public String tanggal = "";
        public String keterangan = "";
        public String accNo = "";
        public BigDecimal jumlah = null;

        
        public GenerateReport() {
        }

        public String getNoBukti() {
            return noBukti;
        }

        public void setNoBukti(String noBukti) {
            this.noBukti = noBukti;
        }

        public String getTanggal() {
            return tanggal;
        }

        public void setTanggal(String tanggal) {
            this.tanggal = tanggal;
        }

        public String getKeterangan() {
            return keterangan;
        }

        public void setKeterangan(String keterangan) {
            this.keterangan = keterangan;
        }

        public String getAccNo() {
            return accNo;
        }

        public void setAccNo(String accNo) {
            this.accNo = accNo;
        }

        public BigDecimal getJumlah() {
            return jumlah;
        }

        public void setJumlah(BigDecimal jumlah) {
            this.jumlah = jumlah;
        }

        @Override
        public String toString() {
            return "GenerateReport{" + "noBukti=" + noBukti + ", tanggal=" + tanggal + ", keterangan=" + keterangan + ", accNo=" + accNo + ", jumlah=" + jumlah + '}';
        }
    }
    
}
