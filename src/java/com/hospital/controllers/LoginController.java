/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital.controllers;

import com.hospital.crud.CrudService;
import com.hospital.models.TblParameter;
import com.hospital.models.TblUser;
import com.hospital.utils.HospitalSecurityUtils;
import com.hospital.utils.HospitalStringUtils;
import java.io.Serializable;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.inject.Named;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import java.awt.print.PrinterJob;
import java.util.ArrayList;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.AttributeSet;
import javax.print.attribute.HashAttributeSet;
import javax.print.attribute.standard.PrinterName;

/**
 *
 * @author san
 */
@Named(value = "loginController")
@SessionScoped
public class LoginController extends BaseController implements Serializable {

    @EJB
    private CrudService crudService;
    private String passwd;
    private String uname;
    private String shift;
    public Connection conn;

    /**
     * Creates a new instance of LoginController
     */
    public LoginController() {

        super();
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        if (session != null) {
            session.invalidate();
        }
    }

    public String login() {
        for (int i = 0; i < 550; i++) {
            System.out.println("------------------------------- " + HospitalStringUtils.generateUUID());
        }
        HttpSession session = null;
        session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        if (session.getAttribute("usrName") != null) {
            session.removeAttribute("usrName");
//            session.invalidate();
        }
        HospitalSecurityUtils hospitalSecurityUtils = new HospitalSecurityUtils();
        System.out.println("PASS = " + hospitalSecurityUtils.md5(passwd));
        List<TblUser> checkUser = crudService.findByNativeQuery("SELECT * FROM tbl_user"
                + " where username = '" + uname + "' and password = '" + hospitalSecurityUtils.md5(passwd) + "'", TblUser.class);
        if (checkUser.isEmpty()) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Login Gagal", null));
            return "FAILLOGIN";
        } else {
//            System.out.println("ROLE ID = " + checkUser.get(0).getUserRoleId().getRoleId());
            session.setAttribute("usrName", uname);
            session.setAttribute("roleId", checkUser.get(0).getUserRoleId().getRoleId().toString());
            List<TblParameter> listStartShiftSiang = crudService.findByNativeQuery("SELECT * FROM tbl_parameter WHERE parameter = 'MULAI_SHIFT_SIANG'", TblParameter.class);
            List<TblParameter> listEndShiftSiang = crudService.findByNativeQuery("SELECT * FROM tbl_parameter WHERE parameter = 'AKHIR_SHIFT_SIANG'", TblParameter.class);
            List<TblParameter> listStartShiftMalam = crudService.findByNativeQuery("SELECT * FROM tbl_parameter WHERE parameter = 'MULAI_SHIFT_MALAM'", TblParameter.class);
            List<TblParameter> listEndShiftMalam = crudService.findByNativeQuery("SELECT * FROM tbl_parameter WHERE parameter = 'AKHIR_SHIFT_MALAM'", TblParameter.class);
            String jamString = new SimpleDateFormat("HH").format(new Date());
            Integer jam = Integer.parseInt(jamString);
            if (jam >= Integer.parseInt(listStartShiftSiang.get(0).getValue()) && jam <= Integer.parseInt(listEndShiftSiang.get(0).getValue())) {
                shift = "Siang";
            } else {
                shift = "Malam";
            }
            session.setAttribute("shift", shift);
            session.setAttribute("poliNama", checkUser.get(0).getUserUnitId().getUnitNama());
            session.setAttribute("poliId", checkUser.get(0).getUserUnitId().getUnitId());
            return "SUCCESSLOGIN";
        }
    }

    public static PrintService findPrintService(String printerName) {

        printerName = printerName.toLowerCase();

        PrintService service = null;

        // Get array of all print services
        PrintService[] services = PrinterJob.lookupPrintServices();

        // Retrieve a print service from the array
        for (int index = 0; service == null && index < services.length; index++) {

            if (services[index].getName().toLowerCase().indexOf(printerName) >= 0) {
                service = services[index];
            }
        }

        // Return the print service
        return service;
    }

    /**
     * Retrieves a List of Printer Service Names.
     *
     * @return List
     */
    public static List<String> getPrinterServiceNameList() {

        // get list of all print services
        PrintService[] services = PrinterJob.lookupPrintServices();
        List<String> list = new ArrayList<String>();

        for (int i = 0; i < services.length; i++) {
            list.add(services[i].getName());
        }

        return list;
    }

    public String logout() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        if (session != null) {
            session.invalidate();
        }
        return "SUCCESSLOGOUT";
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

}
