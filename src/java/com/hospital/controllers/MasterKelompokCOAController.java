/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital.controllers;

import com.hospital.crud.CrudService;
import com.hospital.models.TblKelompokCoa;
import com.hospital.utils.HospitalStringUtils;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author eh-regex
 */
@ManagedBean(name = "masterKelompokCOAController")
@ViewScoped
public class MasterKelompokCOAController extends BaseController implements Serializable {

    @EJB
    private CrudService crudService;
    private final HospitalStringUtils hospitalStringUtils = new HospitalStringUtils();
    private LazyDataModel<TblKelompokCoa> lazyTblKelompokCoa = null;
    private TblKelompokCoa tblKelompokCoa = new TblKelompokCoa();
    private String nameTemp = "";

    /**
     * Creates a new instance of MasterKelompokCOAController
     */
    public MasterKelompokCOAController() {
    }

    public void refreshDataTable() {
        loadTable();
    }

    @PostConstruct
    public void init() {
        loadTable();
    }

    private void loadTable() {
        lazyTblKelompokCoa = new LazyDataModel<TblKelompokCoa>() {
            @Override
            public List<TblKelompokCoa> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> map) {
                String thisQuery = "SELECT t FROM TblKelompokCoa t WHERE t.kelompokCoaKode != '0'";
                Set<Map.Entry<String, Object>> rawParameters = map.entrySet();

                Boolean isSearch = false;
                for (Map.Entry<String, Object> entry : rawParameters) {
                    isSearch = true;
                    thisQuery = thisQuery + " AND LOWER(t." + entry.getKey() + ") LIKE '%" + entry.getValue().toString().toLowerCase() + "%'";
                }

                if (sortField != null) {
                    String sortMethod = "DESC";
                    if (sortOrder.toString().equalsIgnoreCase("ASCENDING")) {
                        sortMethod = "ASC";
                    }
                    thisQuery = thisQuery + " ORDER BY t." + sortField + " " + sortMethod;
                } else {
                    thisQuery = thisQuery + " ORDER BY t.kelompokCoaKode ASC";
                }

                List<TblKelompokCoa> list = crudService.findWithCreateQuery(thisQuery, first, pageSize);
                if (isSearch) {
                    setRowCount(list.size());
                } else {
                    int rowCount = crudService.countWithNamedQuery("TblKelompokCoa.countAll");
                    setRowCount(rowCount);
                }
                setPageSize(pageSize);
                return list;
            }

            @Override
            public TblKelompokCoa getRowData(String rowKey) {
                HashMap<String, Object> param = new HashMap<>();
                param.put("kelompokCoaId", rowKey);
                List<TblKelompokCoa> list = crudService.findWithNamedQuery("TblKelompokCoa.findByKelompokCoaId", param);
                if (list.isEmpty()) {
                    return null;
                } else {
                    return list.get(0);
                }
            }

            @Override
            public Object getRowKey(TblKelompokCoa obj) {
                return obj.getKelompokCoaId();
            }
        };
    }

    //    =INSERT===================================================================
    //<editor-fold defaultstate="collapsed" desc="INSERT">
    public void doInsert() {
        tblKelompokCoa = new TblKelompokCoa();
        executeInstance("PF('widDlgInsert').show()");
        updateInstance("formDlgInsert:pgridInsert");
    }

    public void insert() {
        try {
            tblKelompokCoa.setKelompokCoaId(hospitalStringUtils.generateUUID());
            tblKelompokCoa.setInsertDate(new Date());
            tblKelompokCoa.setInsertBy(getUsername());

            crudService.create(tblKelompokCoa);
            tblKelompokCoa = new TblKelompokCoa();
            createMessage("INFO", "Kelompok COA berhasil disimpan.");
            executeInstance("PF('widDlgInsert').hide()");
            updateInstance("thisForm:thisData");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //</editor-fold>

    //    =UPDATE===================================================================
    //<editor-fold defaultstate="collapsed" desc="UPDATE">
    public void doUpdate() {
        if (tblKelompokCoa == null) {
            createMessage("WARN", "Pilih data terlebih dahulu.");
        } else {
            nameTemp = tblKelompokCoa.getKelompokCoaKode();

            executeInstance("PF('widDlgUpdate').show()");
            updateInstance("formDlgUpdate:pgridUpdate");
        }
    }

    public void update() {
        HashMap<String, Object> param = new HashMap<>();
        param.put("kelompokCoaId", tblKelompokCoa.getKelompokCoaId());
        List<TblKelompokCoa> list = crudService.findWithNamedQuery("TblKelompokCoa.findByKelompokCoaId", param);
        TblKelompokCoa kelompokCOA = list.get(0);
        kelompokCOA.setKelompokCoaKode(tblKelompokCoa.getKelompokCoaKode());
        kelompokCOA.setKelompokCoaNama(tblKelompokCoa.getKelompokCoaNama());

        kelompokCOA.setUpdateDate(new Date());
        kelompokCOA.setUpdateBy(getUsername());

        crudService.update(kelompokCOA);
        tblKelompokCoa = new TblKelompokCoa();
        nameTemp = "";
        createMessage("INFO", "Kelompok COA berhasil diubah.");
        executeInstance("PF('widDlgUpdate').hide()");
        updateInstance("thisForm:thisData");
    }

    //</editor-fold>
    //    =DELETE===================================================================
//<editor-fold defaultstate="collapsed" desc="DELETE">
    public void doDelete() {
        if (tblKelompokCoa == null) {
            createMessage("WARN", "Pilih data terlebih dahulu.");
        } else {
            updateInstance(":formCfmDelete:cfmDelete");
            executeInstance("PF('widCfmDelete').show()");
        }
    }

    public void delete() {
        crudService.delete(tblKelompokCoa);
        createMessage("INFO", "Hapus data berhasil.");
        executeInstance("PF('widCfmDelete').hide()");
        updateInstance("thisForm:thisData");
    }
    //</editor-fold>

    public LazyDataModel<TblKelompokCoa> getLazyTblKelompokCoa() {
        return lazyTblKelompokCoa;
    }

    public void setLazyTblKelompokCoa(LazyDataModel<TblKelompokCoa> lazyTblKelompokCoa) {
        this.lazyTblKelompokCoa = lazyTblKelompokCoa;
    }

    public TblKelompokCoa getTblKelompokCoa() {
        return tblKelompokCoa;
    }

    public void setTblKelompokCoa(TblKelompokCoa tblKelompokCoa) {
        this.tblKelompokCoa = tblKelompokCoa;
    }

    public String getNameTemp() {
        return nameTemp;
    }

    public void setNameTemp(String nameTemp) {
        this.nameTemp = nameTemp;
    }

}
