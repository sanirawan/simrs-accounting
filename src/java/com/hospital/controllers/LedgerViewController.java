/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital.controllers;

import com.hospital.crud.CrudService;
import com.hospital.models.TrxLedger;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author arisoft
 */
@ManagedBean(name = "ledgerViewController")
@ViewScoped
public class LedgerViewController extends BaseController implements Serializable{
    @EJB
    private CrudService crudService;
    private LazyDataModel<TrxLedger> lazyTrxLedger = null;
    
    /**
     * =============== PENCARIAN DATA
     */
    private Boolean toggleButtonFilter = true;

    private Boolean checkBoxFilterTanggal = false;
    private Boolean checkBoxFilterUnit = false;

    private Boolean toggleFilterTanggal = true;
    private Boolean toggleFilterUnit = true;

    private Date selectedFilterTanggal;
    private String selectedFilterUnit = "";
    private String searchNoRef = "";
    /**
     * Creates a new instance of LedgerViewController
     */
    public LedgerViewController() {
    }
    
    @PostConstruct
    public void init() {
        loadTable();
    }
    
    private void loadTable() {
        lazyTrxLedger = new LazyDataModel<TrxLedger>() {
            @Override
            public List<TrxLedger> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> map) {
                List<TrxLedger> list = crudService.findWithNamedQuery("TrxLedger.findAllWithSortDesc", first, pageSize);
                int rowCount = crudService.countWithNamedQuery("TrxLedger.countAll");
                setRowCount(rowCount);
                setPageSize(pageSize);
                return list;
            }

            @Override
            public TrxLedger getRowData(String rowKey) {
                HashMap<String, Object> param = new HashMap<>();
                param.put("ledgerId", rowKey);
                List<TrxLedger> list = crudService.findWithNamedQuery("TrxLedger.findByLedgerId", param);
                if (list.isEmpty()) {
                    return null;
                } else {
                    return list.get(0);
                }
            }

            @Override
            public Object getRowKey(TrxLedger obj) {
                return obj.getLedgerId();
            }
        };
    }
    
    public List<String> findNoRef(String query) {
        return crudService.findByNativeQuery("SELECT ledger_no_ref FROM trx_ledger WHERE UPPER(ledger_no_ref) LIKE '%" + query.toUpperCase() + "%' GROUP BY ledger_no_ref");
    }

    public void searchByNoRef() {
        lazyTrxLedger = new LazyDataModel<TrxLedger>() {
            @Override
            public List<TrxLedger> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> map) {
                String thisQuery = "SELECT t FROM TrxLedger t WHERE t.ledgerNoRef = '" + searchNoRef + "' ORDER BY t.ledgerTgl DESC, t.ledgerSeq ASC";
                String countQuery = "SELECT COUNT(t) FROM TrxLedger t WHERE t.ledgerNoRef = '" + searchNoRef + "'";
                Set<Map.Entry<String, Object>> rawParameters = map.entrySet();
                List<TrxLedger> list = crudService.findWithCreateQuery(thisQuery, first, pageSize);
                setRowCount(crudService.countWithCreateQuery(countQuery));
                setPageSize(pageSize);
                return list;
            }
        };
    }

    public LazyDataModel<TrxLedger> getLazyTrxLedger() {
        return lazyTrxLedger;
    }

    public void setLazyTrxLedger(LazyDataModel<TrxLedger> lazyTrxLedger) {
        this.lazyTrxLedger = lazyTrxLedger;
    }

    public Boolean getToggleButtonFilter() {
        return toggleButtonFilter;
    }

    public void setToggleButtonFilter(Boolean toggleButtonFilter) {
        this.toggleButtonFilter = toggleButtonFilter;
    }

    public Boolean getCheckBoxFilterTanggal() {
        return checkBoxFilterTanggal;
    }

    public void setCheckBoxFilterTanggal(Boolean checkBoxFilterTanggal) {
        this.checkBoxFilterTanggal = checkBoxFilterTanggal;
    }

    public Boolean getCheckBoxFilterUnit() {
        return checkBoxFilterUnit;
    }

    public void setCheckBoxFilterUnit(Boolean checkBoxFilterUnit) {
        this.checkBoxFilterUnit = checkBoxFilterUnit;
    }

    public Boolean getToggleFilterTanggal() {
        return toggleFilterTanggal;
    }

    public void setToggleFilterTanggal(Boolean toggleFilterTanggal) {
        this.toggleFilterTanggal = toggleFilterTanggal;
    }

    public Boolean getToggleFilterUnit() {
        return toggleFilterUnit;
    }

    public void setToggleFilterUnit(Boolean toggleFilterUnit) {
        this.toggleFilterUnit = toggleFilterUnit;
    }

    public Date getSelectedFilterTanggal() {
        return selectedFilterTanggal;
    }

    public void setSelectedFilterTanggal(Date selectedFilterTanggal) {
        this.selectedFilterTanggal = selectedFilterTanggal;
    }

    public String getSelectedFilterUnit() {
        return selectedFilterUnit;
    }

    public void setSelectedFilterUnit(String selectedFilterUnit) {
        this.selectedFilterUnit = selectedFilterUnit;
    }

    public String getSearchNoRef() {
        return searchNoRef;
    }

    public void setSearchNoRef(String searchNoRef) {
        this.searchNoRef = searchNoRef;
    }


    
    
}
