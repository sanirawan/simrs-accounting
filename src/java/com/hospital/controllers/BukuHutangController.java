/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital.controllers;

import com.hospital.crud.CrudService;
import com.hospital.models.BalanceSheet;
import com.hospital.models.TblAccountSpecial;
import com.hospital.models.BukuKas;
import com.hospital.utils.HospitalStringUtils;
import static com.hospital.utils.HospitalStringUtils.generateParameterTanggalReport;
import com.hospital.utils.ReportGenerator;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author arisoft
 */
@ManagedBean(name = "bukuHutangController")
@ViewScoped
public class BukuHutangController extends BaseController implements Serializable {

    @EJB
    private CrudService crudService;
    private List<BukuKas> listBukuHutang = null;
    private List<BukuKas> listBukuPiutang = null;
    private List<TblAccountSpecial> listAccountHutang = new ArrayList<>();
    private List<TblAccountSpecial> listAccountPiutang = new ArrayList<>();
    private String saldoAwalKas;
    private String saldoAwalBank;
    private String saldoAkhirKas;
    private String saldoAkhirBank;

    /**
     * =============== PENCARIAN DATA
     */
    private Boolean toggleButtonFilter = true;

    private Boolean checkBoxFilterTanggal = false;
    private Boolean checkBoxFilterUnit = false;

    private Boolean toggleFilterTanggal = true;
    private Boolean toggleFilterUnit = true;

    private Date selectedFilterTanggal;
    private String selectedFilterUnit = "";
    private String searchNoRef = "";

    /**
     * Creates a new instance of LedgerViewController
     */
    public BukuHutangController() {
    }

    @PostConstruct
    public void init() {
        loadAccountSpecial();
        loadTable();
    }

    private void loadAccountSpecial() {
        HashMap<String, Object> param = new HashMap<>();
        param.put("accountSpecialFunction", "Account Hutang");
        listAccountHutang = crudService.findWithNamedQuery("TblAccountSpecial.findByAccountSpecialFunction", param);
        param = new HashMap<>();
        param.put("accountSpecialFunction", "Account Piutang");
        listAccountPiutang = crudService.findWithNamedQuery("TblAccountSpecial.findByAccountSpecialFunction", param);
    }

    public List<String> findNoRef(String query) {
        return crudService.findByNativeQuery("SELECT ledger_no_ref FROM trx_ledger WHERE UPPER(ledger_no_ref) LIKE '%" + query.toUpperCase() + "%' GROUP BY ledger_no_ref");
    }

    public void listenerCheckboxFilter() {
        if (checkBoxFilterTanggal) {
            toggleFilterTanggal = false;
        } else {
            toggleFilterTanggal = true;
        }
        if (checkBoxFilterUnit) {
            toggleFilterUnit = false;
        } else {
            toggleFilterUnit = true;
        }

        if (checkBoxFilterTanggal || checkBoxFilterUnit) {
            toggleButtonFilter = false;
        } else {
            toggleButtonFilter = true;
        }
    }

    public void loadTable() {
        String tahunBulan = new SimpleDateFormat("yyyy-MM").format(new Date());
        String tahun = new SimpleDateFormat("yyyy").format(new Date());

        String thisQuery = "";
        String thisQueryCount = "select COUNT(*) from trx_ledger tl inner join tbl_coa tc on (tl.ledger_coa_id = tc.coa_id) where ledger_no_ref in (select tl.ledger_no_ref from trx_ledger tl inner join tbl_coa tc on (tl.ledger_coa_id = tc.coa_id) where tc.coa_account_no LIKE '" + listAccountHutang.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "%') and tc.coa_account_no NOT LIKE '" + listAccountHutang.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "%'";

        if (checkBoxFilterTanggal) {
            tahunBulan = new SimpleDateFormat("yyyy-MM").format(selectedFilterTanggal);
            tahun = new SimpleDateFormat("yyyy").format(new Date());

            List<BalanceSheet> listSaldoAwal = crudService.findByNativeQuery("SELECT uuid_in(md5(random()::text || now()::text)::cstring) as balance_sheet_id, countingsaldoawal('" + listAccountHutang.get(0).getAccountSpecialCoaId().getCoaId() + "', '" + tahun + "-01-01', '" + tahunBulan + "-01') as balance_sheet_saldo_awal", BalanceSheet.class);
            thisQuery = "select tl.*, countingsaldoakhirhutang(" + listSaldoAwal.get(0).getBalanceSheetSaldoAwal() + ", '" + listAccountHutang.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "', '" + tahunBulan + "-01', tl.ledger_tgl) as saldo_akhir from trx_ledger tl inner join tbl_coa tc on (tl.ledger_coa_id = tc.coa_id) inner join trx_ledger_group tlg on (tl.ledger_ledger_group_id = tlg.ledger_group_id) where tlg.ledger_group_status_jurnal_umum = 0 AND ledger_no_ref in (select tl.ledger_no_ref from trx_ledger tl inner join tbl_coa tc on (tl.ledger_coa_id = tc.coa_id) where tc.coa_account_no LIKE '" + listAccountHutang.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "%') and tc.coa_account_no NOT LIKE '" + listAccountHutang.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "%' ";
            thisQuery += " and ledger_tgl::text LIKE '" + tahunBulan + "%'";
            thisQueryCount += " and ledger_tgl::text LIKE '" + tahunBulan + "%'";
            saldoAwalKas = HospitalStringUtils.addCommaFromDouble(listSaldoAwal.get(0).getBalanceSheetSaldoAwal());
        } else {
            List<BalanceSheet> listSaldoAwal = crudService.findByNativeQuery("SELECT uuid_in(md5(random()::text || now()::text)::cstring) as balance_sheet_id, countingsaldoawal('" + listAccountHutang.get(0).getAccountSpecialCoaId().getCoaId() + "', '" + tahun + "-01-01', '" + tahunBulan + "-01') as balance_sheet_saldo_awal", BalanceSheet.class);
            thisQuery = "select tl.*, countingsaldoakhirhutang(" + listSaldoAwal.get(0).getBalanceSheetSaldoAwal() + ", '" + listAccountHutang.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "', '" + tahunBulan + "-01', tl.ledger_tgl) as saldo_akhir from trx_ledger tl inner join tbl_coa tc on (tl.ledger_coa_id = tc.coa_id) inner join trx_ledger_group tlg on (tl.ledger_ledger_group_id = tlg.ledger_group_id) where tlg.ledger_group_status_jurnal_umum = 0 AND ledger_no_ref in (select tl.ledger_no_ref from trx_ledger tl inner join tbl_coa tc on (tl.ledger_coa_id = tc.coa_id) where tc.coa_account_no LIKE '" + listAccountHutang.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "%') and tc.coa_account_no NOT LIKE '" + listAccountHutang.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "%' ";
            thisQuery += " and ledger_tgl::text LIKE '" + tahunBulan + "%'";
            thisQueryCount += " and ledger_tgl::text LIKE '" + tahunBulan + "%'";
            saldoAwalKas = HospitalStringUtils.addCommaFromDouble(listSaldoAwal.get(0).getBalanceSheetSaldoAwal());
        }

        thisQuery += " order by ledger_tgl, ledger_no_ref, ledger_seq asc";
        listBukuHutang = crudService.findByNativeQuery(thisQuery, BukuKas.class);
        if (!listBukuHutang.isEmpty()) {
            saldoAkhirKas = HospitalStringUtils.addCommaFromDouble(listBukuHutang.get(listBukuHutang.size() - 1).getSaldoAkhir());
        } else {
            saldoAkhirKas = saldoAwalKas;
        }

        thisQuery = "";
        if (checkBoxFilterTanggal) {
            tahunBulan = new SimpleDateFormat("yyyy-MM").format(selectedFilterTanggal);
            tahun = new SimpleDateFormat("yyyy").format(new Date());

            List<BalanceSheet> listSaldoAwal = crudService.findByNativeQuery("SELECT uuid_in(md5(random()::text || now()::text)::cstring) as balance_sheet_id, countingsaldoawal('" + listAccountPiutang.get(0).getAccountSpecialCoaId().getCoaId() + "', '" + tahun + "-01-01', '" + tahunBulan + "-01') as balance_sheet_saldo_awal", BalanceSheet.class);
            thisQuery = "select tl.*, countingsaldoakhir(" + listSaldoAwal.get(0).getBalanceSheetSaldoAwal() + ", '" + listAccountPiutang.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "', '" + tahunBulan + "-01', tl.ledger_tgl) as saldo_akhir from trx_ledger tl inner join tbl_coa tc on (tl.ledger_coa_id = tc.coa_id) where ledger_no_ref in (select tl.ledger_no_ref from trx_ledger tl inner join tbl_coa tc on (tl.ledger_coa_id = tc.coa_id) where tc.coa_account_no LIKE '" + listAccountPiutang.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "%') and tc.coa_account_no NOT LIKE '" + listAccountPiutang.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "%' ";
            thisQuery += " and ledger_tgl::text LIKE '" + tahunBulan + "%'";
            thisQueryCount += " and ledger_tgl::text LIKE '" + tahunBulan + "%'";
            saldoAwalBank = HospitalStringUtils.addCommaFromDouble(listSaldoAwal.get(0).getBalanceSheetSaldoAwal());
        } else {
            List<BalanceSheet> listSaldoAwal = crudService.findByNativeQuery("SELECT uuid_in(md5(random()::text || now()::text)::cstring) as balance_sheet_id, countingsaldoawal('" + listAccountPiutang.get(0).getAccountSpecialCoaId().getCoaId() + "', '" + tahun + "-01-01', '" + tahunBulan + "-01') as balance_sheet_saldo_awal", BalanceSheet.class);
            thisQuery = "select tl.*, countingsaldoakhir(" + listSaldoAwal.get(0).getBalanceSheetSaldoAwal() + ", '" + listAccountPiutang.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "', '" + tahunBulan + "-01', tl.ledger_tgl) as saldo_akhir from trx_ledger tl inner join tbl_coa tc on (tl.ledger_coa_id = tc.coa_id) where ledger_no_ref in (select tl.ledger_no_ref from trx_ledger tl inner join tbl_coa tc on (tl.ledger_coa_id = tc.coa_id) where tc.coa_account_no LIKE '" + listAccountPiutang.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "%') and tc.coa_account_no NOT LIKE '" + listAccountPiutang.get(0).getAccountSpecialCoaId().getCoaAccountNo() + "%' ";
            thisQuery += " and ledger_tgl::text LIKE '" + tahunBulan + "%'";
            thisQueryCount += " and ledger_tgl::text LIKE '" + tahunBulan + "%'";
            saldoAwalBank = HospitalStringUtils.addCommaFromDouble(listSaldoAwal.get(0).getBalanceSheetSaldoAwal());
        }
        thisQuery += " order by ledger_tgl, ledger_no_ref, ledger_seq asc";
        listBukuPiutang = crudService.findByNativeQuery(thisQuery, BukuKas.class);
        if (!listBukuPiutang.isEmpty()) {
            saldoAkhirBank = HospitalStringUtils.addCommaFromDouble(listBukuPiutang.get(listBukuPiutang.size() - 1).getSaldoAkhir());
        } else {
            saldoAkhirBank = saldoAwalKas;
        }
    }

    public void doPrintAccountHutang() {
        SimpleDateFormat tahunBulan = new SimpleDateFormat("yyyy-MM");
        HashMap<String, Object> paramSearch = new HashMap<>();
        HashMap reportParams = new HashMap();
        reportParams.put("paramDateNow", generateParameterTanggalReport(new Date()));
        reportParams.put("paramRSNama", parameter.getString("rsNama"));
        reportParams.put("paramRSAlamat", parameter.getString("rsAlamat"));
        reportParams.put("paramJudul", "Hutang");
        if (checkBoxFilterTanggal) {
            reportParams.put("paramTanggal", format10.format(selectedFilterTanggal));
        } else {
            reportParams.put("paramTanggal", "-");
        }

        try {
            ReportGenerator reportGenerator = new ReportGenerator();
            reportGenerator.showReportPDF(reportParams, parameter.getString("pathLocationReport") + "Hutang/BukuHutang/LaporanBukuHutang.jasper", listBukuHutang);
        } catch (Exception ex) {
            createMessage("FATAL", "Gagal Mencetak Laporan !");
        }
    }

    public void doPrintAccountPiutang() {
        SimpleDateFormat tahunBulan = new SimpleDateFormat("yyyy-MM");
        HashMap<String, Object> paramSearch = new HashMap<>();
        HashMap reportParams = new HashMap();
        reportParams.put("paramDateNow", generateParameterTanggalReport(new Date()));
        reportParams.put("paramRSNama", parameter.getString("rsNama"));
        reportParams.put("paramRSAlamat", parameter.getString("rsAlamat"));
        reportParams.put("paramJudul", "Piutang");
        if (checkBoxFilterTanggal) {
            reportParams.put("paramTanggal", format10.format(selectedFilterTanggal));
        } else {
            reportParams.put("paramTanggal", "-");
        }

        try {
            ReportGenerator reportGenerator = new ReportGenerator();
            reportGenerator.showReportPDF(reportParams, parameter.getString("pathLocationReport") + "Hutang/BukuHutang/LaporanBukuHutang.jasper", listBukuPiutang);
        } catch (Exception ex) {
            createMessage("FATAL", "Gagal Mencetak Laporan !");
        }
    }

    public List<BukuKas> getListBukuKas() {
        return listBukuHutang;
    }

    public void setListBukuKas(List<BukuKas> listBukuHutang) {
        this.listBukuHutang = listBukuHutang;
    }

    public List<BukuKas> getListBukuKasBank() {
        return listBukuPiutang;
    }

    public void setListBukuKasBank(List<BukuKas> listBukuPiutang) {
        this.listBukuPiutang = listBukuPiutang;
    }

    public List<TblAccountSpecial> getListAccountBank() {
        return listAccountPiutang;
    }

    public void setListAccountBank(List<TblAccountSpecial> listAccountPiutang) {
        this.listAccountPiutang = listAccountPiutang;
    }

    public Boolean getToggleButtonFilter() {
        return toggleButtonFilter;
    }

    public void setToggleButtonFilter(Boolean toggleButtonFilter) {
        this.toggleButtonFilter = toggleButtonFilter;
    }

    public Boolean getCheckBoxFilterTanggal() {
        return checkBoxFilterTanggal;
    }

    public void setCheckBoxFilterTanggal(Boolean checkBoxFilterTanggal) {
        this.checkBoxFilterTanggal = checkBoxFilterTanggal;
    }

    public Boolean getCheckBoxFilterUnit() {
        return checkBoxFilterUnit;
    }

    public void setCheckBoxFilterUnit(Boolean checkBoxFilterUnit) {
        this.checkBoxFilterUnit = checkBoxFilterUnit;
    }

    public Boolean getToggleFilterTanggal() {
        return toggleFilterTanggal;
    }

    public void setToggleFilterTanggal(Boolean toggleFilterTanggal) {
        this.toggleFilterTanggal = toggleFilterTanggal;
    }

    public Boolean getToggleFilterUnit() {
        return toggleFilterUnit;
    }

    public void setToggleFilterUnit(Boolean toggleFilterUnit) {
        this.toggleFilterUnit = toggleFilterUnit;
    }

    public Date getSelectedFilterTanggal() {
        return selectedFilterTanggal;
    }

    public void setSelectedFilterTanggal(Date selectedFilterTanggal) {
        this.selectedFilterTanggal = selectedFilterTanggal;
    }

    public String getSelectedFilterUnit() {
        return selectedFilterUnit;
    }

    public void setSelectedFilterUnit(String selectedFilterUnit) {
        this.selectedFilterUnit = selectedFilterUnit;
    }

    public String getSearchNoRef() {
        return searchNoRef;
    }

    public void setSearchNoRef(String searchNoRef) {
        this.searchNoRef = searchNoRef;
    }

    public List<TblAccountSpecial> getListAccountKas() {
        return listAccountHutang;
    }

    public void setListAccountKas(List<TblAccountSpecial> listAccountHutang) {
        this.listAccountHutang = listAccountHutang;
    }

    public String getSaldoAwalKas() {
        return saldoAwalKas;
    }

    public void setSaldoAwalKas(String saldoAwalKas) {
        this.saldoAwalKas = saldoAwalKas;
    }

    public String getSaldoAwalBank() {
        return saldoAwalBank;
    }

    public void setSaldoAwalBank(String saldoAwalBank) {
        this.saldoAwalBank = saldoAwalBank;
    }

    public String getSaldoAkhirKas() {
        return saldoAkhirKas;
    }

    public void setSaldoAkhirKas(String saldoAkhirKas) {
        this.saldoAkhirKas = saldoAkhirKas;
    }

    public String getSaldoAkhirBank() {
        return saldoAkhirBank;
    }

    public void setSaldoAkhirBank(String saldoAkhirBank) {
        this.saldoAkhirBank = saldoAkhirBank;
    }

}
