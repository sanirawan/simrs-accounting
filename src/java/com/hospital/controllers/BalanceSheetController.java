/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital.controllers;

import com.hospital.crud.CrudService;
import com.hospital.models.BalanceSheet;
import com.hospital.models.TblAccountSpecial;
import com.hospital.models.TblCoa;
import com.hospital.models.TrxCashInHead;
import com.hospital.models.TrxLedger;
import com.hospital.utils.HospitalStringUtils;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.AjaxBehaviorEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author arisoft
 */
@ManagedBean(name = "balanceSheetController")
@ViewScoped
public class BalanceSheetController extends BaseController implements Serializable {

    @EJB
    private CrudService crudService;
    private LazyDataModel<BalanceSheet> lazybalanceSheet = null;
    private List<BalanceSheet> listbalanceSheet = null;
    private LazyDataModel<TrxLedger> lazyTrxLedger = null;
    private String totalDebitFormatter;
    private String totalKreditFormatter;
    private String totalMutasiFormatter;
    private String totalNeracaFormatter;
    private String totalLabaRugiFormatter;
    private String totalSaldoAwalFormatter;
    private Double totalLabaRugiLocal;

    /**
     * =============== PENCARIAN DATA
     */
    private Boolean toggleButtonFilter = true;

    private Boolean checkBoxFilterLevel = false;
    private Boolean checkBoxFilterTanggal = false;
    private Boolean checkBoxFilterUnit = false;

    private Boolean toggleFilterLevel = true;
    private Boolean toggleFilterTanggal = true;
    private Boolean toggleFilterUnit = true;

    private Date selectedFilterTanggal;
    private String selectedFilterAcc = "";
    private String selectedLevel = "";
    private String selectedLaporan = "";

    /**
     * Creates a new instance of BalanceSheetController
     */
    public BalanceSheetController() {
    }

    @PostConstruct
    public void init() {
        loadTable();
    }

//    private void loadTable() {
//        lazybalanceSheet = new LazyDataModel<BalanceSheet>() {
//            @Override
//            public List<BalanceSheet> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> map) {
//                String tahunBulan = new SimpleDateFormat("yyyy-MM").format(new Date());
//                String tahun = new SimpleDateFormat("yyyy").format(new Date());
//                List<BalanceSheet> list = crudService.findByNativeQuery("select uuid_in(md5(random()::text || now()::text)::cstring) as balance_sheet_id, coa_id as balance_sheet_coa_id, countingdebit(coa_id, '" + tahunBulan + "') as balance_sheet_debit, countingkredit(coa_id, '" + tahunBulan + "') as balance_sheet_kredit, countingsaldoawal(coa_id, '" + tahun + "-01-01', '" + tahunBulan + "-01') as balance_sheet_saldo_awal, countingmutasi(coa_id, '" + tahunBulan + "') as balance_sheet_mutasi from tbl_coa tc WHERE tc.coa_account_no != '0' order by coa_account_no ASC", BalanceSheet.class, first, pageSize);
//                int rowCount = crudService.nativeQueryReturnInteger("select COUNT(coa_id) from tbl_coa tc");
//                totalDebit = 0.0;
//                totalKredit = 0.0;
//                totalMutasi = 0.0;
//                totalNeraca = 0.0;
//                totalLabaRugi = 0.0;
//                for (int i = 0; i < list.size(); i++) {
//                    if (list.get(i).getBalanceSheetCoaId().getCoaLevel() == 1) {
//                        System.out.println("COA NO ------------------------- " + list.get(i).getBalanceSheetCoaId().getCoaAccountNo());
//                        System.out.println("LABA RUGI ------------------------- " + list.get(i).getBalanceSheetLabaRugi());
//                        totalDebit = totalDebit + nullToZero(list.get(i).getBalanceSheetDebit());
//                        totalKredit = totalKredit + nullToZero(list.get(i).getBalanceSheetKredit());
//                        totalMutasi = totalMutasi + nullToZero(list.get(i).getBalanceSheetMutasi());
//                        totalNeraca = totalNeraca + nullToZero(list.get(i).getBalanceSheetNeraca());
//                        totalLabaRugi = totalLabaRugi + nullToZero(list.get(i).getBalanceSheetLabaRugi());
//                    }
//                }
//                System.out.println("totalLabaRugi = " + totalLabaRugi);
//                setRowCount(rowCount);
//                setPageSize(pageSize);
//                return list;
//            }
//
//            @Override
//            public BalanceSheet getRowData(String rowKey) {
//                HashMap<String, Object> param = new HashMap<>();
//                param.put("cashInHeadId", rowKey);
//                List<BalanceSheet> list = crudService.findWithNamedQuery("TrxCashInHead.findByCashInHeadId", param);
//                if (list.isEmpty()) {
//                    return null;
//                } else {
//                    return list.get(0);
//                }
//            }
//
//            @Override
//            public Object getRowKey(BalanceSheet obj) {
//                return obj.getBalanceSheetId();
//            }
//        };
//    }
//    private void loadTable() {
//        String tahunBulan = new SimpleDateFormat("yyyy-MM").format(new Date());
//        String tahun = new SimpleDateFormat("yyyy").format(new Date());
//        listbalanceSheet = crudService.findByNativeQuery("select uuid_in(md5(random()::text || now()::text)::cstring) as balance_sheet_id, coa_id as balance_sheet_coa_id, countingdebit(coa_id, '" + tahunBulan + "') as balance_sheet_debit, countingkredit(coa_id, '" + tahunBulan + "') as balance_sheet_kredit, countingsaldoawal(coa_id, '" + tahun + "-01-01', '" + tahunBulan + "-01') as balance_sheet_saldo_awal, countingmutasi(coa_id, '" + tahunBulan + "') as balance_sheet_mutasi from tbl_coa tc WHERE tc.coa_account_no != '0' order by coa_account_no ASC", BalanceSheet.class);
//
//        HashMap<String, Object> param = new HashMap<>();
//        param.put("accountSpecialFunction", "Laba / Rugi Tahun Berjalan");
//        List<TblAccountSpecial> listAccountSpecial = crudService.findWithNamedQuery("TblAccountSpecial.findByAccountSpecialFunction", param);
//        List listTotalLabaRugi = crudService.findByNativeQuery("select countingtotallabarugi('" + tahun + "-01-01', '" + tahunBulan + "-01')");
//        BigDecimal obj = (BigDecimal) listTotalLabaRugi.get(0);
//        Double jumlahSaldoAwalTahunBerjalan = obj.doubleValue();
//        for (int i = 0; i < listbalanceSheet.size(); i++) {
//            if (listbalanceSheet.get(i).getBalanceSheetCoaId().getCoaId().equals(listAccountSpecial.get(0).getAccountSpecialCoaId().getCoaId())) {
////                listbalanceSheet.get(i).setBalanceSheetDebit(totalLabaRugi);
////                totalDebit  = totalDebit + totalLabaRugi;
//                listbalanceSheet.get(i).setBalanceSheetSaldoAwal(listbalanceSheet.get(i).getBalanceSheetSaldoAwal() + jumlahSaldoAwalTahunBerjalan);
//            }
//        }
//
//        Double totalDebit = 0.0;
//        Double totalKredit = 0.0;
//        Double totalMutasi = 0.0;
//        Double totalNeraca = 0.0;
//        Double totalLabaRugi = 0.0;
//        totalLabaRugiLocal = 0.0;
//        for (int i = 0; i < listbalanceSheet.size(); i++) {
//            if (listbalanceSheet.get(i).getBalanceSheetCoaId().getCoaLevel() == 1) {
//                totalDebit = totalDebit + nullToZero(listbalanceSheet.get(i).getBalanceSheetDebit());
//                totalKredit = totalKredit + nullToZero(listbalanceSheet.get(i).getBalanceSheetKredit());
//                totalMutasi = totalMutasi + nullToZero(listbalanceSheet.get(i).getBalanceSheetMutasiSwitch());
//                totalNeraca = totalNeraca + nullToZero(listbalanceSheet.get(i).getBalanceSheetNeraca());
//                totalLabaRugi = totalLabaRugi + nullToZero(listbalanceSheet.get(i).getBalanceSheetLabaRugi());
//                if (listbalanceSheet.get(i).getBalanceSheetCoaId().getCoaReport().equals("RUGI LABA")) {
//                    totalLabaRugiLocal = totalLabaRugiLocal + nullToZero(listbalanceSheet.get(i).getBalanceSheetMutasiSwitch());
//                }
//            }
//        }
//
//        totalDebitFormatter = HospitalStringUtils.addCommaFromDouble(totalDebit);
//        totalKreditFormatter = HospitalStringUtils.addCommaFromDouble(totalKredit);
//        totalMutasiFormatter = HospitalStringUtils.addCommaFromDouble(totalMutasi);
//        totalNeracaFormatter = HospitalStringUtils.addCommaFromDouble(totalNeraca);
//        totalLabaRugiFormatter = HospitalStringUtils.addCommaFromDouble(totalLabaRugi);
//    }
    private void loadTable() {
        jenisLaporanOnChange(null);
    }

    public void jenisLaporanOnChange(final AjaxBehaviorEvent event) {
        String tahunBulan = new SimpleDateFormat("yyyy-MM").format(new Date());
        String tahun = new SimpleDateFormat("yyyy").format(new Date());
        String query = "select uuid_in(md5(random()::text || now()::text)::cstring) as balance_sheet_id, coa_id as balance_sheet_coa_id, countingdebit(coa_id, '" + tahunBulan + "') as balance_sheet_debit, countingkredit(coa_id, '" + tahunBulan + "') as balance_sheet_kredit, countingsaldoawal(coa_id, '" + tahun + "-01-01', '" + tahunBulan + "-01') as balance_sheet_saldo_awal, countingmutasi(coa_id, '" + tahunBulan + "') as balance_sheet_mutasi from tbl_coa tc WHERE tc.coa_account_no != '0' ";

        if (HospitalStringUtils.isNotEmpty(selectedLaporan)) {
            if (selectedLaporan.equals("Neraca")) {
                query += " AND tc.coa_report = 'NERACA'";
            } else if (selectedLaporan.equals("Laba/Rugi")) {
                query += " AND tc.coa_report = 'RUGI LABA'";
            }
        }

        query += " order by coa_account_no ASC";

        listbalanceSheet = crudService.findByNativeQuery(query, BalanceSheet.class);

        HashMap<String, Object> param = new HashMap<>();
        param.put("accountSpecialFunction", "Laba / Rugi Tahun Berjalan");
        List<TblAccountSpecial> listAccountSpecial = crudService.findWithNamedQuery("TblAccountSpecial.findByAccountSpecialFunction", param);
        List listTotalLabaRugi = crudService.findByNativeQuery("select countingtotallabarugi('" + tahun + "-01-01', '" + tahunBulan + "-01')");
        BigDecimal obj = (BigDecimal) listTotalLabaRugi.get(0);
        Double jumlahSaldoAwalTahunBerjalan = obj.doubleValue();
        for (int i = 0; i < listbalanceSheet.size(); i++) {
            if (listbalanceSheet.get(i).getBalanceSheetCoaId().getCoaId().equals(listAccountSpecial.get(0).getAccountSpecialCoaId().getCoaId())) {
                if (HospitalStringUtils.isNotEmpty(selectedLaporan)) {
                    listbalanceSheet.get(i).setBalanceSheetKredit(totalLabaRugiLocal);
                }
                listbalanceSheet.get(i).setBalanceSheetSaldoAwal(listbalanceSheet.get(i).getBalanceSheetSaldoAwal() + jumlahSaldoAwalTahunBerjalan);
            }
        }

        Double totalDebit = 0.0;
        Double totalKredit = 0.0;
        Double totalMutasi = 0.0;
        Double totalNeraca = 0.0;
        Double totalLabaRugi = 0.0;
        Double totalSaldoAwal = 0.0;
        totalLabaRugiLocal = 0.0;
        for (int i = 0; i < listbalanceSheet.size(); i++) {
            if (listbalanceSheet.get(i).getBalanceSheetCoaId().getCoaLevel() == 1) {
                totalDebit = totalDebit + nullToZero(listbalanceSheet.get(i).getBalanceSheetDebit());
                totalKredit = totalKredit + nullToZero(listbalanceSheet.get(i).getBalanceSheetKredit());
                totalMutasi = totalMutasi + nullToZero(listbalanceSheet.get(i).getBalanceSheetMutasiSwitch());
                totalNeraca = totalNeraca + nullToZero(listbalanceSheet.get(i).getBalanceSheetNeraca());
                totalLabaRugi = totalLabaRugi + nullToZero(listbalanceSheet.get(i).getBalanceSheetLabaRugi());
                totalSaldoAwal = totalSaldoAwal + nullToZero(listbalanceSheet.get(i).getBalanceSheetSaldoAwal());
                if (!HospitalStringUtils.isNotEmpty(selectedLaporan)) {
                    if (listbalanceSheet.get(i).getBalanceSheetCoaId().getCoaReport().equals("RUGI LABA")) {
                        totalLabaRugiLocal = totalLabaRugiLocal + nullToZero(listbalanceSheet.get(i).getBalanceSheetMutasiSwitch());
                    }
                }
            }
        }

        totalDebitFormatter = HospitalStringUtils.addCommaFromDouble(totalDebit);
        totalKreditFormatter = HospitalStringUtils.addCommaFromDouble(totalKredit);
        totalMutasiFormatter = HospitalStringUtils.addCommaFromDouble(totalMutasi);
        totalNeracaFormatter = HospitalStringUtils.addCommaFromDouble(totalNeraca);
        totalLabaRugiFormatter = HospitalStringUtils.addCommaFromDouble(totalLabaRugi);
        totalSaldoAwalFormatter = HospitalStringUtils.addCommaFromDouble(totalSaldoAwal);
    }

    private Double nullToZero(Double param) {
        if (param == null) {
            return 0.0;
        } else {
            return param;
        }
    }

    public void prepareDialogLedger(String accNo) {
        loadLedgerDebit(accNo);
        executeInstance("PF('widDlgPosting').show()");
        updateInstance("formPosting:dlgPosting");
    }

    public void prepareDialogLedgerKredit(String accNo) {
        loadLedgerKredit(accNo);
        executeInstance("PF('widDlgPosting').show()");
        updateInstance("formPosting:dlgPosting");
    }

    private void loadLedgerDebit(String accNo) {
        lazyTrxLedger = new LazyDataModel<TrxLedger>() {
            @Override
            public List<TrxLedger> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> map) {
                String tahunBulan = new SimpleDateFormat("yyyyMM").format(new Date());
                HashMap<String, Object> param = new HashMap<>();
                param.put("coaAccountNo", accNo + "%");
                param.put("ledgerTgl", tahunBulan + "%");
                List<TrxLedger> list = crudService.findWithNamedQuery("TrxLedger.findAllFromBalanceDebit", param, first, pageSize);
                param.put("coaAccountNo", accNo + "%");
                param.put("ledgerTgl", tahunBulan + "%");
                int rowCount = crudService.countWithNamedQuery("TrxLedger.countFromBalanceDebit", param);
                setRowCount(rowCount);
                setPageSize(pageSize);
                return list;
            }

            @Override
            public TrxLedger getRowData(String rowKey) {
                HashMap<String, Object> param = new HashMap<>();
                param.put("ledgerId", rowKey);
                List<TrxLedger> list = crudService.findWithNamedQuery("TrxLedger.findByLedgerId", param);
                if (list.isEmpty()) {
                    return null;
                } else {
                    return list.get(0);
                }
            }

            @Override
            public Object getRowKey(TrxLedger obj) {
                return obj.getLedgerId();
            }
        };
    }

    private void loadLedgerKredit(String accNo) {
        lazyTrxLedger = new LazyDataModel<TrxLedger>() {
            @Override
            public List<TrxLedger> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> map) {
                String tahunBulan = new SimpleDateFormat("yyyyMM").format(new Date());
                HashMap<String, Object> param = new HashMap<>();
                param.put("coaAccountNo", accNo + "%");
                param.put("ledgerTgl", tahunBulan + "%");
                List<TrxLedger> list = crudService.findWithNamedQuery("TrxLedger.findAllFromBalanceKredit", param, first, pageSize);
                param.put("coaAccountNo", accNo + "%");
                param.put("ledgerTgl", tahunBulan + "%");
                int rowCount = crudService.countWithNamedQuery("TrxLedger.countFromBalanceKredit", param);
                setRowCount(rowCount);
                setPageSize(pageSize);
                return list;
            }

            @Override
            public TrxLedger getRowData(String rowKey) {
                HashMap<String, Object> param = new HashMap<>();
                param.put("ledgerId", rowKey);
                List<TrxLedger> list = crudService.findWithNamedQuery("TrxLedger.findByLedgerId", param);
                if (list.isEmpty()) {
                    return null;
                } else {
                    return list.get(0);
                }
            }

            @Override
            public Object getRowKey(TrxLedger obj) {
                return obj.getLedgerId();
            }
        };
    }

    public void doFilter() {
        String tahunBulan = new SimpleDateFormat("yyyy-MM").format(new Date());
        String tahun = new SimpleDateFormat("yyyy").format(new Date());

        String thisQuery = "select uuid_in(md5(random()::text || now()::text)::cstring) as balance_sheet_id, coa_id as balance_sheet_coa_id, countingdebit(coa_id, '" + tahunBulan + "') as balance_sheet_debit, countingkredit(coa_id, '" + tahunBulan + "') as balance_sheet_kredit, countingsaldoawal(coa_id, '" + tahun + "-01-01', '" + tahunBulan + "-01') as balance_sheet_saldo_awal, countingmutasi(coa_id, '" + tahunBulan + "') as balance_sheet_mutasi from tbl_coa tc WHERE tc.coa_account_no != '0' ";
        String thisQueryCount = "select COUNT(coa_id) from tbl_coa tc WHERE tc.coa_account_no != '0'";

        if (checkBoxFilterTanggal) {
            tahunBulan = new SimpleDateFormat("yyyy-MM").format(selectedFilterTanggal);
            tahun = new SimpleDateFormat("yyyy").format(selectedFilterTanggal);
            thisQuery = "select uuid_in(md5(random()::text || now()::text)::cstring) as balance_sheet_id, coa_id as balance_sheet_coa_id, countingdebit(coa_id, '" + tahunBulan + "') as balance_sheet_debit, countingkredit(coa_id, '" + tahunBulan + "') as balance_sheet_kredit, countingsaldoawal(coa_id, '" + tahun + "-01-01', '" + tahunBulan + "-01') as balance_sheet_saldo_awal, countingmutasi(coa_id, '" + tahunBulan + "') as balance_sheet_mutasi from tbl_coa tc WHERE tc.coa_account_no != '0' ";
            thisQueryCount = "select COUNT(coa_id) from tbl_coa tc WHERE tc.coa_account_no != '0'";
        }

        if (checkBoxFilterLevel) {
            if (HospitalStringUtils.isNotEmpty(selectedLevel)) {
                thisQuery = thisQuery + " AND (tc.coa_level = '" + selectedLevel + "')";
                thisQueryCount = thisQueryCount + " AND (tc.coa_level = '" + selectedLevel + "')";
            }
        }

        if (checkBoxFilterUnit) {
            thisQuery = thisQuery + " AND tc.coa_account_no = '" + selectedFilterAcc + "'";
            thisQueryCount = thisQueryCount + " AND tc.coa_account_no = '" + selectedFilterAcc + "'";
        }
        thisQuery = thisQuery + " order by tc.coa_account_no ASC";
        listbalanceSheet = crudService.findByNativeQuery(thisQuery, BalanceSheet.class);

        Double totalDebit = 0.0;
        Double totalKredit = 0.0;
        Double totalMutasi = 0.0;
        Double totalNeraca = 0.0;
        Double totalLabaRugi = 0.0;
        Double totalAwalSaldo = 0.0;
        
        for (int i = 0; i < listbalanceSheet.size(); i++) {
            if (checkBoxFilterLevel || checkBoxFilterUnit) {
                totalDebit = totalDebit + nullToZero(listbalanceSheet.get(i).getBalanceSheetDebit());
                totalKredit = totalKredit + nullToZero(listbalanceSheet.get(i).getBalanceSheetKredit());
                totalMutasi = totalMutasi + nullToZero(listbalanceSheet.get(i).getBalanceSheetMutasiSwitch());
                totalNeraca = totalNeraca + nullToZero(listbalanceSheet.get(i).getBalanceSheetNeraca());
                totalLabaRugi = totalLabaRugi + nullToZero(listbalanceSheet.get(i).getBalanceSheetLabaRugi());
                totalAwalSaldo = totalAwalSaldo + nullToZero(listbalanceSheet.get(i).getBalanceSheetSaldoAwal());
            } else {
                if (listbalanceSheet.get(i).getBalanceSheetCoaId().getCoaLevel() == 1) {
                    totalDebit = totalDebit + nullToZero(listbalanceSheet.get(i).getBalanceSheetDebit());
                    totalKredit = totalKredit + nullToZero(listbalanceSheet.get(i).getBalanceSheetKredit());
                    totalMutasi = totalMutasi + nullToZero(listbalanceSheet.get(i).getBalanceSheetMutasiSwitch());
                    totalNeraca = totalNeraca + nullToZero(listbalanceSheet.get(i).getBalanceSheetNeraca());
                    totalLabaRugi = totalLabaRugi + nullToZero(listbalanceSheet.get(i).getBalanceSheetLabaRugi());
                    totalAwalSaldo = totalAwalSaldo + nullToZero(listbalanceSheet.get(i).getBalanceSheetSaldoAwal());
                }
            }
        }
        totalDebitFormatter = HospitalStringUtils.addCommaFromDouble(totalDebit);
        totalKreditFormatter = HospitalStringUtils.addCommaFromDouble(totalKredit);
        totalMutasiFormatter = HospitalStringUtils.addCommaFromDouble(totalMutasi);
        totalNeracaFormatter = HospitalStringUtils.addCommaFromDouble(totalNeraca);
        totalLabaRugiFormatter = HospitalStringUtils.addCommaFromDouble(totalLabaRugi);
        totalSaldoAwalFormatter = HospitalStringUtils.addCommaFromDouble(totalAwalSaldo);

    }

    public void listenerCheckboxFilter() {
        if (checkBoxFilterLevel) {
            toggleFilterLevel = false;
        } else {
            toggleFilterLevel = true;
        }

        if (checkBoxFilterTanggal) {
            toggleFilterTanggal = false;
        } else {
            toggleFilterTanggal = true;
        }
        if (checkBoxFilterUnit) {
            toggleFilterUnit = false;
        } else {
            toggleFilterUnit = true;
        }

        if (checkBoxFilterTanggal || checkBoxFilterUnit || checkBoxFilterLevel) {
            toggleButtonFilter = false;
        } else {
            toggleButtonFilter = true;
        }
    }

    public List<String> findNoAcc(String query) {
        return crudService.findByNativeQuery("SELECT coa_account_no FROM tbl_coa WHERE UPPER(coa_account_no) LIKE '%" + query.toUpperCase() + "%' GROUP BY coa_account_no");
    }

    public LazyDataModel<BalanceSheet> getLazybalanceSheet() {
        return lazybalanceSheet;
    }

    public void setLazybalanceSheet(LazyDataModel<BalanceSheet> lazybalanceSheet) {
        this.lazybalanceSheet = lazybalanceSheet;
    }

    public LazyDataModel<TrxLedger> getLazyTrxLedger() {
        return lazyTrxLedger;
    }

    public void setLazyTrxLedger(LazyDataModel<TrxLedger> lazyTrxLedger) {
        this.lazyTrxLedger = lazyTrxLedger;
    }

    public Boolean getToggleButtonFilter() {
        return toggleButtonFilter;
    }

    public void setToggleButtonFilter(Boolean toggleButtonFilter) {
        this.toggleButtonFilter = toggleButtonFilter;
    }

    public Boolean getCheckBoxFilterTanggal() {
        return checkBoxFilterTanggal;
    }

    public void setCheckBoxFilterTanggal(Boolean checkBoxFilterTanggal) {
        this.checkBoxFilterTanggal = checkBoxFilterTanggal;
    }

    public Boolean getCheckBoxFilterUnit() {
        return checkBoxFilterUnit;
    }

    public void setCheckBoxFilterUnit(Boolean checkBoxFilterUnit) {
        this.checkBoxFilterUnit = checkBoxFilterUnit;
    }

    public Boolean getToggleFilterTanggal() {
        return toggleFilterTanggal;
    }

    public void setToggleFilterTanggal(Boolean toggleFilterTanggal) {
        this.toggleFilterTanggal = toggleFilterTanggal;
    }

    public Boolean getToggleFilterUnit() {
        return toggleFilterUnit;
    }

    public void setToggleFilterUnit(Boolean toggleFilterUnit) {
        this.toggleFilterUnit = toggleFilterUnit;
    }

    public Date getSelectedFilterTanggal() {
        return selectedFilterTanggal;
    }

    public void setSelectedFilterTanggal(Date selectedFilterTanggal) {
        this.selectedFilterTanggal = selectedFilterTanggal;
    }

    public String getSelectedFilterAcc() {
        return selectedFilterAcc;
    }

    public void setSelectedFilterAcc(String selectedFilterAcc) {
        this.selectedFilterAcc = selectedFilterAcc;
    }

    public Boolean getCheckBoxFilterLevel() {
        return checkBoxFilterLevel;
    }

    public void setCheckBoxFilterLevel(Boolean checkBoxFilterLevel) {
        this.checkBoxFilterLevel = checkBoxFilterLevel;
    }

    public Boolean getToggleFilterLevel() {
        return toggleFilterLevel;
    }

    public void setToggleFilterLevel(Boolean toggleFilterLevel) {
        this.toggleFilterLevel = toggleFilterLevel;
    }

    public String getSelectedLevel() {
        return selectedLevel;
    }

    public void setSelectedLevel(String selectedLevel) {
        this.selectedLevel = selectedLevel;
    }

    public String getTotalDebitFormatter() {
        return totalDebitFormatter;
    }

    public void setTotalDebitFormatter(String totalDebitFormatter) {
        this.totalDebitFormatter = totalDebitFormatter;
    }

    public String getTotalKreditFormatter() {
        return totalKreditFormatter;
    }

    public void setTotalKreditFormatter(String totalKreditFormatter) {
        this.totalKreditFormatter = totalKreditFormatter;
    }

    public String getTotalMutasiFormatter() {
        return totalMutasiFormatter;
    }

    public void setTotalMutasiFormatter(String totalMutasiFormatter) {
        this.totalMutasiFormatter = totalMutasiFormatter;
    }

    public String getTotalNeracaFormatter() {
        return totalNeracaFormatter;
    }

    public void setTotalNeracaFormatter(String totalNeracaFormatter) {
        this.totalNeracaFormatter = totalNeracaFormatter;
    }

    public String getTotalLabaRugiFormatter() {
        return totalLabaRugiFormatter;
    }

    public void setTotalLabaRugiFormatter(String totalLabaRugiFormatter) {
        this.totalLabaRugiFormatter = totalLabaRugiFormatter;
    }

    public List<BalanceSheet> getListbalanceSheet() {
        return listbalanceSheet;
    }

    public void setListbalanceSheet(List<BalanceSheet> listbalanceSheet) {
        this.listbalanceSheet = listbalanceSheet;
    }

    public String getSelectedLaporan() {
        return selectedLaporan;
    }

    public void setSelectedLaporan(String selectedLaporan) {
        this.selectedLaporan = selectedLaporan;
    }

    public Double getTotalLabaRugiLocal() {
        return totalLabaRugiLocal;
    }

    public void setTotalLabaRugiLocal(Double totalLabaRugiLocal) {
        this.totalLabaRugiLocal = totalLabaRugiLocal;
    }

    public String getTotalSaldoAwalFormatter() {
        return totalSaldoAwalFormatter;
    }

    public void setTotalSaldoAwalFormatter(String totalSaldoAwalFormatter) {
        this.totalSaldoAwalFormatter = totalSaldoAwalFormatter;
    }

}
