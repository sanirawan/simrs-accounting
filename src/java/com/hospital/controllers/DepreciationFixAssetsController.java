/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital.controllers;

import com.hospital.crud.CrudService;
import com.hospital.models.BalanceSheet;
import com.hospital.models.TrxLedger;
import com.hospital.models.TrxLedgerGroup;
import com.hospital.models.TrxPenyusutan;
import com.hospital.utils.HospitalStringUtils;
import com.sun.javafx.scene.control.skin.VirtualFlow;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;

/**
 *
 * @author arisoft
 */
@ManagedBean(name = "depreciationFixAssetsController")
@ViewScoped
public class DepreciationFixAssetsController extends BaseController implements Serializable {

    @EJB
    private CrudService crudService;
    private List<TrxPenyusutan> listTrxPenyusutan = new ArrayList<>();
    private Date selectedFilterTanggal;
    private String totalNilaiPerolehanFormatter;
    private String totalNilaiAkumulasiFormatter;
    private String totalNilaiBukuAwalFormatter;
    private String totalNilaiPenyusutanFormatter;
    private String totalNilaiBukuAkhirFormatter;

    private Date tglPosting;
    private String jumlahPostingFormatted;
    private String jumlahPostingKreditFormatted;
    private String keteranganPosting;
    private List<TrxLedger> listLedgerPreview = new ArrayList<>();

    public DepreciationFixAssetsController() {
    }

    @PostConstruct
    public void init() {
        loadTable();
    }

    public void loadTable() {
        String periode = "";
        if (selectedFilterTanggal == null) {
            periode = new SimpleDateFormat("yyyyMMdd").format(new Date());
        } else {
            periode = new SimpleDateFormat("yyyyMMdd").format(selectedFilterTanggal);
        }
        String query = "select uuid_in(md5(random()::text || now()::text)::cstring) as penyusutan_id, fix_assets_id as penyusutan_fix_assets_id, countingakumulasi(fix_assets_id) as penyusutan_nilai_akumulasi, fix_assets_nilai_buku as penyusutan_nilai_buku_awal, countingpenyusutan(fix_assets_nilai_penyusutan_perbulan, fix_assets_tgl_perolehan, '" + periode + "') as penyusutan_nilai_penyusutan, countingnilaibuku(fix_assets_acuan_penyusutan, fix_assets_nilai_perolehan, fix_assets_nilai_buku, countingpenyusutan(fix_assets_nilai_penyusutan_perbulan, fix_assets_tgl_perolehan, '" + periode + "')) as penyusutan_nilai_buku_akhir from tbl_fix_assets WHERE fix_assets_nilai_buku > 0";
        listTrxPenyusutan = crudService.findByNativeQuery(query, TrxPenyusutan.class);
        Double totalNilaiPerolehan = 0.0;
        Double totalNilaiAkumulasi = 0.0;
        Double totalNilaiBukuAwal = 0.0;
        Double totalNilaiPenyusutan = 0.0;
        Double totalNilaiBukuAkhir = 0.0;
        for (int i = 0; i < listTrxPenyusutan.size(); i++) {
            totalNilaiPerolehan += listTrxPenyusutan.get(i).getPenyusutanFixAssetsId().getFixAssetsNilaiPerolehan();
            totalNilaiAkumulasi += listTrxPenyusutan.get(i).getPenyusutanNilaiAkumulasi();
            totalNilaiBukuAwal += listTrxPenyusutan.get(i).getPenyusutanNilaiBukuAwal();
            totalNilaiPenyusutan += listTrxPenyusutan.get(i).getPenyusutanNilaiPenyusutan();
            totalNilaiBukuAkhir += listTrxPenyusutan.get(i).getPenyusutanNilaiBukuAkhir();
        }
        totalNilaiPerolehanFormatter = HospitalStringUtils.addCommaFromDouble(totalNilaiPerolehan);
        totalNilaiAkumulasiFormatter = HospitalStringUtils.addCommaFromDouble(totalNilaiAkumulasi);
        totalNilaiBukuAwalFormatter = HospitalStringUtils.addCommaFromDouble(totalNilaiBukuAwal);
        totalNilaiPenyusutanFormatter = HospitalStringUtils.addCommaFromDouble(totalNilaiPenyusutan);
        totalNilaiBukuAkhirFormatter = HospitalStringUtils.addCommaFromDouble(totalNilaiBukuAkhir);

    }

    public void preparePosting() {
        Double jumlahPosting = 0.0;
        Double jumlahPostingKredit = 0.0;
        tglPosting = new Date();
        keteranganPosting = "";
        listLedgerPreview = new ArrayList<>();
        List<TrxLedger> listLedger = new ArrayList();
        for (int i = 0; i < listTrxPenyusutan.size(); i++) {
            listLedger = new ArrayList<>();
            TrxLedgerGroup trxLedgerGroup = new TrxLedgerGroup();
            trxLedgerGroup.setLedgerGroupId(HospitalStringUtils.generateUUID());
            trxLedgerGroup.setLedgerGroupTgl(new Date());
            trxLedgerGroup.setInsertBy(getUsername());
            trxLedgerGroup.setInsertDate(new Date());

            TrxLedger ledgerDebit = new TrxLedger();
            ledgerDebit.setLedgerId(HospitalStringUtils.generateUUID());
            ledgerDebit.setLedgerTgl(new Date());
            ledgerDebit.setLedgerBagian("FA");
            ledgerDebit.setLedgerNoRef(listTrxPenyusutan.get(i).getPenyusutanFixAssetsId().getFixAssetsNo());
            ledgerDebit.setLedgerCoaId(listTrxPenyusutan.get(i).getPenyusutanFixAssetsId().getFixAssetsCoaIdBiaya());
            ledgerDebit.setLedgerDesc(ledgerDebit.getLedgerCoaId().getCoaAccountNama());
            ledgerDebit.setLedgerDebit(listTrxPenyusutan.get(i).getPenyusutanNilaiPenyusutan());
            ledgerDebit.setLedgerKredit(0.0);
            ledgerDebit.setInsertBy(getUsername());
            ledgerDebit.setInsertDate(new Date());
//            selectedNotPostedYet.get(i).set(ledgerDebit);
            ledgerDebit.setLedgerGroupid(trxLedgerGroup);
            listLedger.add(ledgerDebit);
            listLedgerPreview.add(ledgerDebit);

            TrxLedger ledgerKredit = new TrxLedger();
            ledgerKredit.setLedgerId(HospitalStringUtils.generateUUID());
            ledgerKredit.setLedgerTgl(new Date());
            ledgerKredit.setLedgerBagian("FA");
            ledgerKredit.setLedgerNoRef(listTrxPenyusutan.get(i).getPenyusutanFixAssetsId().getFixAssetsNo());
            ledgerKredit.setLedgerCoaId(listTrxPenyusutan.get(i).getPenyusutanFixAssetsId().getFixAssetsCoaIdAkumulasi());
            ledgerKredit.setLedgerDesc(ledgerKredit.getLedgerCoaId().getCoaAccountNama());
            ledgerKredit.setLedgerDebit(0.0);
            ledgerKredit.setLedgerKredit(listTrxPenyusutan.get(i).getPenyusutanNilaiPenyusutan());
            ledgerKredit.setInsertBy(getUsername());
            ledgerKredit.setInsertDate(new Date());
//            selectedNotPostedYet.get(i).setKasirHeadLedgerId(ledgerKredit);
            ledgerKredit.setLedgerGroupid(trxLedgerGroup);
            listLedgerPreview.add(ledgerKredit);

            listLedger.add(ledgerKredit);

            for (int o = 0; o < listLedger.size(); o++) {
                listLedger.get(o).setLedgerSeq(o + 1);
            }

            trxLedgerGroup.setTrxLedger(listLedger);

            listTrxPenyusutan.get(i).setPenyusutanLedgerId(trxLedgerGroup);
        }

        for (int i = 0; i < listLedgerPreview.size(); i++) {
            jumlahPosting = jumlahPosting + listLedgerPreview.get(i).getLedgerDebit();
            jumlahPostingKredit = jumlahPostingKredit + listLedgerPreview.get(i).getLedgerKredit();
        }
        jumlahPostingFormatted = formatterDecimal.format(jumlahPosting) + ".00";
        jumlahPostingKreditFormatted = formatterDecimal.format(jumlahPostingKredit) + ".00";

        executeInstance("PF('widDlgPosting').show()");
        updateInstance("formPosting:dlgPosting");
    }

    public List<TrxPenyusutan> getListTrxPenyusutan() {
        return listTrxPenyusutan;
    }

    public void setListTrxPenyusutan(List<TrxPenyusutan> listTrxPenyusutan) {
        this.listTrxPenyusutan = listTrxPenyusutan;
    }

    public Date getSelectedFilterTanggal() {
        return selectedFilterTanggal;
    }

    public void setSelectedFilterTanggal(Date selectedFilterTanggal) {
        this.selectedFilterTanggal = selectedFilterTanggal;
    }

    public String getTotalNilaiPerolehanFormatter() {
        return totalNilaiPerolehanFormatter;
    }

    public void setTotalNilaiPerolehanFormatter(String totalNilaiPerolehanFormatter) {
        this.totalNilaiPerolehanFormatter = totalNilaiPerolehanFormatter;
    }

    public String getTotalNilaiAkumulasiFormatter() {
        return totalNilaiAkumulasiFormatter;
    }

    public void setTotalNilaiAkumulasiFormatter(String totalNilaiAkumulasiFormatter) {
        this.totalNilaiAkumulasiFormatter = totalNilaiAkumulasiFormatter;
    }

    public String getTotalNilaiBukuAwalFormatter() {
        return totalNilaiBukuAwalFormatter;
    }

    public void setTotalNilaiBukuAwalFormatter(String totalNilaiBukuAwalFormatter) {
        this.totalNilaiBukuAwalFormatter = totalNilaiBukuAwalFormatter;
    }

    public String getTotalNilaiPenyusutanFormatter() {
        return totalNilaiPenyusutanFormatter;
    }

    public void setTotalNilaiPenyusutanFormatter(String totalNilaiPenyusutanFormatter) {
        this.totalNilaiPenyusutanFormatter = totalNilaiPenyusutanFormatter;
    }

    public String getTotalNilaiBukuAkhirFormatter() {
        return totalNilaiBukuAkhirFormatter;
    }

    public void setTotalNilaiBukuAkhirFormatter(String totalNilaiBukuAkhirFormatter) {
        this.totalNilaiBukuAkhirFormatter = totalNilaiBukuAkhirFormatter;
    }

    public Date getTglPosting() {
        return tglPosting;
    }

    public void setTglPosting(Date tglPosting) {
        this.tglPosting = tglPosting;
    }

    public String getJumlahPostingFormatted() {
        return jumlahPostingFormatted;
    }

    public void setJumlahPostingFormatted(String jumlahPostingFormatted) {
        this.jumlahPostingFormatted = jumlahPostingFormatted;
    }

    public String getJumlahPostingKreditFormatted() {
        return jumlahPostingKreditFormatted;
    }

    public void setJumlahPostingKreditFormatted(String jumlahPostingKreditFormatted) {
        this.jumlahPostingKreditFormatted = jumlahPostingKreditFormatted;
    }

    public String getKeteranganPosting() {
        return keteranganPosting;
    }

    public void setKeteranganPosting(String keteranganPosting) {
        this.keteranganPosting = keteranganPosting;
    }

    public List<TrxLedger> getListLedgerPreview() {
        return listLedgerPreview;
    }

    public void setListLedgerPreview(List<TrxLedger> listLedgerPreview) {
        this.listLedgerPreview = listLedgerPreview;
    }

}
