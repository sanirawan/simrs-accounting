/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital.controllers;

import com.hospital.crud.CrudService;
import com.hospital.models.TblAccountSetup;
import com.hospital.models.TblCoa;
import com.hospital.models.TrxCashInDetail;
import com.hospital.models.TrxCashInHead;
import com.hospital.models.TrxLedger;
import com.hospital.models.TrxLedgerGroup;
import com.hospital.utils.HospitalStringUtils;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author arisoft
 */
@ManagedBean(name = "cashInController")
@ViewScoped
public class CashInController extends BaseController implements Serializable {

    @EJB
    private CrudService crudService;
    private LazyDataModel<TrxCashInHead> lazyTrxCashInHead = null;
    private LazyDataModel<TrxCashInHead> lazyTrxCashInHeadNotPostedYet = null;
    private List<TrxCashInHead> selectedNotPostedYet = new ArrayList<>();
    private TrxCashInHead trxCashInHead = new TrxCashInHead();
    private List<TrxCashInDetail> listDetail = new ArrayList<>();
    private List<TrxCashInDetail> listDetailInEdit = new ArrayList<>();
    private TrxCashInDetail selectedTrxCashInDetail = new TrxCashInDetail();
    private List<TblAccountSetup> listAccountSetup = new ArrayList<>();
    private Double subTotal;
    private String subTotalFormatter;
    private String selectedAccount;
    private Date tglPosting;
    private String jumlahPostingFormatted;
    private String jumlahPostingKreditFormatted;
    private String keteranganPosting;
    private List<TrxLedger> listLedgerPreview = new ArrayList<>();
    private List<TrxCashInDetail> listTableDetail;
    private List<TblCoa> listAccount = new ArrayList<>();
    private String selectedJenisAccount = "Account Kas";

    /**
     * =============== PENCARIAN DATA
     */
    private Boolean toggleButtonFilter = true;

    private Boolean checkBoxFilterTanggal = false;
    private Boolean checkBoxFilterUnit = false;

    private Boolean toggleFilterTanggal = true;
    private Boolean toggleFilterUnit = true;

    private Date selectedFilterTanggal;
    private String selectedFilterAcc = "";
    private String searchNoRef = "";

    /**
     * Creates a new instance of CashInController
     */
    public CashInController() {
    }

    @PostConstruct
    public void init() {
        loadTable();
    }

    private void loadTable() {
        lazyTrxCashInHead = new LazyDataModel<TrxCashInHead>() {
            @Override
            public List<TrxCashInHead> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> map) {
                List<TrxCashInHead> list = crudService.findWithNamedQuery("TrxCashInHead.findAllWithStatus", first, pageSize);
                int rowCount = crudService.countWithNamedQuery("TrxCashInHead.countWithStatus");
                setRowCount(rowCount);
                setPageSize(pageSize);
                return list;
            }

            @Override
            public TrxCashInHead getRowData(String rowKey) {
                HashMap<String, Object> param = new HashMap<>();
                param.put("cashInHeadId", rowKey);
                List<TrxCashInHead> list = crudService.findWithNamedQuery("TrxCashInHead.findByCashInHeadId", param);
                if (list.isEmpty()) {
                    return null;
                } else {
                    return list.get(0);
                }
            }

            @Override
            public Object getRowKey(TrxCashInHead obj) {
                return obj.getCashInHeadId();
            }
        };

        lazyTrxCashInHeadNotPostedYet = new LazyDataModel<TrxCashInHead>() {
            @Override
            public List<TrxCashInHead> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> map) {
                HashMap<String, Object> param = new HashMap<>();
                param.put("insertBy", getUsername());
                List<TrxCashInHead> list = crudService.findWithNamedQuery("TrxCashInHead.findAllNotPostedYet", param, first, pageSize);
                param.put("insertBy", getUsername());
                int rowCount = crudService.countWithNamedQuery("TrxCashInHead.countNotPostedYet", param);
                setRowCount(rowCount);
                setPageSize(pageSize);
                return list;
            }

            @Override
            public TrxCashInHead getRowData(String rowKey) {
                HashMap<String, Object> param = new HashMap<>();
                param.put("cashInHeadId", rowKey);
                List<TrxCashInHead> list = crudService.findWithNamedQuery("TrxCashInHead.findByCashInHeadId", param);
                if (list.isEmpty()) {
                    return null;
                } else {
                    return list.get(0);
                }
            }

            @Override
            public Object getRowKey(TrxCashInHead obj) {
                return obj.getCashInHeadId();
            }
        };
    }

    public void doFilter() {
        lazyTrxCashInHead = new LazyDataModel<TrxCashInHead>() {
            @Override
            public List<TrxCashInHead> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> map) {
                String thisQuery = "SELECT t FROM TrxCashInHead t WHERE t.cashInHeadStatus != 1 AND t.cashInHeadIsOut = FALSE";
                String thisQueryCount = "SELECT COUNT(t) FROM TrxCashInHead t WHERE t.cashInHeadStatus != 1 AND t.cashInHeadIsOut = FALSE";
                Set<Map.Entry<String, Object>> rawParameters = map.entrySet();

                if (checkBoxFilterTanggal) {
                    thisQuery = thisQuery + " AND (t.cashInHeadTgl BETWEEN '" + dateQueryFormat("from", selectedFilterTanggal) + "' AND '" + dateQueryFormat("to", selectedFilterTanggal) + "')";
                    thisQueryCount = thisQueryCount + " AND (t.cashInHeadTgl BETWEEN '" + dateQueryFormat("from", selectedFilterTanggal) + "' AND '" + dateQueryFormat("to", selectedFilterTanggal) + "')";
                }
                if (checkBoxFilterUnit) {
                    thisQuery = thisQuery + " AND t.cashInHeadCoaId.coaAccountNo = '" + selectedFilterAcc + "'";
                    thisQueryCount = thisQueryCount + " AND t.cashInHeadCoaId.coaAccountNo = '" + selectedFilterAcc + "'";
                }
                thisQuery = thisQuery + " ORDER BY t.cashInHeadTgl DESC";
                List<TrxCashInHead> list = crudService.findWithCreateQuery(thisQuery, first, pageSize);
                setRowCount(crudService.countWithCreateQuery(thisQueryCount));
                setPageSize(pageSize);
                return list;
            }
        };

        lazyTrxCashInHeadNotPostedYet = new LazyDataModel<TrxCashInHead>() {
            @Override
            public List<TrxCashInHead> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> map) {
                String thisQuery = "SELECT t FROM TrxCashInHead t WHERE t.cashInHeadStatus != 1 AND t.cashInHeadIsOut = FALSE AND t.cashInHeadLedgerGroupId IS NULL AND t.insertBy = '" + getUsername() + "'";
                String thisQueryCount = "SELECT COUNT(t) FROM TrxCashInHead t WHERE t.cashInHeadStatus != 1 AND t.cashInHeadIsOut = FALSE AND t.cashInHeadLedgerGroupId IS NULL AND t.insertBy = '" + getUsername() + "'";
                Set<Map.Entry<String, Object>> rawParameters = map.entrySet();

                if (checkBoxFilterTanggal) {
                    thisQuery = thisQuery + " AND (t.cashInHeadTgl BETWEEN '" + dateQueryFormat("from", selectedFilterTanggal) + "' AND '" + dateQueryFormat("to", selectedFilterTanggal) + "')";
                    thisQueryCount = thisQueryCount + " AND (t.cashInHeadTgl BETWEEN '" + dateQueryFormat("from", selectedFilterTanggal) + "' AND '" + dateQueryFormat("to", selectedFilterTanggal) + "')";
                }
                if (checkBoxFilterUnit) {
                    thisQuery = thisQuery + " AND t.cashInHeadCoaId.coaAccountNo = '" + selectedFilterAcc + "'";
                    thisQueryCount = thisQueryCount + " AND t.cashInHeadCoaId.coaAccountNo = '" + selectedFilterAcc + "'";
                }
                thisQuery = thisQuery + " ORDER BY t.cashInHeadTgl DESC";
                List<TrxCashInHead> list = crudService.findWithCreateQuery(thisQuery, first, pageSize);
                setRowCount(crudService.countWithCreateQuery(thisQueryCount));
                setPageSize(pageSize);
                return list;
            }
        };
    }

    public void loadListAccount() {
        listAccount = crudService.findByNativeQuery("SELECT * FROM tbl_coa WHERE coa_is_transaction = TRUE AND coa_account_no LIKE (SELECT coa_account_no FROM tbl_coa tc inner join tbl_account_special tas on (tc.coa_id = tas.account_special_coa_id) WHERE tas.account_special_function = '" + selectedJenisAccount + "') || '%' ORDER BY coa_account_no ASC", TblCoa.class);
    }

    public void listenerCheckboxFilter() {
        if (checkBoxFilterTanggal) {
            toggleFilterTanggal = false;
        } else {
            toggleFilterTanggal = true;
        }
        if (checkBoxFilterUnit) {
            toggleFilterUnit = false;
        } else {
            toggleFilterUnit = true;
        }

        if (checkBoxFilterTanggal || checkBoxFilterUnit) {
            toggleButtonFilter = false;
        } else {
            toggleButtonFilter = true;
        }
    }

    public void onRowToggle(ToggleEvent event) {
        TrxCashInHead trx = (TrxCashInHead) event.getData();
//        listTableDetail = crudService.findByNativeQuery("select * from trx_tagihan_ri_detail where tagihan_ri_detail_head_id = '" + trx.getTagihanRiHeadId() + "'", TrxTagihanRiDetail.class);
        listTableDetail = trx.getTrxCashInDetail();
    }

    public List<String> findNoRef(String query) {
        return crudService.findByNativeQuery("SELECT cash_in_head_no_ref FROM trx_cash_in_head WHERE UPPER(cash_in_head_no_ref) LIKE '%" + query.toUpperCase() + "%' AND cash_in_head_is_out = FALSE GROUP BY cash_in_head_no_ref");
    }

    public List<String> findNoAcc(String query) {
        return crudService.findByNativeQuery("SELECT coa_account_no FROM tbl_coa WHERE UPPER(coa_account_no) LIKE '%" + query.toUpperCase() + "%' GROUP BY coa_account_no");
    }

    public void searchByNoRef() {
        lazyTrxCashInHead = new LazyDataModel<TrxCashInHead>() {
            @Override
            public List<TrxCashInHead> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> map) {
                String thisQuery = "SELECT t FROM TrxCashInHead t WHERE t.cashInHeadNoRef = '" + searchNoRef + "' AND t.cashInHeadIsOut = FALSE";
                String countQuery = "SELECT COUNT(t) FROM TrxCashInHead t WHERE t.cashInHeadNoRef = '" + searchNoRef + "' AND t.cashInHeadIsOut = FALSE";
                Set<Map.Entry<String, Object>> rawParameters = map.entrySet();
                List<TrxCashInHead> list = crudService.findWithCreateQuery(thisQuery, first, pageSize);
                setRowCount(crudService.countWithCreateQuery(countQuery));
                setPageSize(pageSize);
                return list;
            }
        };

        lazyTrxCashInHeadNotPostedYet = new LazyDataModel<TrxCashInHead>() {
            @Override
            public List<TrxCashInHead> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> map) {
                String thisQuery = "SELECT t FROM TrxCashInHead t WHERE t.cashInHeadNoRef = '" + searchNoRef + "' AND t.cashInHeadIsOut = FALSE AND t.cashInHeadLedgerGroupId IS NULL AND t.insertBy = '" + getUsername() + "'";
                String countQuery = "SELECT COUNT(t) FROM TrxCashInHead t WHERE t.cashInHeadNoRef = '" + searchNoRef + "' AND t.cashInHeadIsOut = FALSE AND t.cashInHeadLedgerGroupId IS NULL AND t.insertBy = '" + getUsername() + "'";
                Set<Map.Entry<String, Object>> rawParameters = map.entrySet();
                List<TrxCashInHead> list = crudService.findWithCreateQuery(thisQuery, first, pageSize);
                setRowCount(crudService.countWithCreateQuery(countQuery));
                setPageSize(pageSize);
                return list;
            }
        };
    }

    public void prepareDialogCashIn() {
        listDetailInEdit = new ArrayList<>();
        trxCashInHead = new TrxCashInHead();
        selectedAccount = "";
        selectedJenisAccount = "Account Kas";
        listDetail = new ArrayList<>();
        HashMap<String, Object> param = new HashMap<>();
        param.put("accountSetupType", "KM");
        listAccountSetup = crudService.findWithNamedQuery("TblAccountSetup.findByAccountSetupType", param);
        TrxCashInDetail detail = new TrxCashInDetail();
        detail.setCashInDetailId(HospitalStringUtils.generateUUID());
        detail.setBagian("Pilih Bagian");
        detail.setAccountNo("Pilih Account");
        detail.setCashInDetailJumlah(0.0);
        detail.setCashInDetailJumlahFormatter("0");
        listDetail.add(detail);
        subTotalFormatter = "0";
        loadListAccount();
        executeInstance("PF('widDlgKasir').show()");
        updateInstance("formDlgKasir:dlgKasir");
    }

    public void prepareDialogEditCashIn(TrxCashInHead selected) {
        listDetailInEdit = new ArrayList<>();
        trxCashInHead = selected;
        selectedAccount = trxCashInHead.getCashInHeadCoaId().getCoaAccountNo();
        selectedJenisAccount = trxCashInHead.getCashInHeadJenisAccount();
        listDetail = trxCashInHead.getTrxCashInDetail();
        HashMap<String, Object> param = new HashMap<>();
        param.put("cashInDetailHeadId", trxCashInHead);
        listDetailInEdit = crudService.findWithNamedQuery("TrxCashInDetail.findByCashInDetailHeadId", param);
        for (int i = 0; i < listDetail.size(); i++) {
            listDetail.get(i).setBagian(listDetail.get(i).getCashInDetailAccountSetupId().getAccountSetupName());
            listDetail.get(i).setAccountNo(listDetail.get(i).getCashInDetailAccountSetupId().getAccountSetupCoaId().getCoaAccountNo());
            listDetail.get(i).setAccountNama(listDetail.get(i).getCashInDetailAccountSetupId().getAccountSetupCoaId().getCoaAccountNama());
            listDetail.get(i).setCashInDetailJumlahFormatter(HospitalStringUtils.addCommaFromDouble(listDetail.get(i).getCashInDetailJumlah())); 
        }
        param = new HashMap<>();
        param.put("accountSetupType", "KM");
        listAccountSetup = crudService.findWithNamedQuery("TblAccountSetup.findByAccountSetupType", param);
        subTotalFormatter = HospitalStringUtils.addCommaFromDouble(trxCashInHead.getCashInHeadJumlah());
        loadListAccount();
        executeInstance("PF('widDlgKasir').show()");
        updateInstance("formDlgKasir:dlgKasir");
    }

    public List<String> findAccountNo(String account) {
        List<String> results;
        results = crudService.findByNativeQuery("select CONCAT(coa_account_no,' - ', coa_account_nama) FROM tbl_coa WHERE coa_account_no LIKE '" + account + "%'");
        return results;
    }

    public void insertNewRowDetail() {
        TrxCashInDetail det = new TrxCashInDetail();
        det.setCashInDetailId(HospitalStringUtils.generateUUID());
        det.setBagian("Pilih Bagian");
        det.setAccountNo("Pilih Account");
        det.setCashInDetailJumlah(0.0);
        det.setCashInDetailJumlahFormatter("0");
        listDetail.add(det);
        updateInstance("formDlgKasir:dtCashIn");
    }

    public void bagianOnChange(TrxCashInDetail detail) {
        HashMap<String, Object> param = new HashMap<>();
        param.put("accountSetupName", detail.getBagian());
        List<TblAccountSetup> setups = crudService.findWithNamedQuery("TblAccountSetup.findByAccountSetupName", param);
//        detail.setCashInDetailId(HospitalStringUtils.generateUUID());
        detail.setCashInDetailAccountSetupId(setups.get(0));
        detail.setCashInDetailCoaId(setups.get(0).getAccountSetupCoaId());
        detail.setAccountNo(detail.getCashInDetailCoaId().getCoaAccountNo());
        detail.setAccountNama(detail.getCashInDetailCoaId().getCoaAccountNama());
//        boolean found = false;
//        for (int i = 0; i < listDetail.size(); i++) {
//            if (listDetail.get(i).getCashInDetailId() == null) {
//                found = true;
//            }
//        }
//        if (!found) {
//            TrxCashInDetail det = new TrxCashInDetail();
//            det.setBagian("Pilih Bagian");
//            det.setAccountNo("Pilih Account");
//            det.setCashInDetailJumlah(0.0);
//            listDetail.add(det);
//        }
    }

    public void jumlahDetailOnChange(TrxCashInDetail detail) {
        subTotal = 0.0;
        for (int i = 0; i < listDetail.size(); i++) {
            subTotal = subTotal + HospitalStringUtils.removeCommaFromString(listDetail.get(i).getCashInDetailJumlahFormatter());
        }

        subTotalFormatter = formatterDecimal.format(subTotal);
    }

    public void doSaveCashIn() {
        try {
//            String noAccount = selectedAccount.substring(0, selectedAccount.indexOf(" "));
            HashMap<String, Object> param = new HashMap<>();
            param.put("coaAccountNo", selectedAccount);
            List<TblCoa> listCoa = crudService.findWithNamedQuery("TblCoa.findByCoaAccountNo", param);
            if (!HospitalStringUtils.isNotEmpty(trxCashInHead.getCashInHeadId())) {
                trxCashInHead.setCashInHeadId(HospitalStringUtils.generateUUID());
            }
            trxCashInHead.setCashInHeadNoRef(generateKey());
            trxCashInHead.setCashInHeadCoaId(listCoa.get(0));
            trxCashInHead.setCashInHeadJenisAccount(selectedJenisAccount);
            trxCashInHead.setInsertBy(getUsername());
            trxCashInHead.setInsertDate(new Date());
            trxCashInHead.setCashInHeadStatus(0);
            trxCashInHead.setCashInHeadJumlah(HospitalStringUtils.removeCommaFromString(subTotalFormatter));
            trxCashInHead.setCashInHeadIsOut(false);
            setHeadToDetail(trxCashInHead, listDetail);
            trxCashInHead.setTrxCashInDetail(listDetail);
            List<TrxCashInDetail> listDeleted = new ArrayList<>();
            boolean found = false;
            for (int i = 0; i < listDetailInEdit.size(); i++) {
                found = false;
                for (int j = 0; j < listDetail.size(); j++) {
                    if(listDetailInEdit.get(i).getCashInDetailId().equals(listDetail.get(j).getCashInDetailId())){
                        found = true;
                        break;
                    }
                }
                if(found == false){
                    listDeleted.add(listDetailInEdit.get(i));
                }
            }
            crudService.update(trxCashInHead);
            for (int i = 0; i < listDeleted.size(); i++) {
                crudService.delete(listDeleted.get(i));
            }
            updateInstance("thisForm:thisData");
            updateInstance("formDlgKasir:panelPasen");
            createMessage("INFO", "Kas Masuk berhasil disimpan.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setHeadToDetail(TrxCashInHead head, List<TrxCashInDetail> list) {
        for (int i = 0; i < list.size(); i++) {
            list.get(i).setInsertBy(getUsername());
            list.get(i).setInsertDate(new Date(0));
            list.get(i).setCashInDetailJumlah(HospitalStringUtils.removeCommaFromString(list.get(i).getCashInDetailJumlahFormatter()));
            list.get(i).setCashInDetailHeadId(head);
        }
    }

    private String generateKey() {
        String tahun = "CI" + new SimpleDateFormat("yyMM").format(new Date());
        List<Integer> list = crudService.findByNativeQuery("select max(substring(cash_in_head_no_ref, 7,4))::int from trx_cash_in_head where cash_in_head_no_ref like '" + tahun + "%'");
        if (list.get(0) == null) {
            return tahun + "0001";
        } else {
            Integer number = list.get(0) + 1;
            String formatted = tahun + String.format("%04d", number);
            return formatted;
        }
    }

    public void deleteDetail(TrxCashInDetail detail) {
        for (int i = 0; i < listDetail.size(); i++) {
            if (listDetail.get(i).getCashInDetailId().equals(detail.getCashInDetailId())) {
                listDetail.remove(i);
                break;
            }
        }
        jumlahDetailOnChange(detail);
    }

    public void preparePosting() {
        Double jumlahPosting = 0.0;
        Double jumlahPostingKredit = 0.0;
        tglPosting = new Date();
        keteranganPosting = "";
        listLedgerPreview = new ArrayList<>();
        List<TrxLedger> listLedger = new ArrayList();
        for (int i = 0; i < selectedNotPostedYet.size(); i++) {
            listLedger = new ArrayList<>();
            TrxLedgerGroup trxLedgerGroup = new TrxLedgerGroup();
            trxLedgerGroup.setLedgerGroupId(HospitalStringUtils.generateUUID());
            trxLedgerGroup.setLedgerGroupTgl(new Date());
            trxLedgerGroup.setInsertBy(getUsername());
            trxLedgerGroup.setInsertDate(new Date());

            TrxLedger ledgerDebit = new TrxLedger();
            ledgerDebit.setLedgerId(HospitalStringUtils.generateUUID());
            ledgerDebit.setLedgerTgl(new Date());
            ledgerDebit.setLedgerBagian("CI");
            ledgerDebit.setLedgerNoRef(selectedNotPostedYet.get(i).getCashInHeadNoRef());
            ledgerDebit.setLedgerCoaId(selectedNotPostedYet.get(i).getCashInHeadCoaId());
            ledgerDebit.setLedgerDesc(ledgerDebit.getLedgerCoaId().getCoaAccountNama());
            ledgerDebit.setLedgerDebit(selectedNotPostedYet.get(i).getCashInHeadJumlah());
            ledgerDebit.setLedgerKredit(0.0);
            ledgerDebit.setInsertBy(getUsername());
            ledgerDebit.setInsertDate(new Date());
//            selectedNotPostedYet.get(i).set(ledgerDebit);
            ledgerDebit.setLedgerGroupid(trxLedgerGroup);
            listLedger.add(ledgerDebit);
            listLedgerPreview.add(ledgerDebit);

            for (int j = 0; j < selectedNotPostedYet.get(i).getTrxCashInDetail().size(); j++) {
                TrxLedger ledgerKredit = new TrxLedger();
                ledgerKredit.setLedgerId(HospitalStringUtils.generateUUID());
                ledgerKredit.setLedgerTgl(new Date());
                ledgerKredit.setLedgerBagian("CI");
                ledgerKredit.setLedgerNoRef(selectedNotPostedYet.get(i).getCashInHeadNoRef());
                ledgerKredit.setLedgerCoaId(selectedNotPostedYet.get(i).getTrxCashInDetail().get(j).getCashInDetailCoaId());
                ledgerKredit.setLedgerDesc(ledgerKredit.getLedgerCoaId().getCoaAccountNama());
                ledgerKredit.setLedgerDebit(0.0);
                ledgerKredit.setLedgerKredit(selectedNotPostedYet.get(i).getTrxCashInDetail().get(j).getCashInDetailJumlah());
                ledgerKredit.setInsertBy(getUsername());
                ledgerKredit.setInsertDate(new Date());
//            selectedNotPostedYet.get(i).setKasirHeadLedgerId(ledgerKredit);
                ledgerKredit.setLedgerGroupid(trxLedgerGroup);
                listLedgerPreview.add(ledgerKredit);

                listLedger.add(ledgerKredit);
            }

            for (int o = 0; o < listLedger.size(); o++) {
                listLedger.get(o).setLedgerSeq(o + 1);
            }

            trxLedgerGroup.setTrxLedger(listLedger);

            selectedNotPostedYet.get(i).setCashInHeadLedgerGroupId(trxLedgerGroup);
        }

        for (int i = 0; i < listLedgerPreview.size(); i++) {
            jumlahPosting = jumlahPosting + listLedgerPreview.get(i).getLedgerDebit();
            jumlahPostingKredit = jumlahPostingKredit + listLedgerPreview.get(i).getLedgerKredit();
        }
        jumlahPostingFormatted = formatterDecimal.format(jumlahPosting) + ".00";
        jumlahPostingKreditFormatted = formatterDecimal.format(jumlahPostingKredit) + ".00";

        executeInstance("PF('widDlgPosting').show()");
        updateInstance("formPosting:dlgPosting");
    }

    public void doPosting() {
        try {
            List<TrxCashInHead> list;
            for (int i = 0; i < selectedNotPostedYet.size(); i++) {
                for (int j = 0; j < selectedNotPostedYet.get(i).getCashInHeadLedgerGroupId().getTrxLedger().size(); j++) {
                    selectedNotPostedYet.get(i).getCashInHeadLedgerGroupId().getTrxLedger().get(j).setLedgerKeterangan(keteranganPosting);
                }
                list = new ArrayList<>();
                list.add(selectedNotPostedYet.get(i));
                selectedNotPostedYet.get(i).getCashInHeadLedgerGroupId().setListTrxCashInHead(list);
            }
            crudService.updateMultiple(selectedNotPostedYet);
            executeInstance("PF('widDlgPosting').hide()");
            updateInstance("thisForm:thisData");
            createMessage("INFO", "Data berhasil diposting.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public LazyDataModel<TrxCashInHead> getLazyTrxCashInHead() {
        return lazyTrxCashInHead;
    }

    public void setLazyTrxCashInHead(LazyDataModel<TrxCashInHead> lazyTrxCashInHead) {
        this.lazyTrxCashInHead = lazyTrxCashInHead;
    }

    public Boolean getToggleButtonFilter() {
        return toggleButtonFilter;
    }

    public void setToggleButtonFilter(Boolean toggleButtonFilter) {
        this.toggleButtonFilter = toggleButtonFilter;
    }

    public Boolean getCheckBoxFilterTanggal() {
        return checkBoxFilterTanggal;
    }

    public void setCheckBoxFilterTanggal(Boolean checkBoxFilterTanggal) {
        this.checkBoxFilterTanggal = checkBoxFilterTanggal;
    }

    public Boolean getCheckBoxFilterUnit() {
        return checkBoxFilterUnit;
    }

    public void setCheckBoxFilterUnit(Boolean checkBoxFilterUnit) {
        this.checkBoxFilterUnit = checkBoxFilterUnit;
    }

    public Boolean getToggleFilterTanggal() {
        return toggleFilterTanggal;
    }

    public void setToggleFilterTanggal(Boolean toggleFilterTanggal) {
        this.toggleFilterTanggal = toggleFilterTanggal;
    }

    public Boolean getToggleFilterUnit() {
        return toggleFilterUnit;
    }

    public void setToggleFilterUnit(Boolean toggleFilterUnit) {
        this.toggleFilterUnit = toggleFilterUnit;
    }

    public Date getSelectedFilterTanggal() {
        return selectedFilterTanggal;
    }

    public void setSelectedFilterTanggal(Date selectedFilterTanggal) {
        this.selectedFilterTanggal = selectedFilterTanggal;
    }

    public String getSelectedFilterAcc() {
        return selectedFilterAcc;
    }

    public void setSelectedFilterAcc(String selectedFilterAcc) {
        this.selectedFilterAcc = selectedFilterAcc;
    }

    public String getSearchNoRef() {
        return searchNoRef;
    }

    public void setSearchNoRef(String searchNoRef) {
        this.searchNoRef = searchNoRef;
    }

    public TrxCashInHead getTrxCashInHead() {
        return trxCashInHead;
    }

    public void setTrxCashInHead(TrxCashInHead trxCashInHead) {
        this.trxCashInHead = trxCashInHead;
    }

    public List<TrxCashInDetail> getListDetail() {
        return listDetail;
    }

    public void setListDetail(List<TrxCashInDetail> listDetail) {
        this.listDetail = listDetail;
    }

    public TrxCashInDetail getSelectedTrxCashInDetail() {
        return selectedTrxCashInDetail;
    }

    public void setSelectedTrxCashInDetail(TrxCashInDetail selectedTrxCashInDetail) {
        this.selectedTrxCashInDetail = selectedTrxCashInDetail;
    }

    public List<TblAccountSetup> getListAccountSetup() {
        return listAccountSetup;
    }

    public void setListAccountSetup(List<TblAccountSetup> listAccountSetup) {
        this.listAccountSetup = listAccountSetup;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    public String getSubTotalFormatter() {
        return subTotalFormatter;
    }

    public void setSubTotalFormatter(String subTotalFormatter) {
        this.subTotalFormatter = subTotalFormatter;
    }

    public String getSelectedAccount() {
        return selectedAccount;
    }

    public void setSelectedAccount(String selectedAccount) {
        this.selectedAccount = selectedAccount;
    }

    public LazyDataModel<TrxCashInHead> getLazyTrxCashInHeadNotPostedYet() {
        return lazyTrxCashInHeadNotPostedYet;
    }

    public void setLazyTrxCashInHeadNotPostedYet(LazyDataModel<TrxCashInHead> lazyTrxCashInHeadNotPostedYet) {
        this.lazyTrxCashInHeadNotPostedYet = lazyTrxCashInHeadNotPostedYet;
    }

    public List<TrxCashInHead> getSelectedNotPostedYet() {
        return selectedNotPostedYet;
    }

    public void setSelectedNotPostedYet(List<TrxCashInHead> selectedNotPostedYet) {
        this.selectedNotPostedYet = selectedNotPostedYet;
    }

    public Date getTglPosting() {
        return tglPosting;
    }

    public void setTglPosting(Date tglPosting) {
        this.tglPosting = tglPosting;
    }

    public String getJumlahPostingFormatted() {
        return jumlahPostingFormatted;
    }

    public void setJumlahPostingFormatted(String jumlahPostingFormatted) {
        this.jumlahPostingFormatted = jumlahPostingFormatted;
    }

    public String getJumlahPostingKreditFormatted() {
        return jumlahPostingKreditFormatted;
    }

    public void setJumlahPostingKreditFormatted(String jumlahPostingKreditFormatted) {
        this.jumlahPostingKreditFormatted = jumlahPostingKreditFormatted;
    }

    public String getKeteranganPosting() {
        return keteranganPosting;
    }

    public void setKeteranganPosting(String keteranganPosting) {
        this.keteranganPosting = keteranganPosting;
    }

    public List<TrxLedger> getListLedgerPreview() {
        return listLedgerPreview;
    }

    public void setListLedgerPreview(List<TrxLedger> listLedgerPreview) {
        this.listLedgerPreview = listLedgerPreview;
    }

    public List<TrxCashInDetail> getListTableDetail() {
        return listTableDetail;
    }

    public void setListTableDetail(List<TrxCashInDetail> listTableDetail) {
        this.listTableDetail = listTableDetail;
    }

    public List<TblCoa> getListAccount() {
        return listAccount;
    }

    public void setListAccount(List<TblCoa> listAccount) {
        this.listAccount = listAccount;
    }

    public String getSelectedJenisAccount() {
        return selectedJenisAccount;
    }

    public void setSelectedJenisAccount(String selectedJenisAccount) {
        this.selectedJenisAccount = selectedJenisAccount;
    }

    public List<TrxCashInDetail> getListDetailInEdit() {
        return listDetailInEdit;
    }

    public void setListDetailInEdit(List<TrxCashInDetail> listDetailInEdit) {
        this.listDetailInEdit = listDetailInEdit;
    }
    

}
