/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital.controllers;

import com.hospital.crud.CrudService;
import com.hospital.models.TblCoa;
import com.hospital.models.TblKelompokCoa;
import com.hospital.models.TblSaldoAwal;
import com.hospital.utils.HospitalStringUtils;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author eh-regex
 */
@ManagedBean(name = "masterCOAController")
@ViewScoped
public class MasterCOAController extends BaseController implements Serializable {

    @EJB
    private CrudService crudService;
    private final HospitalStringUtils hospitalStringUtils = new HospitalStringUtils();
    private LazyDataModel<TblCoa> lazyTblCoa = null;
    private TblCoa tblCoa = new TblCoa();
    private String nameTemp = "";

    private List<TblKelompokCoa> listKelompokCoa = new ArrayList<>();
    private String selectedIdKelompokCoa;
    private String kelompokCode;
    private String coaCode;
//    private Integer coaLevel;
    private List<TblCoa> listParentCoa = new ArrayList<>();
    private String selectedIdParentCoa;
    private Boolean disabledSaldoAwal = true;

    /**
     * Creates a new instance of MasterCOAController
     */
    public MasterCOAController() {
    }

    public void refreshDataTable() {
        loadTable();
    }

    @PostConstruct
    public void init() {
        loadTable();

        listKelompokCoa = crudService.findWithNamedQuery("TblKelompokCoa.findAllSortByKode");

        selectedIdKelompokCoa = null;
        kelompokCode = null;
        coaCode = null;
        listParentCoa = new ArrayList<>();
        selectedIdParentCoa = null;
    }

    private void loadTable() {
        lazyTblCoa = new LazyDataModel<TblCoa>() {
            @Override
            public List<TblCoa> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> map) {
                String thisQuery = "SELECT t FROM TblCoa t WHERE t.coaAccountNo != '0'";
                Set<Map.Entry<String, Object>> rawParameters = map.entrySet();

                Boolean isSearch = false;
                for (Map.Entry<String, Object> entry : rawParameters) {
                    isSearch = true;
                    thisQuery = thisQuery + " AND LOWER(t." + entry.getKey() + ") LIKE '%" + entry.getValue().toString().toLowerCase() + "%'";
                }

                if (sortField != null) {
                    String sortMethod = "DESC";
                    if (sortOrder.toString().equalsIgnoreCase("ASCENDING")) {
                        sortMethod = "ASC";
                    }
                    thisQuery = thisQuery + " ORDER BY t." + sortField + " " + sortMethod;
                } else {
                    thisQuery = thisQuery + " ORDER BY t.coaAccountNo ASC";
                }

                List<TblCoa> list = crudService.findWithCreateQuery(thisQuery, first, pageSize);
                if (isSearch) {
                    setRowCount(list.size());
                } else {
                    int rowCount = crudService.countWithNamedQuery("TblCoa.countAll");
                    setRowCount(rowCount);
                }
                setPageSize(pageSize);
                return list;
            }

            @Override
            public TblCoa getRowData(String rowKey) {
                HashMap<String, Object> param = new HashMap<>();
                param.put("coaId", rowKey);
                List<TblCoa> list = crudService.findWithNamedQuery("TblCoa.findByCoaId", param);
                if (list.isEmpty()) {
                    return null;
                } else {
                    return list.get(0);
                }
            }

            @Override
            public Object getRowKey(TblCoa obj) {
                return obj.getCoaId();
            }
        };
    }

    public void kelompokOnChange() {
        kelompokCode = selectedIdKelompokCoa.substring(0, selectedIdKelompokCoa.indexOf(" "));
    }

    public void levelOnChange() {
        listParentCoa = new ArrayList<>();
        selectedIdParentCoa = null;

        if (tblCoa.getCoaLevel() > 1) {
            HashMap<String, Object> param = new HashMap();
            Integer parentLevel = tblCoa.getCoaLevel() - 1;
            param.put("coaLevel", parentLevel);
            listParentCoa = crudService.findWithNamedQuery("TblCoa.findByCoaLevel", param);
        }
    }

    public String findParentName(String parentId) {
        if ((parentId != null) && (!parentId.equals(""))) {
            HashMap<String, Object> param = new HashMap();
            param.put("coaId", parentId);
            List<TblCoa> listParentCoa = new ArrayList<>();
            listParentCoa = crudService.findWithNamedQuery("TblCoa.findByCoaId", param);
            return listParentCoa.get(0).getCoaAccountNama();
        } else {
            return "-";
        }
    }

    //    =INSERT===================================================================
    //<editor-fold defaultstate="collapsed" desc="INSERT">
    public void doInsert() {
        selectedIdKelompokCoa = null;
        kelompokCode = null;
        coaCode = null;
        listParentCoa = new ArrayList<>();
        selectedIdParentCoa = null;

        tblCoa = new TblCoa();
        executeInstance("PF('widDlgInsert').show()");
        updateInstance("formDlgInsert:pgridInsert");
    }

    public void insert() {
        try {
            HashMap<String, Object> param = new HashMap<>();
            param.put("kelompokCoaId", selectedIdKelompokCoa.substring(selectedIdKelompokCoa.lastIndexOf(" ") + 1));
            List<TblKelompokCoa> thisListKelompokCoa = crudService.findWithNamedQuery("TblKelompokCoa.findByKelompokCoaId", param);
            tblCoa.setTblKelompokCoa(thisListKelompokCoa.get(0));

            if (selectedIdParentCoa != null) {
                param = new HashMap<>();
                param.put("coaId", selectedIdParentCoa);
                List<TblCoa> thisListParentCoa = crudService.findWithNamedQuery("TblCoa.findByCoaId", param);
                tblCoa.setCoaParent(thisListParentCoa.get(0).getCoaId());
            } else {
                tblCoa.setCoaParent(null);
            }

            tblCoa.setCoaAccountNo(kelompokCode.concat(coaCode));
            tblCoa.setCoaId(hospitalStringUtils.generateUUID());
            tblCoa.setInsertDate(new Date());
            tblCoa.setInsertBy(getUsername());

            if (HospitalStringUtils.isNotEmpty(tblCoa.getSaldoAwalFormatter())) {
                TblSaldoAwal tblSaldoAwal = new TblSaldoAwal();
                tblSaldoAwal.setSaldoAwalId(HospitalStringUtils.generateUUID());
                String tahun = new SimpleDateFormat("yyyy").format(new Date());
                Date periode = new SimpleDateFormat("yyyy-MM-dd").parse(tahun + "-01-01");
                tblSaldoAwal.setSaldoAwalPeriode(periode);
                tblSaldoAwal.setSaldoAwalJml(HospitalStringUtils.removeCommaFromString(tblCoa.getSaldoAwalFormatter()));
                tblSaldoAwal.setSaldoAwalCoaId(tblCoa);
                List<TblSaldoAwal> listTblSaldoAwal = new ArrayList<>();
                listTblSaldoAwal.add(tblSaldoAwal);
                tblCoa.setTblSaldoAwal(listTblSaldoAwal);
            }

            crudService.create(tblCoa);
            tblCoa = new TblCoa();
            createMessage("INFO", "COA berhasil disimpan.");
            executeInstance("PF('widDlgInsert').hide()");
            updateInstance("thisForm:thisData");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //</editor-fold>

    //    =UPDATE===================================================================
    //<editor-fold defaultstate="collapsed" desc="UPDATE">
    public void doUpdate() {
        disabledSaldoAwal = false;
        if (tblCoa == null) {
            createMessage("WARN", "Pilih data terlebih dahulu.");
        } else {
            listParentCoa = new ArrayList<>();

            if (tblCoa.getTblKelompokCoa() != null) {
                selectedIdKelompokCoa = tblCoa.getTblKelompokCoa().getKelompokCoaKode() + " - " + tblCoa.getTblKelompokCoa().getKelompokCoaId();
                kelompokCode = tblCoa.getTblKelompokCoa().getKelompokCoaKode();
                coaCode = tblCoa.getCoaAccountNo().substring(kelompokCode.length());
            }
            if (tblCoa.getCoaLevel() > 1) {
                HashMap<String, Object> param = new HashMap();
                Integer parentLevel = tblCoa.getCoaLevel() - 1;
                param.put("coaLevel", parentLevel);
                listParentCoa = crudService.findWithNamedQuery("TblCoa.findByCoaLevel", param);
            }
            if (tblCoa.getCoaParent() != null) {
                selectedIdParentCoa = tblCoa.getCoaParent();
            }
            if (!tblCoa.getTblSaldoAwal().isEmpty()) {
                tblCoa.setSaldoAwalFormatter(HospitalStringUtils.addCommaFromDouble(tblCoa.getSaldoAwalLast()));
                if (tblCoa.getTblSaldoAwal().size() > 1) {
                    disabledSaldoAwal = true;
                }
            }
            nameTemp = tblCoa.getCoaAccountNo();

            executeInstance("PF('widDlgUpdate').show()");
            updateInstance("formDlgUpdate:pgridUpdate");
        }
    }

    public void update() throws ParseException {
        HashMap<String, Object> param = new HashMap<>();
        param.put("coaId", tblCoa.getCoaId());
        List<TblCoa> list = crudService.findWithNamedQuery("TblCoa.findByCoaId", param);
        TblCoa kelompokCOA = list.get(0);
        kelompokCOA.setCoaAccountNama(tblCoa.getCoaAccountNama());
        kelompokCOA.setCoaLevel(tblCoa.getCoaLevel());
        kelompokCOA.setCoaIsTransaction(tblCoa.getCoaIsTransaction());
        kelompokCOA.setCoaReport(tblCoa.getCoaReport());

        param = new HashMap<>();
        param.put("kelompokCoaId", selectedIdKelompokCoa.substring(selectedIdKelompokCoa.lastIndexOf(" ") + 1));
        List<TblKelompokCoa> thisListKelompokCoa = crudService.findWithNamedQuery("TblKelompokCoa.findByKelompokCoaId", param);
        kelompokCOA.setTblKelompokCoa(thisListKelompokCoa.get(0));

        if (selectedIdParentCoa != null) {
            param = new HashMap<>();
            param.put("coaId", selectedIdParentCoa);
            List<TblCoa> thisListParentCoa = crudService.findWithNamedQuery("TblCoa.findByCoaId", param);
            kelompokCOA.setCoaParent(thisListParentCoa.get(0).getCoaId());
        } else {
            kelompokCOA.setCoaParent(null);
        }

        kelompokCOA.setCoaAccountNo(kelompokCode.concat(coaCode));

        kelompokCOA.setUpdateDate(new Date());
        kelompokCOA.setUpdateBy(getUsername());
        if (HospitalStringUtils.isNotEmpty(tblCoa.getSaldoAwalFormatter())) {
            if (kelompokCOA.getTblSaldoAwal().isEmpty()) {
                TblSaldoAwal tblSaldoAwal = new TblSaldoAwal();
                tblSaldoAwal.setSaldoAwalId(HospitalStringUtils.generateUUID());
                String tahun = new SimpleDateFormat("yyyy").format(new Date());
                Date periode = new SimpleDateFormat("yyyy-MM-dd").parse(tahun + "-01-01");
                tblSaldoAwal.setSaldoAwalPeriode(periode);
                tblSaldoAwal.setSaldoAwalJml(HospitalStringUtils.removeCommaFromString(tblCoa.getSaldoAwalFormatter()));
                tblSaldoAwal.setSaldoAwalCoaId(kelompokCOA);
                List<TblSaldoAwal> listTblSaldoAwal = new ArrayList<>();
                listTblSaldoAwal.add(tblSaldoAwal);
                kelompokCOA.setTblSaldoAwal(listTblSaldoAwal);
            } else {
                kelompokCOA.getTblSaldoAwal().get(0).setSaldoAwalJml(HospitalStringUtils.removeCommaFromString(tblCoa.getSaldoAwalFormatter()));
            }
        }

        crudService.update(kelompokCOA);
        tblCoa = new TblCoa();
        nameTemp = "";
        createMessage("INFO", "COA berhasil diubah.");
        executeInstance("PF('widDlgUpdate').hide()");
        updateInstance("thisForm:thisData");
    }

    //</editor-fold>
    //    =DELETE===================================================================
//<editor-fold defaultstate="collapsed" desc="DELETE">
    public void doDelete() {
        if (tblCoa == null) {
            createMessage("WARN", "Pilih data terlebih dahulu.");
        } else {
            updateInstance(":formCfmDelete:cfmDelete");
            executeInstance("PF('widCfmDelete').show()");
        }
    }

    public void delete() {
        crudService.delete(tblCoa);
        createMessage("INFO", "Hapus data berhasil.");
        executeInstance("PF('widCfmDelete').hide()");
        updateInstance("thisForm:thisData");
    }
    //</editor-fold>

    public LazyDataModel<TblCoa> getLazyTblCoa() {
        return lazyTblCoa;
    }

    public void setLazyTblCoa(LazyDataModel<TblCoa> lazyTblCoa) {
        this.lazyTblCoa = lazyTblCoa;
    }

    public TblCoa getTblCoa() {
        return tblCoa;
    }

    public void setTblCoa(TblCoa tblCoa) {
        this.tblCoa = tblCoa;
    }

    public String getNameTemp() {
        return nameTemp;
    }

    public void setNameTemp(String nameTemp) {
        this.nameTemp = nameTemp;
    }

    public List<TblKelompokCoa> getListKelompokCoa() {
        return listKelompokCoa;
    }

    public void setListKelompokCoa(List<TblKelompokCoa> listKelompokCoa) {
        this.listKelompokCoa = listKelompokCoa;
    }

    public String getSelectedIdKelompokCoa() {
        return selectedIdKelompokCoa;
    }

    public void setSelectedIdKelompokCoa(String selectedIdKelompokCoa) {
        this.selectedIdKelompokCoa = selectedIdKelompokCoa;
    }

    public String getKelompokCode() {
        return kelompokCode;
    }

    public void setKelompokCode(String kelompokCode) {
        this.kelompokCode = kelompokCode;
    }

    public List<TblCoa> getListParentCoa() {
        return listParentCoa;
    }

    public void setListParentCoa(List<TblCoa> listParentCoa) {
        this.listParentCoa = listParentCoa;
    }

    public String getSelectedIdParentCoa() {
        return selectedIdParentCoa;
    }

    public void setSelectedIdParentCoa(String selectedIdParentCoa) {
        this.selectedIdParentCoa = selectedIdParentCoa;
    }

    public String getCoaCode() {
        return coaCode;
    }

    public void setCoaCode(String coaCode) {
        this.coaCode = coaCode;
    }

    public Boolean getDisabledSaldoAwal() {
        return disabledSaldoAwal;
    }

    public void setDisabledSaldoAwal(Boolean disabledSaldoAwal) {
        this.disabledSaldoAwal = disabledSaldoAwal;
    }

}
