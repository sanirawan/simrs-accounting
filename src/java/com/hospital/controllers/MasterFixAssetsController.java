/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital.controllers;

import com.hospital.crud.CrudService;
import com.hospital.models.TblCoa;
import com.hospital.models.TblFixAssets;
import com.hospital.utils.HospitalStringUtils;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

/**
 *
 * @author arisoft
 */
@ManagedBean(name = "masterFixAssetsController")
@ViewScoped
public class MasterFixAssetsController extends BaseController implements Serializable {

    @EJB
    private CrudService crudService;
    private LazyDataModel<TblFixAssets> lazyTblFixAssets = null;
    private TblFixAssets selectedTblFixAssets = new TblFixAssets();
    private String selectedAccAsset;
    private String selectedAccAkumulasi;
    private String selectedAccBiaya;
    private String nilaiPerolehan;
    private String nilaiBuku;
    private String nilaiPenyusutanPerTahun;
    private String nilaiPenyusutanPerBulan;
    private String acuanPenyusutan;
    private Integer umurEkonomis;
    private BigDecimal persentasePenyusutan;
    private Boolean isInsert;

    /**
     * Creates a new instance of MasterFixAssetsController
     */
    public MasterFixAssetsController() {
    }

    @PostConstruct
    public void init() {
        loadTable();
    }
    
    public void refreshDataTable() {
        loadTable();
    }

    private void loadTable() {
        lazyTblFixAssets = new LazyDataModel<TblFixAssets>() {
            @Override
            public List<TblFixAssets> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> map) {
                String thisQuery = "SELECT t FROM TblFixAssets t WHERE 1=1 ";
                String thisQueryCount = "SELECT COUNT(t) FROM TblFixAssets t WHERE 1=1 ";
                Set<Map.Entry<String, Object>> rawParameters = map.entrySet();

                Boolean isSearch = false;
                for (Map.Entry<String, Object> entry : rawParameters) {
                    isSearch = true;
                    thisQuery = thisQuery + " AND LOWER(t." + entry.getKey() + ") LIKE '%" + entry.getValue().toString().toLowerCase() + "%'";
                    thisQueryCount = thisQueryCount + " AND LOWER(t." + entry.getKey() + ") LIKE '%" + entry.getValue().toString().toLowerCase() + "%'";
                }

                if (sortField != null) {
                    String sortMethod = "DESC";
                    if (sortOrder.toString().equalsIgnoreCase("ASCENDING")) {
                        sortMethod = "ASC";
                    }
                    thisQuery = thisQuery + " ORDER BY t." + sortField + " " + sortMethod;
                } else {
                    thisQuery = thisQuery + " ORDER BY t.fixAssetsNo ASC";
                }

                List<TblFixAssets> list = crudService.findWithCreateQuery(thisQuery, first, pageSize);
                if (isSearch) {
                    setRowCount(list.size());
                } else {
                    int rowCount = crudService.countWithCreateQuery(thisQueryCount);
                    setRowCount(rowCount);
                }
                setPageSize(pageSize);
                return list;
            }

            @Override
            public TblFixAssets getRowData(String rowKey) {
                HashMap<String, Object> param = new HashMap<>();
                param.put("fixAssetsId", rowKey);
                List<TblFixAssets> list = crudService.findWithNamedQuery("TblFixAssets.findByFixAssetsId", param);
                if (list.isEmpty()) {
                    return null;
                } else {
                    return list.get(0);
                }
            }

            @Override
            public Object getRowKey(TblFixAssets obj) {
                return obj.getFixAssetsId();
            }
        };
    }

    public void prepareInsert() {
        selectedTblFixAssets = new TblFixAssets();
        selectedAccAsset = "";
        selectedAccAkumulasi = "";
        selectedAccBiaya = "";
        acuanPenyusutan = "Nilai Perolehan";
        nilaiPerolehan = "0";
        nilaiBuku = "0";
        umurEkonomis = 0;
        persentasePenyusutan = BigDecimal.ZERO;
        nilaiPenyusutanPerTahun = "0";
        nilaiPenyusutanPerBulan = "0";
        isInsert = true;
        executeInstance("PF('widDlgInsert').show()");
        updateInstance("formDlgInsert:pgridInsert");
    }
    
    public void prepareUpdate() {
        selectedAccAsset = selectedTblFixAssets.getFixAssetsCoaIdAsset().getCoaAccountNo() + " - " + selectedTblFixAssets.getFixAssetsCoaIdAsset().getCoaAccountNama();
        selectedAccAkumulasi = selectedTblFixAssets.getFixAssetsCoaIdAkumulasi().getCoaAccountNo() + " - " + selectedTblFixAssets.getFixAssetsCoaIdAkumulasi().getCoaAccountNama();
        selectedAccBiaya = selectedTblFixAssets.getFixAssetsCoaIdBiaya().getCoaAccountNo() + " - " + selectedTblFixAssets.getFixAssetsCoaIdBiaya().getCoaAccountNama();
        acuanPenyusutan = selectedTblFixAssets.getFixAssetsAcuanPenyusutan();
        nilaiPerolehan = HospitalStringUtils.addCommaFromDouble(selectedTblFixAssets.getFixAssetsNilaiPerolehan());
        nilaiBuku = HospitalStringUtils.addCommaFromDouble(selectedTblFixAssets.getFixAssetsNilaiBuku());
        umurEkonomis = selectedTblFixAssets.getFixAssetsUmurEkonomis();
        persentasePenyusutan = selectedTblFixAssets.getFixAssetsPersentasePenyusutan();
        nilaiPenyusutanPerTahun = HospitalStringUtils.addCommaFromDouble(selectedTblFixAssets.getFixAssetsNilaiPenyusutanPertahun());
        nilaiPenyusutanPerBulan = HospitalStringUtils.addCommaFromDouble(selectedTblFixAssets.getFixAssetsNilaiPenyusutanPerbulan());
        isInsert = false;
        executeInstance("PF('widDlgInsert').show()");
        updateInstance("formDlgInsert:pgridInsert");
    }

    public List<String> findAccountNo(String account) {
        List<String> results;
        results = crudService.findByNativeQuery("select CONCAT(coa_account_no,' - ', coa_account_nama) FROM tbl_coa WHERE coa_account_no LIKE '" + account + "%' OR coa_account_nama LIKE '" + account + "%'");
        return results;
    }

    public void countAll() {
        nilaiBuku = nilaiPerolehan;
        if (persentasePenyusutan.compareTo(BigDecimal.ZERO) > 0 || umurEkonomis > 0) {
            countPersentasePenyusutan();
        }
    }

    public void countPersentasePenyusutan() {
        Double nilaiPerolehan = 0.0;
        if (acuanPenyusutan.equals("Nilai Perolehan")) {
            nilaiPerolehan = HospitalStringUtils.removeCommaFromString(this.nilaiPerolehan);
        } else {
            nilaiPerolehan = HospitalStringUtils.removeCommaFromString(nilaiBuku);
        }
        try {
            persentasePenyusutan = new BigDecimal(100 / umurEkonomis);
        } catch (Exception e) {
            persentasePenyusutan = BigDecimal.ZERO;
        }

        Double penyusutan = (100 / umurEkonomis) * (nilaiPerolehan / 100);
        nilaiPenyusutanPerTahun = HospitalStringUtils.addCommaFromDouble(penyusutan);
        nilaiPenyusutanPerBulan = HospitalStringUtils.addCommaFromDouble(penyusutan / 12);

    }

    public void countNilaiPenyusutan() {
        Double nilaiPerolehan = 0.0;
        if (acuanPenyusutan.equals("Nilai Perolehan")) {
            nilaiPerolehan = HospitalStringUtils.removeCommaFromString(this.nilaiPerolehan);
        } else {
            nilaiPerolehan = HospitalStringUtils.removeCommaFromString(nilaiBuku);
        }
        Double umur = 100 / persentasePenyusutan.doubleValue();
        umurEkonomis = umur.intValue();
        Double penyusutan = (nilaiPerolehan / 100) * persentasePenyusutan.doubleValue();
        nilaiPenyusutanPerTahun = HospitalStringUtils.addCommaFromDouble(penyusutan);
        nilaiPenyusutanPerBulan = HospitalStringUtils.addCommaFromDouble(penyusutan / 12);

    }

    public void countPersentasePenyusutanTahun() {
        Double nilaiPerolehan = 0.0;
        if (acuanPenyusutan.equals("Nilai Perolehan")) {
            nilaiPerolehan = HospitalStringUtils.removeCommaFromString(this.nilaiPerolehan);
        } else {
            nilaiPerolehan = HospitalStringUtils.removeCommaFromString(nilaiBuku);
        }

        Double penyusutanTahun = HospitalStringUtils.removeCommaFromString(nilaiPenyusutanPerTahun) / (nilaiPerolehan / 100);
        Double penyusutanBulan = HospitalStringUtils.removeCommaFromString(nilaiPenyusutanPerTahun) / 12;
        System.out.println("penyusutanTahun = " + penyusutanTahun);
        persentasePenyusutan = new BigDecimal(penyusutanTahun).setScale(4, BigDecimal.ROUND_DOWN);
        nilaiPenyusutanPerBulan = HospitalStringUtils.addCommaFromDouble(penyusutanBulan);

    }

    public void countPersentasePenyusutanBulan() {
        Double nilaiPerolehan = 0.0;
        if (acuanPenyusutan.equals("Nilai Perolehan")) {
            nilaiPerolehan = HospitalStringUtils.removeCommaFromString(this.nilaiPerolehan);
        } else {
            nilaiPerolehan = HospitalStringUtils.removeCommaFromString(nilaiBuku);
        }
        Double penyusutanTahun = (HospitalStringUtils.removeCommaFromString(nilaiPenyusutanPerBulan) * 12);
        Double penyusutanBulan = penyusutanTahun / (nilaiPerolehan / 100);
        persentasePenyusutan = new BigDecimal(penyusutanBulan).setScale(4, BigDecimal.ROUND_DOWN);
        nilaiPenyusutanPerTahun = HospitalStringUtils.addCommaFromDouble(penyusutanTahun);

    }

    public void doInsertFixAssets() {
        String noAccount = selectedAccAkumulasi.substring(0, selectedAccAkumulasi.indexOf(" "));
        HashMap<String, Object> param = new HashMap<>();
        param.put("coaAccountNo", noAccount);
        List<TblCoa> listCoaAkumulasi = crudService.findWithNamedQuery("TblCoa.findByCoaAccountNo", param);

        noAccount = selectedAccBiaya.substring(0, selectedAccBiaya.indexOf(" "));
        param = new HashMap<>();
        param.put("coaAccountNo", noAccount);
        List<TblCoa> listCoaBiaya = crudService.findWithNamedQuery("TblCoa.findByCoaAccountNo", param);
        
        noAccount = selectedAccAsset.substring(0, selectedAccAsset.indexOf(" "));
        param = new HashMap<>();
        param.put("coaAccountNo", noAccount);
        List<TblCoa> listCoaAsset = crudService.findWithNamedQuery("TblCoa.findByCoaAccountNo", param);
        if(!HospitalStringUtils.isNotEmpty(selectedTblFixAssets.getFixAssetsId())){
            selectedTblFixAssets.setFixAssetsId(HospitalStringUtils.generateUUID());
        }
        selectedTblFixAssets.setFixAssetsCoaIdAsset(listCoaAsset.get(0));
        selectedTblFixAssets.setFixAssetsCoaIdAkumulasi(listCoaAkumulasi.get(0));
        selectedTblFixAssets.setFixAssetsCoaIdBiaya(listCoaBiaya.get(0));
        selectedTblFixAssets.setFixAssetsNilaiPerolehan(HospitalStringUtils.removeCommaFromString(nilaiPerolehan));
        selectedTblFixAssets.setFixAssetsNilaiBuku(HospitalStringUtils.removeCommaFromString(nilaiBuku));
        selectedTblFixAssets.setFixAssetsNilaiPenyusutanPertahun(HospitalStringUtils.removeCommaFromString(nilaiPenyusutanPerTahun));
        selectedTblFixAssets.setFixAssetsNilaiPenyusutanPerbulan(HospitalStringUtils.removeCommaFromString(nilaiPenyusutanPerBulan));
        selectedTblFixAssets.setFixAssetsPersentasePenyusutan(persentasePenyusutan);
        selectedTblFixAssets.setFixAssetsAcuanPenyusutan(acuanPenyusutan);
        selectedTblFixAssets.setFixAssetsUmurEkonomis(umurEkonomis);
        selectedTblFixAssets.setInsertBy(getUsername());
        selectedTblFixAssets.setInsertDate(new Date());
        crudService.update(selectedTblFixAssets);

        createMessage("INFO", "Fix asset berhasil disimpan.");
        executeInstance("PF('widDlgInsert').hide()");
        updateInstance("thisForm:thisData");
    }
    
    public void prepareDelete() {
        if (selectedTblFixAssets == null) {
            createMessage("WARN", "Pilih data terlebih dahulu.");
        } else {
            updateInstance(":formCfmDelete:cfmDelete");
            executeInstance("PF('widCfmDelete').show()");
        }
    }
    
    public void doDelete() {
        crudService.delete(selectedTblFixAssets);
        createMessage("INFO", "Hapus data berhasil.");
        executeInstance("PF('widCfmDelete').hide()");
        updateInstance("thisForm:thisData");
    }

    public LazyDataModel<TblFixAssets> getLazyTblFixAssets() {
        return lazyTblFixAssets;
    }

    public void setLazyTblFixAssets(LazyDataModel<TblFixAssets> lazyTblFixAssets) {
        this.lazyTblFixAssets = lazyTblFixAssets;
    }

    public TblFixAssets getSelectedTblFixAssets() {
        return selectedTblFixAssets;
    }

    public void setSelectedTblFixAssets(TblFixAssets selectedTblFixAssets) {
        this.selectedTblFixAssets = selectedTblFixAssets;
    }

    public String getSelectedAccAkumulasi() {
        return selectedAccAkumulasi;
    }

    public void setSelectedAccAkumulasi(String selectedAccAkumulasi) {
        this.selectedAccAkumulasi = selectedAccAkumulasi;
    }

    public String getSelectedAccBiaya() {
        return selectedAccBiaya;
    }

    public void setSelectedAccBiaya(String selectedAccBiaya) {
        this.selectedAccBiaya = selectedAccBiaya;
    }

    public BigDecimal getPersentasePenyusutan() {
        return persentasePenyusutan;
    }

    public void setPersentasePenyusutan(BigDecimal persentasePenyusutan) {
        this.persentasePenyusutan = persentasePenyusutan;
    }

    public String getAcuanPenyusutan() {
        return acuanPenyusutan;
    }

    public void setAcuanPenyusutan(String acuanPenyusutan) {
        this.acuanPenyusutan = acuanPenyusutan;
    }

    public String getNilaiPerolehan() {
        return nilaiPerolehan;
    }

    public void setNilaiPerolehan(String nilaiPerolehan) {
        this.nilaiPerolehan = nilaiPerolehan;
    }

    public String getNilaiBuku() {
        return nilaiBuku;
    }

    public void setNilaiBuku(String nilaiBuku) {
        this.nilaiBuku = nilaiBuku;
    }

    public String getNilaiPenyusutanPerTahun() {
        return nilaiPenyusutanPerTahun;
    }

    public void setNilaiPenyusutanPerTahun(String nilaiPenyusutanPerTahun) {
        this.nilaiPenyusutanPerTahun = nilaiPenyusutanPerTahun;
    }

    public String getNilaiPenyusutanPerBulan() {
        return nilaiPenyusutanPerBulan;
    }

    public void setNilaiPenyusutanPerBulan(String nilaiPenyusutanPerBulan) {
        this.nilaiPenyusutanPerBulan = nilaiPenyusutanPerBulan;
    }

    public Integer getUmurEkonomis() {
        return umurEkonomis;
    }

    public void setUmurEkonomis(Integer umurEkonomis) {
        this.umurEkonomis = umurEkonomis;
    }

    public String getSelectedAccAsset() {
        return selectedAccAsset;
    }

    public void setSelectedAccAsset(String selectedAccAsset) {
        this.selectedAccAsset = selectedAccAsset;
    }

    public Boolean getIsInsertSwitch() {
        if(isInsert){
            return false;
        }else{
            return true;
        }
    }
    public Boolean getIsInsert() {
        return isInsert;
    }

    public void setIsInsert(Boolean isInsert) {
        this.isInsert = isInsert;
    }
}
