package com.hospital.crud;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 * A minimalistic implementation of the generic CrudService.
 *
 * @author San
 */
@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class CrudService {

    @PersistenceContext(unitName = "HospitalPU")
    EntityManager em;

    public <T> T create(T t) {
        this.em.persist(t);
        this.em.flush();
        this.em.refresh(t);
        this.em.clear();
        return t;
    }

    public void delete(Class type, Object id) {
        Object ref = this.em.getReference(type, id);
        this.em.remove(ref);
    }

    public <T> void delete(T t) {
        em.remove(em.merge(t));
        em.flush();
    }

    public <T> T update(T t) {
        T objT = (T) em.merge(t);
        em.flush();
        em.clear();
        return objT;
    }
    public <T> T updateMultiple(List<T> t) {
        T objT = null;
        for (int i = 0; i < t.size(); i++) {
            objT = (T) em.merge(t.get(i));
        }
        em.flush();
        em.clear();
        return objT;
    }

    public <T> T updateIgnoreNull(T t) {
        T objT = (T) em.merge(t);
        em.flush();
        em.clear();
        return objT;
    }

    public <T> Object find(Class type, Object id) {
        return this.em.find(type, id);
    }

    public <T> List<T> findByNativeQuery(String sql) {
        return this.em.createNativeQuery(sql).getResultList();
    }

    public <T> List<T> findByNativeQuery(String sql, Class<T> type) {
        return this.em.createNativeQuery(sql, type).getResultList();
    }
    
    public <T> List<T> findByNativeQuery(String sql, Class<T> type, int first, int resultLimit){
        return this.em.createNativeQuery(sql, type).setFirstResult(first).
                setMaxResults(resultLimit).
                getResultList();
    }
    
    public <T> List<T> findByNativeQuery(String sql, Class<T> type,  Map<String, Object> parameters) {
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = this.em.createNativeQuery(sql,type);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        parameters.clear();
        return query.getResultList();
    }

    public List findWithNamedQuery(String namedQueryName) {
        return this.em.createNamedQuery(namedQueryName).getResultList();
    }

    public List findWithNamedQuery(String namedQueryName, Map<String, Object> parameters) {
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = this.em.createNamedQuery(namedQueryName);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        parameters.clear();
        return query.getResultList();
    }

    public List findWithNamedQuery(String namedQueryName, Map<String, Object> parameters, int first, int resultLimit) {
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = this.em.createNamedQuery(namedQueryName);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        parameters.clear();
        return query.
                setFirstResult(first).
                setMaxResults(resultLimit).getResultList();
    }

    public List findWithNamedQuery(String queryName, int first, int resultLimit) {
        return this.em.createNamedQuery(queryName).
                setFirstResult(first).
                setMaxResults(resultLimit).
                getResultList();
    }

    public List findWithNativeQuery(String queryName, int first, int resultLimit) {
        return this.em.createNativeQuery(queryName).
                setFirstResult(first).
                setMaxResults(resultLimit).
                getResultList();
    }
    

    public int countWithNamedQuery(String namedQueryName) {
        int count = ((Number) em.createNamedQuery(namedQueryName).getSingleResult()).intValue();
        return count;
    }
    
    public int countWithNamedQuery(String namedQueryName, Map<String, Object> parameters) {
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = this.em.createNamedQuery(namedQueryName);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        parameters.clear();
        int count = ((Number) query.getSingleResult()).intValue();
        return count;
    }

    public List findWithDynamicQuery(String entity, List<String> criteria, Integer first, Integer resultLimit) {
        String condition = "";
        Integer i;
        for (i = 0; i < criteria.size(); i++) {
            condition += "e." + criteria.get(i);
            if (i + 1 < criteria.size()) {
                condition += " AND ";
            }
        }
        Query query = em.createQuery("SELECT e FROM " + entity + " e WHERE " + condition);
        query.setFirstResult(first);
        query.setMaxResults(resultLimit);
        return query.getResultList();
    }

    public <T> List<T> findWithDynamicNativeQuery(Class<T> type, String entity, List<String> criteria, Integer first, Integer resultLimit) {
        String condition = "";
        Integer i;
        for (i = 0; i < criteria.size(); i++) {
            condition += criteria.get(i);
            if (i + 1 < criteria.size()) {
                condition += " AND ";
            }
        }
        Query query = null;
        if (criteria.isEmpty()) {
            query = em.createNativeQuery("SELECT * FROM " + entity + " e ", type);
        } else {
            query = em.createNativeQuery("SELECT * FROM " + entity + " e WHERE " + condition, type);
        }
        query.setFirstResult(first);
        query.setMaxResults(resultLimit);
        return query.getResultList();
    }

    public Integer countDynamicQuery(String entity, List<String> criteria) {
        String condition = "";
        Integer i;
        for (i = 0; i < criteria.size(); i++) {
            condition += "e." + criteria.get(i);
            if (i + 1 < criteria.size()) {
                condition += " AND ";
            }
        }
        Query query = em.createQuery("SELECT COUNT(e) FROM " + entity + " e WHERE " + condition);
        int count = ((Number) query.getSingleResult()).intValue();
        return count;
    }

    public Integer countDynamicNativeQuery(String entity, List<String> criteria) {
        String condition = "";
        Integer i;
        for (i = 0; i < criteria.size(); i++) {
            condition += criteria.get(i);
            if (i + 1 < criteria.size()) {
                condition += " AND ";
            }
        }
        Query query = null;
        if (criteria.isEmpty()) {
            query = em.createNativeQuery("SELECT COUNT(*) FROM " + entity + " e");
        } else {
            query = em.createNativeQuery("SELECT COUNT(*) FROM " + entity + " e WHERE " + condition);
        }
        int count = ((Number) query.getSingleResult()).intValue();
        return count;
    }

    public List sortByNotZeroId(String entity, String col[], String sort[], Integer first, Integer resultLimit) {
        String sortQuery = "";

        for (int i = 0; i < col.length; i++) {
            if (col[i] == null) {
                sortQuery += "";
            } else {
                sortQuery += "e." + col[i] + " " + sort[i] + ",";
            }
        }
        sortQuery = sortQuery.substring(0, sortQuery.length() - 1);
        Query q = em.createQuery("SELECT e FROM " + entity + " ORDER BY " + sortQuery);

        q.setFirstResult(first);
        q.setMaxResults(resultLimit);
        return q.getResultList();
    }

    public Integer countDynamicSortQueryNotZeroId(String entity, String col[], String sort[]) {
        String sortQuery = "";

        for (int i = 0; i < col.length; i++) {
            if (col[i] == null) {
                sortQuery += "";
            } else {
                sortQuery += "e." + col[i] + " " + sort[i] + ",";
            }

        }
        sortQuery = sortQuery.substring(0, sortQuery.length() - 1);

        Query query = em.createQuery("SELECT COUNT(e) FROM " + entity + " ORDER BY " + sortQuery);
        int count = ((Number) query.getSingleResult()).intValue();
        return count;
    }

    public <T> List<T> findWithCreateQuery(String stringQuery) {
        return this.em.createQuery(stringQuery).getResultList();
    }
    
    public <T> List<T> findWithCreateQuery(String stringQuery, Map<String, Object> parameters) {
        Set<Map.Entry<String, Object>> rawParameters = parameters.entrySet();
        Query query = this.em.createQuery(stringQuery);
        for (Map.Entry<String, Object> entry : rawParameters) {
            query.setParameter(entry.getKey(), entry.getValue());
        }
        parameters.clear();
        return query.getResultList();
    }

    public <T> List<T> findWithCreateQuery(String stringQuery, int first, int maxResult) {
        return this.em.createQuery(stringQuery).setFirstResult(first).
                setMaxResults(maxResult).
                getResultList();
    }

    public int countWithCreateQuery(String stringQuery) {
        int count = ((Number) em.createQuery(stringQuery).getSingleResult()).intValue();
        return count;
    }

    public List sortBy(String entity, String col[], String sort[], Integer first, Integer resultLimit) {
        String sortQuery = "";
        for (int i = 0; i < col.length; i++) {
            if (col[i] == null) {
                sortQuery += "";
            } else {
                sortQuery += "e." + col[i] + " " + sort[i] + ",";
            }
        }
        sortQuery = sortQuery.substring(0, sortQuery.length() - 1);
        Query q = em.createQuery("SELECT e FROM " + entity + " e ORDER BY " + sortQuery);
        q.setFirstResult(first);
        q.setMaxResults(resultLimit);
        return q.getResultList();
    }

    public <T> List<T> sortByNativeQuery(Class<T> type, String entity, String col[], String sort[], Integer first, Integer resultLimit) {
        String sortQuery = "";
        for (int i = 0; i < col.length; i++) {
            if (col[i] == null) {
                sortQuery += "";
            } else {
                sortQuery += "e." + col[i] + " " + sort[i] + ",";
            }
        }
        sortQuery = sortQuery.substring(0, sortQuery.length() - 1);
        Query q = em.createNativeQuery("SELECT * FROM " + entity + " e ORDER BY " + sortQuery, type);
        q.setFirstResult(first);
        q.setMaxResults(resultLimit);
        return q.getResultList();
    }

    public Integer countDynamicSortQuery(String entity, String col[], String sort[]) {
        String sortQuery = "";
        for (int i = 0; i < col.length; i++) {
            if (col[i] == null) {
                sortQuery += "";
            } else {
                sortQuery += "e." + col[i] + " " + sort[i] + ",";
            }
        }
        sortQuery = sortQuery.substring(0, sortQuery.length() - 1);
        Query query = em.createQuery("SELECT COUNT(e) FROM " + entity + " e ORDER BY " + sortQuery);
        int count = ((Number) query.getSingleResult()).intValue();
        return count;
    }

    public Integer countNativeSortQuery(String entity, String col[], String sort[]) {
        String sortQuery = "";
        for (int i = 0; i < col.length; i++) {
            if (col[i] == null) {
                sortQuery += "";
            } else {
                sortQuery += "e." + col[i] + " " + sort[i] + ",";
            }
        }
        sortQuery = sortQuery.substring(0, sortQuery.length() - 1);
        Query query = em.createNativeQuery("SELECT COUNT(*) FROM " + entity + " e ORDER BY " + sortQuery);
        int count = ((Number) query.getSingleResult()).intValue();
        return count;
    }

    public Integer countNativeQuery(String table) {

        Query query = em.createNativeQuery("SELECT COUNT(*) FROM " + table);
        int count = ((Number) query.getSingleResult()).intValue();
        return count;
    }
    
    public Integer nativeQueryReturnInteger(String qry) {
        Query query = em.createNativeQuery(qry);
        int count = ((Number) query.getSingleResult()).intValue();
        return count;
    }

    public void updateWithNativeQuery(String sql) {
        Query q = em.createNativeQuery(sql);
        q.executeUpdate();
        em.flush();
    }
}
