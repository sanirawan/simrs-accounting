package com.hospital.util.validator;

import com.hospital.controllers.BaseController;
import com.hospital.crud.CrudService;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Erick
 */
@FacesValidator("com.hospital.util.validator.UniqueValidator")
public class UniqueValidator extends BaseController implements Validator {

    private HttpServletRequest request;
    private String getValue;

    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        try {
            InitialContext ic = new InitialContext();
            CrudService crudService = (CrudService) ic.lookup("java:module/CrudService");
            List x;

            String table = (String) component.getAttributes().get("table");
            String column = (String) component.getAttributes().get("column");
            String info = (String) component.getAttributes().get("info");
            String compare = (String) component.getAttributes().get("compare");
            String actInfo = (String) component.getAttributes().get("actInfo");
            getValue = (String) value;
            boolean comp = compare.equalsIgnoreCase(getValue);
            if (actInfo.equalsIgnoreCase("A") || comp == false) {
//                if (isInteger(getValue) == true) {
//                    x = crudService.findByNativeQuery("SELECT * FROM " + table + " s WHERE s." + column + " = " + value + "");
//                } else {
                x = crudService.findByNativeQuery("SELECT * FROM " + table + " s WHERE s." + column + " = '" + value + "'");
//                }

                if (x.isEmpty()) {
                } else {
                    String UniqueValidator = "sudah ada di database, silakan mencoba input yang lain!";
                    context = FacesContext.getCurrentInstance();
                    FacesMessage fmsg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "" + info + " " + UniqueValidator + "", "" + info + " " + UniqueValidator + "");
                    throw new ValidatorException(fmsg);
                }
            }

        } catch (NamingException ex) {
            Logger.getLogger(UniqueValidator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static boolean isInteger(String str) {
        if (str == null) {
            return false;
        }
        int length = str.length();
        if (length == 0) {
            return false;
        }
        int i = 0;
        if (str.charAt(0) == '-') {
            if (length == 1) {
                return false;
            }
            i = 1;
        }
        for (; i < length; i++) {
            char c = str.charAt(i);
            if (c <= '/' || c >= ':') {
                return false;
            }
        }
        return true;
    }
}
