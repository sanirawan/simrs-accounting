/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.hospital.utils;

import com.hospital.controllers.BaseController;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;

/**
 *
 * @author San
 */
public class ReportGenerator extends BaseController implements Serializable {

    private static Socket socket;
    String dateNow = format4.format(new Date());

    public void downloadReportPDF(HashMap param, String jasperPath, String reportName, List list) throws Exception {
        JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(list);
        String fileName = jasperPath;
        FacesContext fcontext = null;
        try {
            JasperPrint jprint = JasperFillManager.fillReport(fileName, param, beanCollectionDataSource);
            fcontext = FacesContext.getCurrentInstance();
            HttpServletResponse resp = (HttpServletResponse) fcontext.getExternalContext().getResponse();
            OutputStream os = resp.getOutputStream();
            resp.setContentType("application/pdf");
            resp.addHeader("Content-disposition", "attachment; filename=" + reportName + dateNow + ".pdf");
            JRPdfExporter exporter = new net.sf.jasperreports.engine.export.JRPdfExporter();
            exporter.setExporterInput(new SimpleExporterInput(jprint));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(os));
            SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
            exporter.setConfiguration(configuration);
            exporter.exportReport();
        } catch (JRException | IOException e) {
            System.out.println("Exception in downloadReportPDF");
            e.printStackTrace();
        }
        fcontext.responseComplete();
    }

    public void showReportPDF(HashMap param, String jasperPath, List list) throws Exception {
        JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(list);
        String fileName = jasperPath;
        FacesContext fcontext = null;
        try {
            JasperPrint jprint = JasperFillManager.fillReport(fileName, param, beanCollectionDataSource);
            fcontext = FacesContext.getCurrentInstance();
            HttpServletResponse resp = (HttpServletResponse) fcontext.getExternalContext().getResponse();
            JasperExportManager.exportReportToPdfStream(jprint, resp.getOutputStream());
        } catch (JRException | IOException e) {
            System.out.println("Exception in showReportPDF");
            e.printStackTrace();
        }
        fcontext.responseComplete();
    }

    public void directPrint(HashMap param, String jasperPath, List list) throws Exception {
        JRBeanCollectionDataSource beanCollectionDataSource = new JRBeanCollectionDataSource(list);
        String fileName = jasperPath;
        FacesContext fcontext = null;
        try {
            JasperPrint jprint = JasperFillManager.fillReport(fileName, param, beanCollectionDataSource);
            JasperPrintManager.printPage(jprint, 0, false);
        } catch (JRException e) {
            System.out.println("Exception in directPrint");
            e.printStackTrace();
        }
        fcontext.responseComplete();
    }

    public void sendJsonToClient(String json, String ip) {
        try {
            int port = 2016;
            InetAddress address = InetAddress.getByName(ip);
            socket = new Socket(address, port);

            //Send the message to the server
            OutputStream os = socket.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedWriter bw = new BufferedWriter(osw);
            final byte cEndMessageByte = 94;

            bw.write(json);
            bw.write(cEndMessageByte);
            bw.flush();
            System.out.println("Message sent to the server : " + json);

            //Get the return message from the server
            InputStream is = socket.getInputStream();

//            sendMessage("Connection successful");
            //4. The two parts communicate via the input and output streams
            StringBuilder concatnator = new StringBuilder();
            final byte cEndMessageByte2 = 94;
            byte tMessageByte = cEndMessageByte2;
            System.out.println("IS = " + is.toString());
            while ((tMessageByte = (byte) is.read()) != cEndMessageByte2) {
                //sb.append((char) tMessageByte);
                concatnator.append((char) tMessageByte);
            }
            System.out.println("MSG = " + String.valueOf(concatnator));

        } catch (IOException exception) {
            exception.printStackTrace();
        } finally {
            //Closing the socket
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
